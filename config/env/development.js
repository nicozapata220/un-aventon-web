'use strict';

module.exports = {
	db: 'mongodb://localhost/aventon-dev',
	app: {
		title: 'Un Aventon - Development Environment'
	},
	facebook: {
		clientID: process.env.FACEBOOK_ID || '922797427753319',
		clientSecret: process.env.FACEBOOK_SECRET || '07459edcca6b65d1c3d80172bd76e846',
		callbackURL: '/auth/facebook/callback'
	},
	twitter: {
		clientID: process.env.TWITTER_KEY || '7wF9Z98OyzZAUUnAcCMgeMV3z',
		clientSecret: process.env.TWITTER_SECRET || 'jwnOr0D6U3L23BDKfGruXehzcU2GGH3HnK0wtsq6GwUXZ71XOR',
		callbackURL: '/auth/twitter/callback'
	},
	google: {
		clientID: process.env.GOOGLE_ID || 'APP_ID',
		clientSecret: process.env.GOOGLE_SECRET || 'APP_SECRET',
		callbackURL: '/auth/google/callback'
	},
	linkedin: {
		clientID: process.env.LINKEDIN_ID || 'APP_ID',
		clientSecret: process.env.LINKEDIN_SECRET || 'APP_SECRET',
		callbackURL: '/auth/linkedin/callback'
	},
	github: {
		clientID: process.env.GITHUB_ID || 'APP_ID',
		clientSecret: process.env.GITHUB_SECRET || 'APP_SECRET',
		callbackURL: '/auth/github/callback'
	},
	mailer: {
		from: process.env.MAILER_FROM || 'aventonprueba@gmail.com',
		options: {
			service: process.env.MAILER_SERVICE_PROVIDER || 'Gmail',
			auth: {
				user: process.env.MAILER_EMAIL_ID || 'aventonprueba@gmail.com',
				pass: process.env.MAILER_PASSWORD || '@26481357'
			}
		}
	}
};
