'use strict';

// Users service used for communicating with the roles REST endpoint
angular.module('users').factory('Roles', ['$resource',
	function($resource) {
		return $resource('roles/:roleId', {
			roleId : '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);