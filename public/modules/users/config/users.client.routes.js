'use strict';

// Setting up route
angular.module('users').config(['$stateProvider',
	function($stateProvider) {
		// Users state routing
		$stateProvider.

		state('adminProfile', {
			templateUrl: 'modules/users/views/settings/profile-layout-admin.client.view.html'
		}).
		state('adminProfile.usersList', {
			url: '/usersListAdmin',
			templateUrl: 'modules/users/views/settings/users-list.client.view.html'
		}).
		state('adminProfile.faqsAdmin', {
			url: '/faqsAdmin',
			templateUrl: 'modules/users/views/settings/faqsAdmin.client.view.html'
		}).
		state('adminProfile.newsletterList', {
			url: '/newsletterList',
			templateUrl: 'modules/users/views/settings/newsletter-list.client.view.html'
		}).
		state('adminProfile.newsletterView', {
			url: '/newsletterView/:newsletterId',
			templateUrl: 'modules/users/views/settings/newsletter-view.client.view.html'
		}).
		state('adminProfile.aventonEditAdmin', {
			url: '/editAventonAdmin/:aventonId',
			templateUrl: 'modules/users/views/settings/edit-aventon-admin.client.view.html'
		}).
		state('adminProfile.userProfileAdmin', {
			url: '/userProfileAdmin/:userId',
			templateUrl: 'modules/users/views/settings/userAdmin.client.view.html'
		}).
		state('adminProfile.statistics', {
			url: '/statisticsAdmin',
			templateUrl: 'modules/users/views/settings/statistics.client.view.html'
		}).
		state('adminProfile.aventones', {
			url: '/aventonesAdmin',
			templateUrl: 'modules/users/views/settings/aventones.client.view.html'
		}).
		state('adminProfile.packList', {
			url: '/packListAdmin',
			templateUrl: 'modules/pack/views/pack-list.client.view.html'
		}).
		state('adminProfile.createPack', {
            url: '/createPack',
            templateUrl: 'modules/pack/views/createPack.client.view.html'
        }).
		state('adminProfile.usersNewsletterList', {
			url: '/usersNewsletterListAdmin',
			templateUrl: 'modules/users/views/settings/usersNewsletter-list.client.view.html'
		}).
		state('adminProfile.panel', {
			url: '/panel',
			templateUrl: 'modules/users/views/settings/panel.client.view.html'
		}).     
		state('adminProfile.roleList', {
			url: '/roles',
			templateUrl: 'modules/users/views/settings/roles-list.client.view.html'
		}).
		state('adminProfile.createRole', {
			url: '/roles/create',
			templateUrl: 'modules/users/views/settings/createRole.client.view.html'
		}).
		state('adminProfile.editRole', {
			url: '/roles/:roleId/edit',
			templateUrl: 'modules/users/views/settings/editRole.client.view.html'
		}).
		state('adminProfile.manageRoles', {
			url: '/users/:userId/roles',
			templateUrl: 'modules/users/views/settings/user-roles.client.view.html'
		}).
		state('publicProfile', {
			templateUrl: 'modules/users/views/settings/profile-layout-public.client.view.html'
		}).
		state('publicProfile.review', {
			url: '/profilePublic/:userId',
			templateUrl: 'modules/users/views/settings/review-public.client.view.html'
		}).
		state('profile', {
			templateUrl: 'modules/users/views/settings/profile-layout.client.view.html'
		}).
		state('profile.review', {
			url: '/profile',
			templateUrl: 'modules/users/views/settings/review.client.view.html'
		}).
		state('profile.mytrips', {
			url: '/profile/viajes',
			templateUrl: 'modules/users/views/settings/my-trips.client.view.html'
		}).
		state('profile.myinvitations', {
			url: '/profile/invitaciones',
			templateUrl: 'modules/users/views/settings/my-invitations.client.view.html'
		}).
		state('profile.messages', {
			templateUrl: 'modules/users/views/settings/messages-layout.client.view.html'
		}).
		state('profile.messages.list', {
			url: '/messages',
			templateUrl: 'modules/users/views/settings/messages-list.client.view.html'
		}).
		state('profile.messages.listSent', {
			url: '/messagesSent',
			templateUrl: 'modules/users/views/settings/messages-list-sent.client.view.html'
		}).
		state('profile.messages.listAll', {
			url: '/allMessages',
			templateUrl: 'modules/users/views/settings/messages-list-all.client.view.html'
		}).
		state('profile.messages.detail', {
			url: '/messages/:messageId',
			templateUrl: 'modules/users/views/settings/message-detail.client.view.html'
		}).
		state('profile.messages.detailSent', {
			url: '/messagesSent/:messageId',
			templateUrl: 'modules/users/views/settings/message-detail-sent.client.view.html'
		}).
		state('profile.notifications', {
			url: '/profile/notificaciones',
			templateUrl: 'modules/users/views/settings/notifications.client.view.html'
		}).
		state('profile.settings', {
			templateUrl: 'modules/users/views/settings/settings-layout.client.view.html'
		}).
		state('profile.settings.info', {
			url: '/settings/edit',
			templateUrl: 'modules/users/views/settings/edit-profile.client.view.html'
		}).
		state('profile.settings.cfg', {
			url: '/settings/editCfg',
			templateUrl: 'modules/users/views/settings/edit-cfg.client.view.html'
		}).
		state('profile.settings.imageProfile', {
			url: '/settings/edit/imageProfile',
			templateUrl: 'modules/users/views/settings/edit-image-profile.client.view.html'
		}).
		state('profile.settings.password', {
			url: '/settings/password',
			templateUrl: 'modules/users/views/settings/change-password.client.view.html'
		}).
		state('profile.settings.social', {
			url: '/settings/accounts',
			templateUrl: 'modules/users/views/settings/social-accounts.client.view.html'
		}).
		state('signup', {
			url: '/signup',
			templateUrl: 'modules/users/views/authentication/signup.client.view.html'
		}).
		state('signin', {
			url: '/signin',
			templateUrl: 'modules/users/views/authentication/signin.client.view.html'
		}).
		state('forgot', {
			url: '/password/forgot',
			templateUrl: 'modules/users/views/password/forgot-password.client.view.html'
		}).
		state('reset-invalid', {
			url: '/password/reset/invalid',
			templateUrl: 'modules/users/views/password/reset-password-invalid.client.view.html'
		}).
		state('reset-success', {
			url: '/password/reset/success',
			templateUrl: 'modules/users/views/password/reset-password-success.client.view.html'
		}).
		state('reset', {
			url: '/password/reset/:token',
			templateUrl: 'modules/users/views/password/reset-password.client.view.html'
		});
	}
]);
