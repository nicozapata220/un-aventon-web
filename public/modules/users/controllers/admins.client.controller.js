'use strict';

angular.module('users').controller('AdminsController', ['$scope', '$http', '$location', 'Authentication', 'Users','Packs', '$rootScope', '$stateParams', 'Aventon',
	function($scope, $http, $location, Authentication,Users,Packs, $rootScope, $stateParams, Aventon) {
		$scope.authentication = Authentication;
		
		$scope.showMailBox = false;

		$scope.usersListAdmin = function(){
			$http.get('/usersListAdmin')
			.success(function(users){
				$scope.users = users;
			})
		};

		$scope.usersNewsletterListAdmin = function(){
			$http.get('/usersNewsLetter')
			.success(function(users){
				$scope.users = users;
			})
		};

		$scope.sendMailNewsLetter = function(){
			var contenido = this.content;
			var asunto = this.subject;
			$http.post('/newsletters',{
				subject : asunto,
				content : contenido
			})
			.success(function(){
				$rootScope.alerts.push({type: 'success', msg:'Se ha registrado su newsletter'});
			})
			.error(function(){
				$rootScope.alerts.push({type: 'danger', msg:'Ha ocurrido un error al intentar registrar su newsletter'});
			});

			$http.get('/usersNewsLetter')
			.success(function(users){
				for (var i = users.length - 1; i >= 0; i--) {
					$http.post('/sendEmail',{
						email: users[i].email,
						subject:asunto,
						content: contenido
					}).success(function(){
						console.log("Mail Enviado");
					});
				};
				$rootScope.alerts.push({type: 'success', msg:'Su mensaje ha sido enviado'});
				$scope.content = "";
				$scope.subject = "";
			})
			.error(function(){
				$rootScope.alerts.push({type: 'danger', msg:'Su mensaje no ha podido ser enviado'});
			});
			
		};

		$scope.showMailBoxOn = function(){
			$scope.showMailBox = true;
		}

		$scope.config = "";
		$scope.showMeConfig = function(){
			$http.get('/configs')
			.success(function(configs){
				$scope.config =  configs[0];
			});
		}

		$scope.changeConfig = function(){
			$http.put('/configs/'+ $scope.config._id, {
				price_aventon : this.config.price_aventon
			})
			.success(function(){
				$rootScope.alerts.push({type: 'success', msg:'La configuración se actualizó correctamente'});
			});

		};
		
		$scope.statistics = [];

		$scope.getStatistics = function(){
			// cantidad de usuarios, cantidad de aventones, cantidad de aventones vigentes, 
			//cantidad de aventonados, cantidad de usuarios newsletter
			$http.get('/aventonesQuantity')
			.success(function(data){
				$scope.statistics.push({title:"Cantidad de aventones", value:data.total});
			});
			$http.get('/aventonesLiveQuantity')
			.success(function(data){
				$scope.statistics.push({title:"Cantidad de vigentes", value:data.total});
			});
			$http.get('/aventonesTodayQuantity')
			.success(function(data){
				$scope.statistics.push({title:"Cantidad de aventones que salen hoy", value:data.total});
			});
			$http.get('/usersQuantity')
			.success(function(data){
				$scope.statistics.push({title:"Cantidad de usuarios registrados", value:data.total});
			});
			$http.get('/usersNewsLetterQuantity')
			.success(function(data){
				$scope.statistics.push({title:"Cantidad de usuarios de newsletter registrados", value:data.total});
			});
			$http.get('/demandsQuantity')
			.success(function(data){
				$scope.statistics.push({title:"Cantidad de solicitudes de aventón enviadas", value:data.total});
			});
			$http.get('/demandsAcceptedQuantity')
			.success(function(data){
				$scope.statistics.push({title:"Cantidad de solicitudes de aventón aceptadas", value:data.total});
			});

		};

		$scope.createFaq = function(){
			$http.post('/faqs',{
				question : this.question,
				answer : this.answer
			})
			.success(function(faq){
				$rootScope.alerts.push({type: 'success', msg:'Se ha creado una nueva faq'});
				$scope.listFaq();
				$scope.question = "";
				$scope.answer = "";
				$scope.showMailBox = false;
			});
		};

		$scope.removeFaq = function(id){
			$http.delete('/faqs/'+id)
			.success(function(){
				$rootScope.alerts.push({type: 'success', msg:'Se ha eliminado una faq'});
				$scope.listFaq();
			});
		};

		$scope.updateFaq = function(id){
			$http.put('/faqs/'+id,{
				question: this.faq.question,
				answer: this.faq.answer
			})
			.success(function(faqs){
				$scope.idUpdate = "";
				$scope.listFaq();
			});
		};

		$scope.listFaq = function(){
			$http.get('/faqs')
			.success(function(faqs){
				$scope.faqs = faqs;
			});
		};
		
		$scope.idRemove = "";

		$scope.changeIdRemove = function(id){
			$scope.idRemove = id;
		};

		$scope.idUpdate = "";

		$scope.changeIdUpdate = function(id){
			$scope.idUpdate = id;
			console.log($scope.idUpdate);
		};

		$scope.findUser = function() {
			$http.get('/users/'+$stateParams.userId)
			.success(function(user){
				$scope.user = user;
			});
		};

		$scope.updateUser = function() {
			$http.put('/admin/users/'+$stateParams.userId,
				{
					firstName : this.user.firstName,
					lastName : this.user.lastName,
					credit : this.user.credit,
					email : this.user.email
				})
			.success(function(){
				$rootScope.alerts.push({type: 'success', msg:'Usuario modificado correctamente'});			
			});
		};

		$scope.goUser = function(id){
			$location.path('/userProfileAdmin/'+ id);
		};

		$scope.editRoles = function(id){
			$location.path('/users/'+ id +'/roles');
		};

		$scope.removeUser = function(id){
			$http.delete('users/'+id)
			.success(function(){
				$location.path('/usersListAdmin');
				$rootScope.alerts.push({type: 'success', msg:'Usuario eliminado correctamente'});			
			});
		};

		$scope.totalItems = 0;
		$scope.perPage = 2;
		$scope.currentPage = 1;
		$scope.maxSizeAventones = 10;

		$scope.findAventones = function() {
			var page = $scope.currentPage;
			Aventon.paginateAdmin({page: page}, function(response) {
				$scope.aventones = response.aventones;
				$scope.totalItems = response.total;
			});
		};

		$scope.showCheck = "false";

		$scope.changeShowCheck = function(value){
			$scope.showCheck = value;
		};

		$scope.findCheckAventones = function(){
			$http.get("/showCheckedNeed")
			.success(function(aventones){
				$scope.aventones = aventones;
			});
		};

		$scope.setPage = function (pageNo) {
		    $scope.currentPage = pageNo;
		  };
		$scope.removeAventon = function(id){
			$http.delete('aventon/' + id)
			.success(function(){
				$rootScope.alerts.push({type: 'success', msg:'Aventón eliminado correctamente'});
				$scope.findAventones();
				$http.post('/demandsAcceptedByAventon', {aventonId : id})
				.success(function(demands){
					for (var i = demands.length - 1; i >= 0; i--) {
						var idUser = demands[i].user._id;
						$http.post('/notifications',{
							type: 'cancelAventon',
							anexo_user: idUser							
						})
						.success(function(){
							$http.post('/sendEmail',{
								email: user.email,
								subject:'Aventón cancelado - UnAventon',
								content: 'El usuario '+demands[i].user.displayName+' ha cancelado el aventón ofrecido'
							}).success(function(){
								console.log("Mail Enviado");
							});
						});

						$http.put('/demands/'+id,{
							state : 'Cancelada'
						})
					}; //endFor
				});
			});

		};

		$scope.goUpdateAventon = function(id){
			$location.path('/editAventonAdmin/' + id);
		};

		$scope.blockUser = function(id){
			$http.put('/users/'+id,{
				state : 'Bloqueado'
			})
			.success(function(){
				$scope.usersListAdmin();
				$rootScope.alerts.push({type: 'success', msg:'Usuario bloqueado'});
			});
		};

		$scope.unBlockUser = function(id){
			$http.put('/users/'+id,{
				state : 'Activado'
			})
			.success(function(){
				$scope.usersListAdmin();
				$rootScope.alerts.push({type: 'success', msg:'Usuario desbloqueado'});
			});
		};

		$scope.goNewsletterList = function(id){
			$location.path('/newsletterList');
		};

		$scope.newsletterList = function(){
			$http.get('/newsletters')
			.success(function(data){
				$scope.newsletters =  data;
			})
			.error(function(){
				$rootScope.alerts.push({type: 'success', msg:'Hubo un error y no pudieron cargarse las newsletter enviadas'});
			});
		};

		$scope.goNewsletterView = function(id){
			$location.path('/newsletterView/'+ id);
		};

		$scope.findOneNewsletter = function(){
			$http.get('/newsletters/'+$stateParams.newsletterId)
			.success(function(newsletter){
				$scope.newsletter = newsletter;
			})
			.error(function(){
				$rootScope.alerts.push({type: 'danger', msg:'Hubo un error y no pudo cargarse el newsletter'});
			});
		};

	}
]);