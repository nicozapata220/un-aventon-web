'use strict';

angular.module('users').controller('RolesController', ['$scope', '$http', '$location', '$rootScope', '$stateParams', 'Authentication', 'Users', 'Roles',
	function($scope, $http, $location, $rootScope, $stateParams, Authentication, Users, Roles) {
		$scope.authentication = Authentication;

	    $scope.allPermissions = ['Manejo_Newsletter', 'Manejo_Usuarios', 'Manejo_Viajes', 'Manejo_Aventones', 'Estadisticas'];
	    $scope.permissions = [];

		$scope.create = function() {
			var role = new Roles({
				permissions: this.permissions,
				name: this.name
			});
			role.$save(function(response) {
	            $scope.name = "";
	            $scope.permissions = [];
				$location.path('roles');
	            $rootScope.alerts.push({ type: 'success', msg: 'Rol creado correctamente' });
			}, function(errorResponse) {
	            $rootScope.alerts.push({ type: 'error', msg: errorResponse.data.message });
			});
		};

		$scope.remove = function(role) {
			if (role) {
				role.$remove();

				for (var i in $scope.roles) {
					if ($scope.roles[i]._id === role._id) {
						$scope.roles.splice(i, 1);
					}
				}
				rootScope.alerts.push({ type: 'success', msg: 'Rol removido correctamente' });

			} else {
				$scope.role.$remove(function() {
					$location.path('roles');
				});
			}
		};

		$scope.update = function() {
			var role = $scope.role;

			role.$update(function() {
				$location.path('roles/' + role._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.find = function() {
			$scope.roles = Roles.query();
		};

		$scope.findOne = function() {
			$scope.role = Roles.get({
				roleId: $stateParams.roleId
			});
		};
		

	    $scope.updateUser = function() {
	        var user = $scope.user;
	        /*user.$update(function() {
	            $rootScope.alerts.push({ type: 'success', msg: 'Roles de usuario actualizados correctamente' });
	        });*/
	        var user_obj = {
	        	custom_roles: user.custom_roles
	        }
	      	$http.put('/admin/users/'+user._id, user_obj).then(function() {
	            $rootScope.alerts.push({ type: 'success', msg: 'Roles de usuario actualizados correctamente' });
	        });
	    };

	    $scope.findUser = function() {
	        Users.get({
	            userId: $stateParams.userId
	        }, function(user) {
	            $scope.user = user;
	        });
	    };		
	}
]);