'use strict';

angular.module('users').controller('SettingsController', ['$scope', '$http', '$location', 'Users', 'Authentication', '$rootScope', '$stateParams', '$upload', '$timeout',
	function($scope, $http, $location, Users, Authentication, $rootScope, $stateParams, $upload, $timeout) {

		$scope.today = new Date().toJSON();

		$scope.SetActiveTab = function(tab){
			$rootScope.activeTab = tab;
		};

		$scope.user = Authentication.user;

		$scope.getUser = function(){
			$scope.user = Users.get({
				userId: $stateParams.userId
			});
		};

		$scope.internTab = "viajes";

		$scope.internTabChg = function(t){
			$scope.internTab = t;
		};

		$scope.internTabI = "invitaciones";
		
		$scope.internTabIChg = function(t){
			$scope.internTabI = t;
		};

		
		//MOCK reviws and messages
		$scope.message = {
			user:'Frodo Bolsón',
			asunto: 'Hola como estas?',
			date: '20/12/2014',
			text: 'Curabitur tincidunt tortor quis metus dapibus feugiat. Morbi ornare tortor quis sapien maximus tristique. Fusce eget convallis eros. Donec vel odio ut dolor suscipit dictum eget ac mi. Sed imperdiet, dui quis accumsan lobortis, leo enim pellentesque odio, eget euismod nunc orci sit amet risus. Mauris dictum justo in velit suscipit fringilla eget a ex. Mauris in nisl nisi. Nulla sollicitudin nisi sed erat dignissim consequat. Proin ultrices nulla enim. Mauris et nunc libero. Morbi tincidunt imperdiet tellus vel condimentum. Ut commodo hendrerit est, vulputate posuere mi consectetur non. Integer eu erat vulputate, hendrerit lorem ac, tincidunt urna. '
		};
		
		$scope.user.reviews = [{
			user:'Frodo Bolsón',
			date: 'hace 2 días',
			vote: 'Todo excelente',
			description: 'Con golum fuimos y volvimos a Buenos Aires el anterior invierno, la verdad que salio todo perfecto, ahorramos unos pesos y nos hicimos buenos amigos'
		},
			{
				user:'Frodo Bolsón',
				date: 'hace 2 días',
				vote: 'Todo excelente',
				description: 'Con golum fuimos y volvimos a Buenos Aires el anterior invierno, la verdad que salio todo perfecto, ahorramos unos pesos y nos hicimos buenos amigos'
			},
			{
				user:'Frodo Bolsón',
				date: 'hace 2 días',
				vote: 'Todo excelente',
				description: 'Con golum fuimos y volvimos a Buenos Aires el anterior invierno, la verdad que salio todo perfecto, ahorramos unos pesos y nos hicimos buenos amigos'
			},
			{
				user:'Frodo Bolsón',
				date: 'hace 2 días',
				vote: 'Todo excelente',
				description: 'Con golum fuimos y volvimos a Buenos Aires el anterior invierno, la verdad que salio todo perfecto, ahorramos unos pesos y nos hicimos buenos amigos'
			},
			{
				user:'Frodo Bolsón',
				date: 'hace 2 días',
				vote: 'Todo excelente',
				description: 'Con golum fuimos y volvimos a Buenos Aires el anterior invierno, la verdad que salio todo perfecto, ahorramos unos pesos y nos hicimos buenos amigos'
			}
		];
		// If user is not signed in then redirect back home
		if (!$scope.user) $location.path('/');

		// Check if there are additional accounts 
		$scope.hasConnectedAdditionalSocialAccounts = function(provider) {
			for (var i in $scope.user.additionalProvidersData) {
				return true;
			}

			return false;
		};

		// Check if provider is already in use with current user
		$scope.isConnectedSocialAccount = function(provider) {
			return $scope.user.provider === provider || ($scope.user.additionalProvidersData && $scope.user.additionalProvidersData[provider]);
		};

		// Remove a user social account
		$scope.removeUserSocialAccount = function(provider) {
			$scope.success = $scope.error = null;

			$http.delete('/users/accounts', {
				params: {
					provider: provider
				}
			}).success(function(response) {
				// If successful show success message and clear form
				$scope.success = true;
				$scope.user = Authentication.user = response;
			}).error(function(response) {
				$scope.error = response.message;
			});
		};

		// Update a user profile
		$scope.updateUserProfile = function(isValid) {
			if (isValid){
				$scope.success = $scope.error = null;
				var user = new Users($scope.user);
	
				user.$update(function(response) {
					$scope.success = true;
					Authentication.user = response;
				}, function(response) {
					$scope.error = response.data.message;
				});

				if($scope.newsletter && !$scope.newsletterOld){
					$http.post('/subscribe', {email : user.email, state:'Activado'});
					$scope.checkUserNewsLetter();
				}
				if(!$scope.newsletter && $scope.newsletterOld) {
					$http.post('/deleteSuscriptorByEmail', {email : user.email})
					$scope.checkUserNewsLetter();
				}

			} else {
				$scope.submitted = true;
			}

		};

		$scope.range = function(min, max, step){
	        step = step || 1;
	        var input = [];
	        for (var i = min; i <= max; i += step) input.push(i);
	            return input;
	    };

	    $scope.addPatent = function(patent){
	    	if(patent != ""){
		    	if(!$scope.user.patents)
		    		$scope.user.patents = [];
		    	var obj = new Object();
		    	obj.patent = patent;
		    	$scope.user.patents.push(obj);
		    	$scope.newPatent = "";
		    }
	    };

	    $scope.removePatent = function(index){
	        $scope.user.patents.splice(index,1);
	    };

		$scope.checkUserNewsLetter = function(){
			$http.post('/getUserNewsLetterByEmail',{email: $scope.user.email})
			.success(function(){
				$scope.newsletter = true;
				$scope.newsletterOld = true;
			})
			.error(function(){
				$scope.newsletter = false;
				$scope.newsletterOld = false;
			});
		}

		// Change user password
		$scope.changeUserPassword = function() {
			$scope.success = $scope.error = null;

			$http.post('/users/password', $scope.passwordDetails).success(function(response) {
				// If successful show success message and clear form
				$scope.success = true;
				$scope.passwordDetails = null;
			}).error(function(response) {
				$scope.error = response.message;
			});
		};

		//return true if the current user is registered whit a local account, otherwise return false
		$scope.isLocal = function() {
			return $scope.user.provider == "local"

		}

		$scope.goChangeImageProfile = function(){
			$location.path('/settings/edit/imageProfile');
		};

		$scope.saveImageProfile = function(files){
			$scope.file = files[0];
		};

		$scope.upload = function () {
			var file = $scope.file;
	        if (file) {
	        		$scope.generateThumb(file);
	                $upload.upload({
	                    url: '/uploadImage/'+$scope.user._id,
	                    fields: {},
	                    file: file
	                }).progress(function (evt) {
	                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
	                    console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
	                }).success(function (data, status, headers, config) {
	                    console.log('file ' + config.file.name + 'uploaded. Response: ' + data);
	                    
	                    var user = new Users($scope.user);
						user.imageProfile = data.file.path;
						user.imageProfile = user.imageProfile.replace('public/','');

						user.$update(function(response) {
							$scope.user.imageProfile = file.dataUrl;
							$rootScope.alerts.push({type: 'success', msg:'Se ha cargado correctamente su nueva imágen de perfil'});
						}, function error(err) {
							$rootScope.alerts.push({type: 'danger', msg:'Hubo un error, intente nuevamente.'});
						});
	                    
	                });
	        }
	    };

	    $scope.generateThumb = function(file) {
            if (file != null) {
                $timeout(function() {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL(file);
                    fileReader.onload = function(e) {
                        $timeout(function() {
                            file.dataUrl = e.target.result;
                        });
                    }
                });
            }
        };

	}
]);
