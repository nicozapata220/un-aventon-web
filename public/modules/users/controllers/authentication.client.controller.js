'use strict';

angular.module('users').controller('AuthenticationController', ['$scope', '$http', '$location', 'Authentication', '$rootScope','$stateParams',
	function($scope, $http, $location, Authentication, $rootScope,$stateParams) {
		$scope.authentication = Authentication;

		// If user is signed in then redirect back home
		if ($scope.authentication.user) $location.path('/');

		$scope.signup = function() {
			if($scope.credentials.password != $scope.credentials.password2){
				$rootScope.alerts.push({type: 'danger', msg:'Las contraseñas no coinciden'});
				console.log("no");
			}
			else{
	            $scope.credentials.username=$scope.credentials.email;
				$http.post('/auth/signup', $scope.credentials).success(function(response) {
					// If successful we assign the response to the global user model
					/*response = permissionsObject(response);
					$scope.authentication.user = response;

					// And redirect to the index page
					//$location.path('/');*/
					$scope.success = response.message;
				}).error(function(response) {
					$scope.error = response.message;
				});
			}
		};

		$scope.signin = function() {
			$http.post('/auth/signin', $scope.credentials).success(function(response) {
				// If successful we assign the response to the global user model
				response = permissionsObject(response);
				$scope.authentication.user = response;

				// And redirect to the index page
				$location.path('/');
			}).error(function(response) {
				$scope.error = response.message;
			});
		};


		function permissionsObject(user) {
		    if(user && user.custom_roles) {
		        user.permissions = {};
		        for (var i = user.custom_roles.length - 1; i >= 0; i--) {
		            user.permissions.showMenu = true;
		            if(user.custom_roles[i].permissions) {
			            for (var j = user.custom_roles[i].permissions.length - 1; j >= 0; j--) {
			                user.permissions[user.custom_roles[i].permissions[j]] = true;
			            };
		            }
		        };
		    }
		    return user;		
		};


		$scope.showForgot = 'false';

		$scope.changeShowForgot = function(){
			if($scope.showForgot == 'false')
				$scope.showForgot = 'true';
			else
				$scope.showForgot = 'false';
			console.log($scope.showForgot);
		};

		$scope.initializeValidate = function(){			
			$http.get('/auth/validateuser/'+$stateParams.id+'/'+$stateParams.token)
			.success(function(data){
				$scope.success = data.message;
				console.log(data.msg);
			})
			.error(function(data){
				$scope.error = data.message;
				console.log(data.msg);
			});
		};

	}
]);