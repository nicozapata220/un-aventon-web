'use strict';

// Setting up route
angular.module('messages').config(['$stateProvider',
	function($stateProvider) {
		// Message state routing
		$stateProvider.
		state('createMessages', {
			url: '/message/create',
			templateUrl: 'modules/message/views/create-messages.client.view.html'
		});
	}
]);