'use strict';

angular.module('messages').controller('MessagesController', ['$scope', '$stateParams', '$location', 'Authentication', 'Messages','$http', '$rootScope',
	function($scope, $stateParams, $location, Authentication, Messages, $http, $rootScope) {
		$scope.authentication = Authentication;
		$rootScope.activeTab = 'messages';

		$scope.preCreate = function(idReceiver){
			$scope.idReceiver = idReceiver;
		};

		$scope.create = function(idReceiver) {
			var message = new Messages({
				content: this.content,
				subject: this.subject,
				user_sender: $scope.authentication.user._id,
				user_receiver: idReceiver
			});
			message.$save(function(response) {
				

				$scope.content = '';
				$scope.subject = '';

				//$location.path('usersNewsLetter/' + response._id);
				//mostrar cartel
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.fastCreate = function(idReceiver){
			var message = new Messages({
				content: this.content,
				subject: 'mensaje rapido - pedido de aventón',
				user_sender: $scope.authentication.user._id,
				user_receiver: idReceiver
			});
			message.$save(function(response) {
				$http.post('/conversations',{members:[idReceiver,$scope.authentication.user._id]})
				.success(function(data){
					$rootScope.alerts.push({type: 'info', msg:'Usted puede chatear con este usuario'});
				});
				$http.post('/searchPublicUser',{id:idReceiver})
				.success(function(user){
					$http.post('/notifications',{
						type: 'message',
						anexo_user: $scope.authentication.user._id,
						comment: 'Un usuario te ha enviado un mensaje privado',
						user: user._id,
						message: response._id
					})
					.success(function(){
						$http.post('/sendEmail',{
							email: user.email,
							subject:'Recibiste un mensaje privado - UnAventon',
							content: 'El usuario '+$scope.authentication.user.displayName+' te ha enviado un mensaje privado.'
						}).success(function(){
							console.log("Mail Enviado");
						});

					});

				});
				
				$rootScope.alerts.push({type: 'success', msg:'Su mensaje ha sido enviado correctamente'});
				$scope.content = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};


		$scope.sendPrivateMessage = function(idReceiver){
			var message = new Messages({
				content: this.content,
				subject: 'mensaje rapido - pedido de aventón',
				user_sender: $scope.authentication.user._id,
				user_receiver: idReceiver
			});
			message.$save(function(response) {
				$scope.allMessagesByUser();
				$http.post('/searchPublicUser',{id:idReceiver})
				.success(function(user){
					$http.post('/notifications',{
						type: 'message',
						anexo_user: $scope.authentication.user._id,
						comment: 'Un usuario te ha enviado un mensaje privado',
						user: user._id,
						message: response._id
					})
					.success(function(){
						$http.post('/sendEmail',{
							email: user.email,
							subject:'Recibiste un mensaje privado - UnAventon',
							content: 'El usuario '+$scope.authentication.user.displayName+' te ha enviado un mensaje privado.'
						}).success(function(){
							console.log("Mail Enviado");
						});

					});

				});
				
				$rootScope.alerts.push({type: 'success', msg:'Su mensaje ha sido enviado correctamente'});
				$scope.content = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});			
		};

		$scope.remove = function(article) {
			if (article) {
				article.$remove();

				for (var i in $scope.articles) {
					if ($scope.articles[i] === article) {
						$scope.articles.splice(i, 1);
					}
				}
			} else {
				$scope.article.$remove(function() {
					$location.path('articles');
				});
			}
		};

		$scope.update = function(idReview,comment,valoration) {
			$http.get('/reviews/'+idReview)
			.success(function(review){
				review.comment = comment;
				review.valoration = valoration;
				review.$update(function(){
					$rootScope.alerts.push({type: 'success', msg:'Usted ha realizado una devolución correctamente'});
				}, function(errorResponse){
					$rootScope.alerts.push({type: 'danger', msg:'Hubo un error, intente nuevamente'});
				});
			})
			.error(function(){
				$rootScope.alerts.push({type: 'danger', msg:'Hubo un error, intente nuevamente'});
			});
		};

		$scope.find = function() {
			$scope.articles = Articles.query();
		};

		$scope.findOne = function() {
			$scope.message = Messages.get({
				messageId: $stateParams.messageId
			});
		};

		$scope.findOneSent = function() {
			$scope.message = Messages.get({
				messageId: $stateParams.messageId
			});
		};


		$scope.messagesToMe = function(){
			$http.get('/messagesToMe')
			.success(function(data){
				$scope.messages = data;
			});
		};

		$scope.idMailWindow = "";

		$scope.changeIdMailWindow = function(newId){
			if($scope.idMailWindow == newId )
				$scope.idMailWindow = "";
			else
				$scope.idMailWindow = newId;
		};

		$scope.messagesSent = function(){
			$http.get('/messagesSent')
			.success(function(data){
				$scope.messages = data;
			});
		};

		$scope.allMessages = function(){
			$http.get('/allMessagesByUser')
			.success(function(data){
				$scope.messages = data;
			});	
		}

		$scope.allMessagesByUser = function(){
			$http.get('/allMessagesByUser/'+ $stateParams.messageId)
			.success(function(data){
				$scope.messages = data;
				$scope.userResponse = $stateParams.messageId;
				var sample = $scope.messages[0];
				if(sample.user_receiver._id != $scope.authentication.user._id)
					$scope.contact = sample.user_receiver;
				else
					$scope.contact = sample.user_sender;
				console.log($scope.messages);
			});	
		}

	}
]);
