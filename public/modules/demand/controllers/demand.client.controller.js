'use strict';

angular.module('demands').controller('DemandsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Demands', '$http', '$rootScope', 
	function($scope, $stateParams, $location, Authentication, Demands, $http, $rootScope) {
		$scope.authentication = Authentication;

		$scope.create = function(aventon) {
			var demand = new Demands({
				user: $scope.authentication.user._id,
				aventon: aventon._id
			});
			demand.$save(function(response) {
				if(response.message){
					$rootScope.alerts.push({type: 'danger', msg:'Usted ya ha enviado una solicitud anteriormente por este aventón'});
				}else{ 
					$rootScope.alerts.push({type: 'info', msg:'Le has enviado el pedido de viaje'});
					$http.post('/notifications',{
						aventon: aventon._id,
						type : 'trip',
						anexo_user : $scope.authentication.user._id,
						comment: 'Un usuario quiere aprovechar su aventón',
						user: aventon.user._id,
					})
					.success(function(){
						$http.post('/sendEmail',{
							email: aventon.user.email,
							subject:'Un usuario quiere aprovechar tu aventón',
							content: 'El usuario '+$scope.authentication.user.displayName+' quiere aprovechar tu aventón'
						}).success(function(){
							console.log("Mail Enviado");
						});
					});
				};				
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.remove = function(article) {
			if (article) {
				article.$remove();

				for (var i in $scope.articles) {
					if ($scope.articles[i] === article) {
						$scope.articles.splice(i, 1);
					}
				}
			} else {
				$scope.article.$remove(function() {
					$location.path('articles');
				});
			}
		};

		$scope.update = function() {
			var article = $scope.article;

			article.$update(function() {
				$location.path('articles/' + article._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.find = function() {
			$scope.articles = Articles.query();
		};

		$scope.findOne = function() {
			$scope.article = Articles.get({
				articleId: $stateParams.articleId
			});
		};

		$scope.demandsForMe = function() {
			$http.get('/demandsForMe').success(function(data){
				$scope.demands = data;
			});
		};

		$scope.acceptDemand = function(idDemand){
			Demands.get({
				demandId : idDemand
			}, function(demand){
				demand.state = 'Aceptada';
				demand.$update(function(){
					$rootScope.alerts.push({type: 'info', msg:'Usted ha aceptado correctamente la solicitud de aventón'});
					$scope.giveMeDemandsByAventon(demand.aventon);
					$http.get('/aventon/'+demand.aventon)
					.success(function(aventon){
						console.log(demand.user);
						console.log(aventon.user);
						console.log(aventon._id);
						$http.post('/review',{
							'user_origin' : demand.user,
							'user_receiver' : aventon.user._id ,
							'aventon' : aventon._id
						})
						.success(function(review){
							$http.post('/review',{
								'user_receiver' : demand.user,
								'user_origin' : aventon.user._id ,
								'aventon' : aventon._id
							})
							$http.post('/notifications',{
								'user' : demand.user,
								'anexo_user' : $scope.authentication.user._id,
								'comment' : 'Han aceptado tu solicitud de Aventón',
								'aventon' : aventon._id,
								'type' : "demandA"
							});
						})
						.error(function(){

						});
					})
					.error(function(){

					});
					
				},
				function(errorResponse){
					
				});
			});
		};

		$scope.dismissDemand = function(idDemand){
			Demands.get({
				demandId : idDemand
			}, function(demand){
				demand.state = 'Rechazada';
				demand.$update(function(){
					$rootScope.alerts.push({type: 'info', msg:'Usted ha rechazado la solicitud de aventón'});
					$scope.giveMeDemandsByAventon(demand.aventon);
				},
				function(errorResponse){
					$rootScope.alerts.push({type: 'danger', msg:'Hubo un error, intentelo nuevamente'});
				});
			});
		};

		$scope.demandsWaitingByAventon = function(idAventon){
			$http.post('/demandsWaitingByAventon',{
				'aventonId' : idAventon
			})
			.success(function(demandsWaiting){
				$scope.demandsWaiting = demandsWaiting;
			})
		};

		$scope.demandsAcceptedByAventon = function(idAventon){
			$http.post('/demandsAcceptedByAventon',{
				'aventonId' : idAventon
			})
			.success(function(demandsAccepted){
				$scope.demandsAccepted = demandsAccepted;
			})
		};

		$scope.demandsRejectedByAventon = function(idAventon){
			$http.post('/demandsRejectedByAventon',{
				'aventonId' : idAventon
			})
			.success(function(demandsRejected){
				$scope.demandsRejected = demandsRejected;
			})
		};

		$scope.giveMeDemandsByAventon = function(idAventon){
			$scope.demandsAcceptedByAventon(idAventon);
			$scope.demandsWaitingByAventon(idAventon);
		};

	}
]);