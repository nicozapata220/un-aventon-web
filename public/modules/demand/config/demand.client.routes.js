'use strict';

// Setting up route
angular.module('demands').config(['$stateProvider',
	function($stateProvider) {
		// Articles state routing
		$stateProvider.
		state('exampleDemand', {
			url: '/demand/example',
			templateUrl: 'modules/demand/views/example-demand.client.view.html'
		});
	}
]);