'use strict';

//Demands service used for communicating with the articles REST endpoints
angular.module('demands').factory('Demands', ['$resource',
	function($resource) {
		return $resource('demands/:demandId', {
			demandId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);