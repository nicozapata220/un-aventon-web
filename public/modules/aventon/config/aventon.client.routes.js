'use strict';

// Setting up route
angular.module('aventon').config(['$stateProvider',
	function($stateProvider) {
		// Articles state routing
		$stateProvider.
		state('createAventon', {
			url: '/aventon/nuevo',
			templateUrl: 'modules/aventon/views/create-aventon.client.view.html'
		}).
		state('viewAventon', {
			url: '/aventon/:aventonId',
			templateUrl: 'modules/aventon/views/view-aventon.client.view.html'
		}).
		state('editAventon', {
			url: '/aventon/:aventonId/edit',
			templateUrl: 'modules/aventon/views/edit-aventon.client.view.html'
		});
	}
]);

/*
Uso complicado de este modulo
<google-places location=direccion identificador="1" id="google_places_ac1"type="text" class="a-place"/>
el id debe iniciar 'google_places_ac' y el final debe coincidir con identificador

*/
angular.module('core').
    directive('googlePlaces', function(){
        return {
            restrict:'E',
            replace:true,
            //transclude:true,
            scope: {location:"=", identificador:"@", callback:"&"},
            template: '<input class="" placeholder="Direccion"  type="text" />',
            link: function($scope, elm, attrs){
                var id = "#google_places_ac"+$scope.identificador;
                var autocomplete = new google.maps.places.Autocomplete($(id)[0], {});
                google.maps.event.addListener(autocomplete, 'place_changed', function() {
                    var place = autocomplete.getPlace();
                    $scope.location = {};
                    $scope.location.lat = place.geometry.location.lat();
                    $scope.location.lng = place.geometry.location.lng();
                    $scope.location.direccion = place.formatted_address;
                    $scope.$apply();
                    $scope.callback();
                });
            }
        }
    })