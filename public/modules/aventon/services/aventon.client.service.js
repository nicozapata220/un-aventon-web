'use strict';

//Articles service used for communicating with the articles REST endpoints
angular.module('aventon').factory('Aventon', ['$resource',
	function($resource) {
		return $resource('aventon/:aventonId/:method', {
			aventonId: '@_id'
		}, {
			update: {
				method: 'PUT'
			},
			paginate: {
				method: 'GET',
				params: {'method' : 'paginate'},
				isArray: false
			},
			paginateAdmin: {
				method: 'GET',
				params: {'method' : 'paginateAdmin'},
				isArray: false
			}
		});
	}
]);