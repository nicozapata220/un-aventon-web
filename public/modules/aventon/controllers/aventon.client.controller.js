'use strict';

angular.module('aventon').controller('AventonController', ['$scope', '$stateParams', '$location', 'Authentication', 'Aventon', '$http', '$rootScope', 'Demands',
	function($scope, $stateParams, $location, Authentication, Aventon, $http, $rootScope, Demands) {
		$scope.authentication = Authentication;
        $scope.defineDate = false;
        var validateAddress = false;
        $scope.price_aventon = 0;

        $scope.dir = "";
        $scope.dir2 = "";

        $scope.setDefineDate=function(value){
            $scope.defineDate = value ? true : false;
        };

		$scope.minDate = $scope.minDate ? null : new Date();

		$scope.unsetSeatPrice = function(){
			$scope.seat_price = 0;
		};
		
		$scope.create = function() {
			$http.get('/users/' + $scope.authentication.user._id )
			.success(function(u){
				$scope.authentication.user = u;
				// ACTIVAR
				//if($scope.authentication.user.credit < $scope.price_aventon){
				if(1!=1){
					$rootScope.alerts.push({type: 'danger', msg:'Necesita como mínimo'+ $scope.price_aventon + 'creditos para realizar esta publicación'});
				}else{
					if(validateAddress){
						var tags_search = JSON.parse(JSON.stringify($scope.cities));
						//var tags_pos = JSON.parse(JSON.stringify($scope.pos_cities));
						var pos_obj = new Object();
						var pos_extra = [];
						pos_extra[0] = $scope.direccion.lng;
						pos_extra[1] = $scope.direccion.lat;
						pos_obj.pos = pos_extra;
						$scope.pos_cities.push(pos_obj);

						var pos_obj = new Object();
						var pos_extra = [];
						pos_extra[0] = $scope.direcciondos.lng;
						pos_extra[1] = $scope.direcciondos.lat;
						pos_obj.pos = pos_extra;
						$scope.pos_cities.push(pos_obj);
						
						var tags_pos = $scope.pos_cities;
						
						tags_search.push($scope.direccion.direccion);
						tags_search.push($scope.direcciondos.direccion);

						var pos_start = [];
						pos_start[0] = $scope.direccion.lng;
						pos_start[1] = $scope.direccion.lat;
						
						var pos_end = [];
						pos_end[0] = $scope.direcciondos.lng;
						pos_end[1] = $scope.direcciondos.lat;
			
						
						var aventon = new Aventon({
							start_place: $scope.direccion.direccion,
							end_place: $scope.direcciondos.direccion,
							start_hour: $scope.start_hour,
							pos_start: pos_start,
							pos_end: pos_end,
							end_hour: $scope.end_hour,
							start_day: $scope.start_day,
							end_day: $scope.end_day,
							places_list: $scope.cities,
							seats_available: $scope.seats_available,
							more_info: $scope.more_info,
							recurrent_travel: $scope.recurrent_travel,
							frequency: $scope.frequency,
							patent_show: $scope.patent_show,
							user: $scope.authentication.user._id,
							tags_search : tags_search,
							tags_pos : tags_pos,
							time_flexibility: $scope.time_flexibility,
							from_hour: $scope.from_hour,
							until_hour: $scope.until_hour,
							from_day: $scope.from_day,
							until_day: $scope.until_day,
							from_hour_end: $scope.from_hour_end,
							until_hour_end: $scope.until_hour_end,
							from_day_end: $scope.from_day_end,
							until_day_end: $scope.until_day_end,
							private_price: $scope.private_price,
							seat_price: $scope.seat_price
						});
						
			      aventon.$save(function(response) {
							$location.path('/profile/viajes');
							$http.get('/configs')
							.success(function(configs){
								var creditNew = -configs[0].price_aventon;
								$http.post('/addCredit/'+ $scope.authentication.user._id,
								{
									"credit" : creditNew
								})
								.success(function(user){
									$scope.authentication.user = user;
								});
							});
							
							$rootScope.alerts.push({type: 'info', msg:'El aventón fue creado con éxito'});
							
							$http.post('/sendEmail',{
								email: $scope.authentication.user.email,
								subject:'Aventón creado - UnAventon',
								content: 'Has creado un aventón.'
							}).success(function(){
							});
						}, function(errorResponse) {
							$scope.error = errorResponse.data.message;
						});
			        }
					else{
						$rootScope.alerts.push({type: 'danger', msg:'La ubicación no es válida'});
					}	
				}
			})
			.error(function(){
				$rootScope.alerts.push({type: 'danger', msg:'Hubo un error, intentelo nuevamente'});
			});
		};


		$scope.idRemove = "";

		$scope.changeIdRemove = function(id){
			$scope.idRemove = id;
		};

		$scope.removeAventon = function(id){
			$http.delete('aventon/' + id)
			.success(function(oldAventon){
				$rootScope.alerts.push({type: 'success', msg:'Aventón eliminado correctamente'});
				$scope.aventonesByUser();
				$http.post('/demandsAcceptedByAventon', {aventonId : id})
				.success(function(demands){
					for (var i = demands.length - 1; i >= 0; i--) {
						var userD = demands[i].user;
						$http.post('/notifications',{
							type: 'cancelAventon',
							user: userD._id,
							anexo_user : oldAventon.user._id					
						})
						.success(function(){
							$http.post('/sendEmail',{
								email: userD.email,
								subject:'Aventón cancelado - UnAventon',
								content: 'El usuario '+$scope.authentication.user.displayName+' ha cancelado el aventón ofrecido.'
							}).success(function(){
								console.log("Mail Enviado");
							});
						});

						$http.put('/demands/'+demands[i]._id,{
							state : 'Cancelada'
						})
					}; //endFor
				});
			});

		};

		$scope.remove = function(aventon) {
			if (aventon) {
                aventon.$remove();

				for (var i in $scope.aventones) {
					if ($scope.aventones[i] === article) {
						$scope.aventones.splice(i, 1);
					}
				}
			} else {
				$scope.aventones.$remove(function() {
					$location.path('aventon');
				});
			}
		};

		$scope.update = function() {
			var aventon = $scope.aventon;

			aventon.$update(function() {
				$rootScope.alerts.push({type: 'success', msg:'El aventón se actualizó con éxito'});
			}, function(errorResponse) {
				$rootScope.alerts.push({type: 'danger', msg:'No se  ha podido realizar el cambio, intente nuevamente'});
			});
		};

		$scope.find = function() {
			$scope.aventones = Articles.query();
		};

		$scope.findOne = function() {
			Aventon.get({
				aventonId: $stateParams.aventonId
			}, function(a){
				$scope.aventon = a;
				$scope.dir = $scope.aventon.start_place;
				$scope.dir2 = $scope.aventon.end_place;
				$scope.cities = $scope.aventon.places_list;
			});

		};

		$scope.openFirst = function($event) {
			$event.preventDefault();
    		$event.stopPropagation();
			$scope.opened = true;
			
		};

		$scope.openSecond = function($event) {
			$event.preventDefault();
    		$event.stopPropagation();
			$scope.openedTwo = true;
			
		};

		$scope.openThird = function($event) {
			$event.preventDefault();
    		$event.stopPropagation();
			$scope.openedThree = true;
		};

		$scope.openFourth = function($event) {
			$event.preventDefault();
    		$event.stopPropagation();
			$scope.openedFour = true;
			
		};

		$scope.openFive = function($event) {
			$event.preventDefault();
    		$event.stopPropagation();
			$scope.openedFive = true;
			
		};

		$scope.openSix = function($event) {
			$event.preventDefault();
    		$event.stopPropagation();
			$scope.openedSix = true;
			
		};

		$scope.cities = [];
		$scope.pos_cities = [];
		$scope.newCity = "";
		$scope.indexUpdate = "-1";

		$scope.addCity = function(nc){
			if(($scope.newCity =! true || $scope.newCity != '') && $scope.indexUpdate == "-1"){
				$scope.cities.push(nc.direccion); 
				var pos_obj = new Object();
				var pos = [];
				pos[0] = nc.lng;
				pos[1] = nc.lat;
				pos_obj.pos = pos;
				$scope.pos_cities.push(pos_obj); 
			}
			if($scope.indexUpdate != "-1")
				$scope.cities[$scope.indexUpdate] = nc.direccion;
			$scope.calcRoute();
			$scope.newCity = '';
			$scope.indexUpdate = "-1";
		}

		$scope.preUpdateCity = function(city){
			$scope.newCity = city;
			$scope.indexUpdate = $scope.cities.indexOf(city);
		}

		$scope.removeCity = function(indexCity) {
			$scope.cities.splice(indexCity,1);
			$scope.pos_cities.splice(indexCity,1);
			$scope.calcRoute();
		};

		$scope.getIndexCity = function(cityLabel) {
			return $scope.cities.indexOf(cityLabel);
		};

		

		$scope.aventonesByUser = function(){
			$http.post('/aventonesByUser',{
				'userId' : $scope.authentication.user._id
			})
			.success(function(aventones){
				$scope.aventones = aventones;
			});
		};

		$scope.getInvitation = function(id){
			var res = [];
			for (var i = 0; i < $scope.allInvitations.length; i++) {
				if($scope.allInvitations[i].index = id)
					res = $scope.allInvitations[i].invitations;
			};
			console.log(res);
			return res;
		};

		$scope.invitationsByAventon = function(id, cb){
			$http.post('/invitationsByIdAventon',{
				idAventon : id
			})
			.success(function(invitations){
				cb(invitations,id);
			});
		};

		$scope.receiveInvitations = function(){
			$http.get('/invitationsToMe')
			.success(function(data){
				$scope.invitations = data;
			})
		};

		$scope.invitationsMade = function(){
			$http.get('/invitationsSent')
			.success(function(data){
				$scope.invitationsMade = data;
			})
		};

		$scope.rejectInvitation = function(idInvitation){
			$http.put('/invitations/'+idInvitation,{
				state : 'rejected',
				responded: true
			})
			.success(function(){
				$scope.receiveInvitations();
				$rootScope.alerts.push({type: 'success', msg:'Usted ha aceptado la invitación'});
			});
		};

		$scope.acceptInvitation = function(idInvitation,idAventon){
			$http.get('/getAventon/'+idAventon)
			.success(function(aventon){
				$http.post('/demandsAcceptedQuantityAventon',{idAventon:idAventon})
				.success(function(total){
					if(total.total >= aventon.seats_available){
						$rootScope.alerts.push({type: 'danger', msg:'Los lugares han sido ocupados'});	
						$scope.rejectInvitation(idInvitation);
					}else{
						//creo una demanda y la acepto
						var demand = new Demands({
							user: $scope.authentication.user._id,
							aventon: idAventon,
							state: "Aceptada"
						});
						demand.$save(function(response) {
							if(response.message){
								$rootScope.alerts.push({type: 'danger', msg:'Debe esperar a que el usuario responda a la solicitud'});
							}else{ 
								$http.put('/invitations/'+idInvitation,{
									state : 'accepted',
									responded: true
								})
								.success(function(){
									$scope.receiveInvitations();
									$rootScope.alerts.push({type: 'success', msg:'Usted ha aceptado la invitación'});
								});
							};				
						}, function(errorResponse) {
							$scope.error = errorResponse.data.message;
						});
					}
				})	
			});			
		};

		//ANOTHER SERVICE
		var directionsDisplay;
		var directionsService = new google.maps.DirectionsService();
		var map;
		var initView = false;
		$scope.initView = function() {
			initView = true;
			Aventon.get({
				aventonId: $stateParams.aventonId
			}, function(aventon) {
				$scope.aventon = aventon;
				calcRouteView(aventon);				
			});
		}

		$scope.consultFinished = false;

		$scope.saveComment = function(){
        	//tomar datos del comentario y mandar a object
        	var question = $scope.question;
        	var consultation = new Object();
        	consultation.question = question;
        	consultation.user_name = $scope.authentication.user.displayName;
        	consultation.user_id = $scope.authentication.user._id;

        	//pedir el aventon sin popular, insertar el comment y verificar array empty
    		if(!$scope.aventon.consultations)
				aventon.consultations = [];
			$scope.aventon.consultations.push(consultation);
			//save del aventon y verificar success o error con notify 
        	$scope.aventon.$update(function() {
        		$scope.initView();
        		$scope.question = "";
        		$scope.consultFinished = true;
				$rootScope.alerts.push({type: 'success', msg:'La consulta se ha guardado con éxito'});
			}, function(errorResponse) {
				$rootScope.alerts.push({type: 'danger', msg:'No se  ha podido realizar la consulta, intente nuevamente'});
			});
        };

        $scope.passVisible = function(id){
        	for (var i = 0; i < $scope.aventon.consultations.length; i++) {
        		if($scope.aventon.consultations[i]._id == id)
        			$scope.aventon.consultations[i].state = 'visible';
        	};
        	$scope.aventon.$update(function() {
        		$scope.initView();
				$rootScope.alerts.push({type: 'success', msg:'Es visible ahora el mensaje'});
			}, function(errorResponse) {
				$rootScope.alerts.push({type: 'danger', msg:'La acción ha fallado, intente nuevamente'});
			});
        };
		
		$scope.passBlocked = function(id){
        	for (var i = 0; i < $scope.aventon.consultations.length; i++) {
        		if($scope.aventon.consultations[i]._id == id)
        			$scope.aventon.consultations[i].state = 'blocked';
        	};
        	$scope.aventon.$update(function() {
        		$scope.initView();
				$rootScope.alerts.push({type: 'success', msg:'Es oculto ahora el mensaje'});
			}, function(errorResponse) {
				$rootScope.alerts.push({type: 'danger', msg:'La acción ha fallado, intente nuevamente'});
			});
        };

        $scope.responseQuestion = function(id){
        	for (var i = 0; i < $scope.aventon.consultations.length; i++) {
        		if($scope.aventon.consultations[i]._id == id){
        			$scope.aventon.consultations[i].answer = this.answer;
        			$scope.aventon.consultations[i].date_answer = new Date();
        			$scope.aventon.consultations[i].responded = true;
        		}
        	};
        	$scope.aventon.$update(function() {
        		$scope.initView();
				$rootScope.alerts.push({type: 'success', msg:'Ha respondido la consulta'});
			}, function(errorResponse) {
				$rootScope.alerts.push({type: 'danger', msg:'La acción ha fallado, intente nuevamente'});
			});
        };

		$scope.doubleControl = function(){
			if(($scope.direccion ))
				if(($scope.direcciondos )){
					$scope.calcRoute();
				}
		};


		$scope.initialize = function() {
			$http.get('/configs')
			.success(function(configs){
				$scope.price_aventon = configs[0].price_aventon;
			});
			$scope.user = $scope.authentication.user;
			directionsDisplay = new google.maps.DirectionsRenderer();
			var argentina = new google.maps.LatLng(-33.119318, -64.391015);
			var mapOptions = {
			zoom: 3,
			center: argentina
			}
			map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
			directionsDisplay.setMap(map);
		}

		function calcRouteView(av) {
 		  $scope.initialize();
 		  var aventon = av;
		  var start = aventon.start_place;
		  var end = aventon.end_place;
		  var waypts = [];
		  for (var i = aventon.places_list.length - 1; i >= 0; i--) {
		  	waypts.push({
		  		location: aventon.places_list[i],
		  		stopover:true
		  	});
		  };
		  var request = {
		      origin: start,
		      destination: end,
		      waypoints: waypts,
		      optimizeWaypoints: true,
		      travelMode: google.maps.TravelMode.DRIVING
		  };
		  directionsService.route(request, function(response, status) {
		    if (status == google.maps.DirectionsStatus.OK) {
		      directionsDisplay.setDirections(response);
		    }
		  });
		}

		$scope.calcRoute = function() {
		  var start = this.direccion.direccion;
		  var end = this.direcciondos.direccion;
		  var waypts = [];
		  for (var i = $scope.cities.length - 1; i >= 0; i--) {
		  	waypts.push({
		  		location: $scope.cities[i],
		  		stopover:true
		  	});
		  };
		  var request = {
		      origin: start,
		      destination: end,
		      waypoints: waypts,
		      optimizeWaypoints: true,
		      travelMode: google.maps.TravelMode.DRIVING
		  };
		  directionsService.route(request, function(response, status) {
		    if (status == google.maps.DirectionsStatus.OK) {
		      validateAddress = true;
		      directionsDisplay.setDirections(response);
		    }
		    else
		      validateAddress = false;

		  });
		};

			google.maps.event.addDomListener(window, 'load', $scope.initialize);
		
	}


]);

// NEW CONTROLLER

angular.module('aventon').controller('AventonViewController', ['$scope', '$stateParams', '$location', 'Authentication', 'Aventon', '$http', '$rootScope', 'Demands',
	function($scope, $stateParams, $location, Authentication, Aventon, $http, $rootScope, Demands) {
		$scope.authentication = Authentication;
        $scope.defineDate = false;
        var validateAddress = false;
        $scope.price_aventon = 0;

        $scope.dir = "";
        $scope.dir2 = "";

        
		//ANOTHER SERVICE
		var directionsDisplay;
		var directionsService = new google.maps.DirectionsService();
		var map;
		var initView = false;
		$scope.initView = function() {
			initView = true;
			Aventon.get({
				aventonId: $stateParams.aventonId
			}, function(aventon) {
				$scope.aventon = aventon;
				calcRouteView(aventon);				
			});
		}

		$scope.consultFinished = false;

		$scope.saveComment = function(){
        	//tomar datos del comentario y mandar a object
        	var question = $scope.question;
        	var consultation = new Object();
        	consultation.question = question;
        	consultation.user_name = $scope.authentication.user.displayName;
        	consultation.user_id = $scope.authentication.user._id;

        	//pedir el aventon sin popular, insertar el comment y verificar array empty
    		if(!$scope.aventon.consultations)
				aventon.consultations = [];
			$scope.aventon.consultations.push(consultation);
			//save del aventon y verificar success o error con notify 
        	$scope.aventon.$update(function() {
        		$scope.initView();
        		$scope.question = "";
        		$scope.consultFinished = true;
				$rootScope.alerts.push({type: 'success', msg:'La consulta se ha guardado con éxito'});
			}, function(errorResponse) {
				$rootScope.alerts.push({type: 'danger', msg:'No se  ha podido realizar la consulta, intente nuevamente'});
			});
        };

        $scope.passVisible = function(id){
        	for (var i = 0; i < $scope.aventon.consultations.length; i++) {
        		if($scope.aventon.consultations[i]._id == id)
        			$scope.aventon.consultations[i].state = 'visible';
        	};
        	$scope.aventon.$update(function() {
        		$scope.initView();
				$rootScope.alerts.push({type: 'success', msg:'Es visible ahora el mensaje'});
			}, function(errorResponse) {
				$rootScope.alerts.push({type: 'danger', msg:'La acción ha fallado, intente nuevamente'});
			});
        };
		
		$scope.passBlocked = function(id){
        	for (var i = 0; i < $scope.aventon.consultations.length; i++) {
        		if($scope.aventon.consultations[i]._id == id)
        			$scope.aventon.consultations[i].state = 'blocked';
        	};
        	$scope.aventon.$update(function() {
        		$scope.initView();
				$rootScope.alerts.push({type: 'success', msg:'Es oculto ahora el mensaje'});
			}, function(errorResponse) {
				$rootScope.alerts.push({type: 'danger', msg:'La acción ha fallado, intente nuevamente'});
			});
        };

        $scope.responseQuestion = function(id){
        	for (var i = 0; i < $scope.aventon.consultations.length; i++) {
        		if($scope.aventon.consultations[i]._id == id){
        			$scope.aventon.consultations[i].answer = this.answer;
        			$scope.aventon.consultations[i].date_answer = new Date();
        			$scope.aventon.consultations[i].responded = true;
        		}
        	};
        	$scope.aventon.$update(function() {
        		$scope.initView();
				$rootScope.alerts.push({type: 'success', msg:'Ha respondido la consulta'});
			}, function(errorResponse) {
				$rootScope.alerts.push({type: 'danger', msg:'La acción ha fallado, intente nuevamente'});
			});
        };

		$scope.doubleControl = function(){
			if(($scope.direccion ))
				if(($scope.direcciondos )){
					$scope.calcRoute();
				}
		};


		$scope.initialize = function() {
			$http.get('/configs')
			.success(function(configs){
				$scope.price_aventon = configs[0].price_aventon;
			});
			$scope.user = $scope.authentication.user;
			directionsDisplay = new google.maps.DirectionsRenderer();
			var argentina = new google.maps.LatLng(-33.119318, -64.391015);
			var mapOptions = {
			zoom: 3,
			center: argentina
			}
			map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
			directionsDisplay.setMap(map);
		}

		function calcRouteView(av) {
 		  $scope.initialize();
 		  var aventon = av;
		  var start = aventon.start_place;
		  var end = aventon.end_place;
		  var waypts = [];
		  for (var i = aventon.places_list.length - 1; i >= 0; i--) {
		  	waypts.push({
		  		location: aventon.places_list[i],
		  		stopover:true
		  	});
		  };
		  var request = {
		      origin: start,
		      destination: end,
		      waypoints: waypts,
		      optimizeWaypoints: true,
		      travelMode: google.maps.TravelMode.DRIVING
		  };
		  directionsService.route(request, function(response, status) {
		    if (status == google.maps.DirectionsStatus.OK) {
		      directionsDisplay.setDirections(response);
		    }
		  });
		}

		
	}


]);