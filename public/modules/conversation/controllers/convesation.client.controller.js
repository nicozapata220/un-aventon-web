'use strict';

angular.module('conversations').controller('ConversationsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Conversations','$http', '$rootScope', 'ChatMsgs', '$interval',
	function($scope, $stateParams, $location, Authentication, Conversations, $http, $rootScope, ChatMsgs, $interval) {
		$scope.authentication = Authentication;


		$scope.create = function(idReceiver) {
			var message = new Messages({
				content: this.content,
				subject: this.subject,
				user_sender: $scope.authentication.user._id,
				user_receiver: idReceiver
			});
			message.$save(function(response) {
				

				$scope.content = '';
				$scope.subject = '';

				//$location.path('usersNewsLetter/' + response._id);
				//mostrar cartel
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.remove = function(article) {
			if (article) {
				article.$remove();

				for (var i in $scope.articles) {
					if ($scope.articles[i] === article) {
						$scope.articles.splice(i, 1);
					}
				}
			} else {
				$scope.article.$remove(function() {
					$location.path('articles');
				});
			}
		};

		$scope.update = function(idReview,comment,valoration) {
			$http.get('/reviews/'+idReview)
			.success(function(review){
				review.comment = comment;
				review.valoration = valoration;
				review.$update(function(){
					alert('Usted ha realizado una devolución correctamente');
				}, function(errorResponse){
					alert('Hubo un error, intentelo nuevamente');
				});
			})
			.error(function(){
				alert('Hubo un error, intentelo nuevamente');
			});
		};

		$scope.find = function() {
			$scope.articles = Articles.query();
		};

		$scope.findOne = function() {
			$scope.message = Messages.get({
				messageId: $stateParams.messageId
			});
		};

		$scope.scroll_to = function(id){
            var div = document.getElementById(id);
            div.scrollTop = div.scrollHeight;
        };

        $scope.myContacts = function() {
        	$http.post('/conversationsByUser', {
        		'userId' : $scope.authentication.user._id
        	})
        	.success(function(conversations){
        		for (var i = conversations.length - 1; i >= 0; i--) {
        			for (var j = conversations[i]["members"].length - 1; j >= 0; j--) {
        				if(conversations[i]["members"][j]._id != $scope.authentication.user._id)
        					conversations[i]["contact"] = conversations[i]["members"][j]; 
        			};
        		};
        		$scope.conversations = conversations;
        		console.log($scope.conversations);
        	})
        };

        
        $scope.refreshChat = function(){
        	if($scope.currentConversationId != "nothing" && $scope.currentContact != "nothing")
        		$scope.msgsBetweenUsers($scope.currentContact, $scope.currentConversationId)
        };

        $interval($scope.refreshChat,5000);

        $scope.msgsBetweenUsers = function(contact, conversationId){
			$scope.showChatWindow = true;
			$scope.currentContact = contact;
			$scope.currentConversationId = conversationId;
			$http.post('/chatMsgByConversationId', {
				"conversationId" : conversationId
			})
			.success(function(msgs){
				$scope.msgs = msgs;
				$scope.scroll_to("chatDiv");
			});

		};
		$scope.currentConversationId = "nothing";
		$scope.currentContact = "nothing";
		$scope.showChatWindow = false;

		$scope.createMsg = function(){
			var chatMsg = new ChatMsgs({
				content: this.content,
				user_a: $scope.authentication.user._id,
				user_b: $scope.currentContact._id,
				conversation : $scope.currentConversationId,
				displayName_a : $scope.authentication.user.displayName,
				displayName_b : $scope.currentContact.displayName
			});
			this.content = '';

			chatMsg.$save(function(response) {
				$scope.msgsBetweenUsers($scope.currentContact, $scope.currentConversationId);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};


	}
]);
