'use strict';

//Articles service used for communicating with the articles REST endpoints
angular.module('conversations').factory('Conversations', ['$resource',
	function($resource) {
		return $resource('conversations/:conversationId', {
			conversationId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);