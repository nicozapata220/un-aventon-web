'use strict';

// Setting up route
angular.module('conversations').config(['$stateProvider',
	function($stateProvider) {
		// Conversation state routing
		$stateProvider.
		state('chat', {
            url: '/chat',
            templateUrl: 'modules/conversation/views/chat.client.view.html'
        });
	}
]);