'use strict';

//Articles service used for communicating with the articles REST endpoints
angular.module('chatMsgs').factory('ChatMsgs', ['$resource',
	function($resource) {
		return $resource('chatMsgs/:chatMsgId', {
			chatMsgId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);