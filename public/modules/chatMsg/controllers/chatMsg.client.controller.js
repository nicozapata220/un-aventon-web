'use strict';

angular.module('chatMsgs').controller('ChatMsgsController', ['$scope', '$stateParams', '$location', 'Authentication', 'ChatMsgs','$http', '$rootScope',
	function($scope, $stateParams, $location, Authentication, ChatMsgs, $http, $rootScope) {
		$scope.authentication = Authentication;

		$scope.create = function(idReceiver) {
			var message = new Messages({
				content: this.content,
				subject: this.subject,
				user_sender: $scope.authentication.user._id,
				user_receiver: idReceiver
			});
			message.$save(function(response) {
				

				$scope.content = '';
				$scope.subject = '';

				//$location.path('usersNewsLetter/' + response._id);
				//mostrar cartel
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.remove = function(article) {
			if (article) {
				article.$remove();

				for (var i in $scope.articles) {
					if ($scope.articles[i] === article) {
						$scope.articles.splice(i, 1);
					}
				}
			} else {
				$scope.article.$remove(function() {
					$location.path('articles');
				});
			}
		};

		$scope.update = function(idReview,comment,valoration) {
			$http.get('/reviews/'+idReview)
			.success(function(review){
				review.comment = comment;
				review.valoration = valoration;
				review.$update(function(){
					alert('Usted ha realizado una devolución correctamente');
				}, function(errorResponse){
					alert('Hubo un error, intentelo nuevamente');
				});
			})
			.error(function(){
				alert('Hubo un error, intentelo nuevamente');
			});
		};

		$scope.find = function() {
			$scope.articles = Articles.query();
		};

		$scope.findOne = function() {
			$scope.message = Messages.get({
				messageId: $stateParams.messageId
			});
		};

		
	}
]);
