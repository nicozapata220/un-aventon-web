'use strict';

// Setting up route
angular.module('chatMsgs').config(['$stateProvider',
	function($stateProvider) {
		// chatMsg state routing
		$stateProvider.
		state('createChatMessages', {
			url: '/messageChat/create',
			templateUrl: 'modules/chatMsg/views/create-messages.client.view.html'
		});
	}
]);