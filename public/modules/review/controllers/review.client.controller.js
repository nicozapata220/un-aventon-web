'use strict';

angular.module('reviews').controller('ReviewsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Reviews','$http' , 'Users', '$rootScope',
	function($scope, $stateParams, $location, Authentication, Reviews, $http, Users, $rootScope) {
		$scope.authentication = Authentication;

		$scope.create = function() {
			var userNewsLetter = new UsersNewsLetter({
				email: this.email
			});
			userNewsLetter.$save(function(response) {
				//$location.path('usersNewsLetter/' + response._id);

				$scope.email = '';
				$scope.success = 'Se ha suscripto correctamente';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.remove = function(article) {
			if (article) {
				article.$remove();

				for (var i in $scope.articles) {
					if ($scope.articles[i] === article) {
						$scope.articles.splice(i, 1);
					}
				}
			} else {
				$scope.article.$remove(function() {
					$location.path('articles');
				});
			}
		};

		$scope.update = function(idReview,comment,valoration) {
			Reviews.get({
				reviewId : idReview
			},function(review){
				review.comment = comment;
				review.valoration = valoration.valoration;
				review.state = 'checked';
				review.$update(function(reviewUpdated){
					$scope.pendingReviews();
					$scope.reviewsList();
					$rootScope.alerts.push({type: 'success', msg:'Usted ha realizado una devolución correctamente'});
					Users.get({
						userId : reviewUpdated.user_receiver
					},function(user){
						user.review_point += reviewUpdated.valoration;
						user.review_count += 1;
						user.$update(function(){
							console.log("usuario actualizado");
						});
					});
				}, function(errorResponse){
					$rootScope.alerts.push({type: 'danger', msg:'Hubo un error, intente nuevamente'});
				});
			});

		};

		$scope.find = function() {
			$scope.articles = Articles.query();
		};

		$scope.findOne = function() {
			$scope.article = Articles.get({
				articleId: $stateParams.articleId
			});
		};

		$scope.pendingReviews = function() {
			$http.post('/pendingReviews', {
				"user_origin" : $scope.authentication.user._id,
				"user_receiver" : $stateParams.userId
			})
			.success(function(pendings){
				$scope.pendings = pendings;
			});
		};

		$scope.valorationValues = [
			{valoration:'5', text: 'Todo Excelente'},
			{valoration:'4', text: 'Muy Buen viaje'},
			{valoration:'3', text: 'Sin problemas'},
			{valoration:'2', text: 'Disconforme'},
			{valoration:'1', text: 'No recomendable'}
		];

		$scope.valoration = [
			'No recomendable',
			'Disconforme',
			'Sin problemas',
			'Muy Buen viaje',
			'Todo Excelente',
		];

		$scope.reviewsList = function(){
			$http.post('/reviewsList',{
				userId : $stateParams.userId
			})
			.success(function(reviews){
				$scope.reviews =  reviews;
			})
		};

		$scope.getArray = function(n){
			var input = new Array();
			for (var i = n ; i >= 0; i--) {
				input.push(i);
			};
			return input;
		};

		$scope.myReviews = function(){
			$http.post('/reviewsList',{
				userId : $scope.authentication.user._id
			})
			.success(function(reviews){
				$scope.reviews =  reviews;
			})	
		}


	}
]);