'use strict';

//Articles service used for communicating with the articles REST endpoints
angular.module('reviews').factory('Reviews', ['$resource',
	function($resource) {
		return $resource('reviews/:reviewId', {
			reviewId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);