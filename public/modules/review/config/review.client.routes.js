'use strict';

// Setting up route
angular.module('reviews').config(['$stateProvider',
	function($stateProvider) {
		// Articles state routing
		$stateProvider.
		state('createReviews', {
			url: '/reviews/create',
			templateUrl: 'modules/reviews/views/create-reviews.client.view.html'
		});
	}
]);