'use strict';

// Setting up route
angular.module('packs').config(['$stateProvider',
	function($stateProvider) {
		// Packs state routing
		$stateProvider.
		state('credits', {
            url: '/credits',
            templateUrl: 'modules/pack/views/credits.client.view.html'
        })
        .state('returnMP', {
            url: '/returnMP',
            templateUrl: 'modules/pack/views/returnMP.client.view.html'
        });
	}
]);