'use strict';

angular.module('packs').controller('PacksController', ['$scope', '$stateParams', '$location', 'Authentication', 'Packs', '$http', '$rootScope', '$window', '$timeout',
	function($scope, $stateParams, $location, Authentication, Packs, $http, $rootScope, $window, $timeout) {
		$scope.authentication = Authentication;


		$scope.createPack = function() {
			var pack = new Packs({
				nombre: this.name,
				cantidad_pines: this.cantidad_pines,
				precio : this.precio
			});
			pack.$save(function(response) {
				$location.path("packListAdmin");

				$scope.nombre = '';
				$scope.cantidad_pines = '';
				$scope.precio = '';

			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.create = function(aventon) {
			var demand = new Demands({
				user: $scope.authentication.user._id,
				aventon: aventon._id
			});
			demand.$save(function(response) {
				$rootScope.alerts.push({type: 'info', msg:'Le has enviado el pedido de viaje...'});
				$http.post('/notifications',{
					aventon: aventon._id,
					type : 'trip',
					anexo_user : $scope.authentication.user._id,
					comment: 'Un usuario quiere aprovechar su aventón',
					user: aventon.user._id,
				})
				.success(function(){
					$http.post('/sendEmail',{
						email: aventon.user.email,
						subject:'Un usuario quiere aprovechar tu aventón',
						content: 'El usuario '+$scope.authentication.user.displayName+' quiere aprovechar tu aventón'
					}).success(function(){
						console.log("Mail Enviado");
					});

				});

				
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.remove = function(pack) {
			if (pack) {
				pack.$remove();

				for (var i in $scope.packs) {
					if ($scope.packs[i] === pack) {
						$scope.packs.splice(i, 1);
					}
				}
			} else {
				$scope.pack.$remove(function() {
					$location.path('packListAdmin');
				});
			}
		};

		$scope.update = function() {
			var article = $scope.article;

			article.$update(function() {
				$location.path('articles/' + article._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.find = function() {
			$scope.packs = Packs.query();
		};

		$scope.findOne = function() {
			$scope.article = Articles.get({
				articleId: $stateParams.articleId
			});
		};

		$scope.buyPack = function(packId) {
			$window.location.href = '/packs/'+packId+'/buy';
		};	

		$scope.controlReturnMP = function(){
			var mercado_pago_id = $location.search().external_reference;
			var estado = $location.search().collection_status;
			$http.post('/changeStatusTransaction',{
				"mercado_pago_id" : mercado_pago_id ,
				"estado" : estado 
			})
			.success(function(t){
				$http.post('/searchPack',{
					"id_pack" : t.pack
				})
				.success(function(pack){
					var creditNew = pack.cantidad_pines;
					$http.post('/addCredit/'+ $scope.authentication.user._id,
					{
						"credit" : creditNew
					})
					.success(function(user){
						$scope.authentication.user = user;
						$rootScope.alerts.push({type: 'success', msg:'Se ha completado la transacción, espere a ser redirigido'});
						$timeout(function(){
							$location.path('/');
						}, 10000);		
					});
						
				});
				
			});
		};
	
	}
]);