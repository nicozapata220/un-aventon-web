'use strict';

// Setting up route
angular.module('usersNewsLetter').config(['$stateProvider',
	function($stateProvider) {
		// Articles state routing
		$stateProvider.
		state('createUserNewsLetter', {
			url: '/usersNewsLetter/create',
			templateUrl: 'modules/usersNewsLetter/views/create-userNewsLetter.client.view.html'
		})
		.state('authenticateUserNewsletter', {
			url : '/authenticateUserNewsLetter/:userNewsLetterId',
			templateUrl : 'modules/usersNewsLetter/views/authenticate-user-newsletter.client.view.html'
		});
	}
]);