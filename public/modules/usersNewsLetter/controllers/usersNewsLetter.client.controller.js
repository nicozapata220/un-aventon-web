'use strict';

angular.module('usersNewsLetter').controller('UsersNewsLetterController', ['$scope', '$stateParams', '$location', 'Authentication', 'UsersNewsLetter', '$rootScope', '$http',
	function($scope, $stateParams, $location, Authentication, UsersNewsLetter, $rootScope, $http) {
		$scope.authentication = Authentication;

		$scope.create = function() {
			var userNewsLetter = new UsersNewsLetter({
				email: this.email
			});

			userNewsLetter.$save(function(response) {
				$http.post('/authenticateusersend', {
					email : userNewsLetter.email,
					id : userNewsLetter._id
				});
				$rootScope.alerts.push({type: 'success', msg:'Suscripción realizada, revise su correo para completar la suscripción'});
				$scope.email = '';	
			}, function(errorResponse) {
				$scope.email = '';
				console.log(errorResponse);
				$rootScope.alerts.push({type: 'danger', msg:'No se ha podido suscribir: ' + errorResponse.data.message});
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.preCreate = function() {
			if(this.unsuscribe){
				$http.post("/deleteSuscriptorByEmail", {
					"email" : this.email
				})
				.success(function(data){
					$rootScope.alerts.push({type: 'success', msg:'Suscripción eliminada'});	
					$scope.email = '';
				});
			}
			else{
				$scope.create();
			}
		};


		$scope.remove = function(article) {
			if (article) {
				article.$remove();

				for (var i in $scope.articles) {
					if ($scope.articles[i] === article) {
						$scope.articles.splice(i, 1);
					}
				}
			} else {
				$scope.article.$remove(function() {
					$location.path('articles');
				});
			}
		};

		$scope.update = function() {
			var article = $scope.article;

			article.$update(function() {
				$location.path('articles/' + article._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.find = function() {
			$scope.articles = Articles.query();
		};

		$scope.findOne = function() {
			$scope.article = Articles.get({
				articleId: $stateParams.articleId
			});
		};

		$scope.showUnsuscribeValue = false;

		$scope.showUnsuscribe = function() {
			$scope.showUnsuscribeValue = true;			
		};


		$scope.enabledUser = function(){
			console.log($stateParams.userNewsLetterId);
			$http.post('/authenticateUser/'+ $stateParams.userNewsLetterId)
			.success(function(){
				$rootScope.alerts.push({type: 'success', msg:'El usuario ha sido activado correctamente'});
				$location.path('/');
			})
			.error(function(){
				$rootScope.alerts.push({type: 'error', msg:'El usuario no se ha activado, intente nuevamente'});
			});
		};


	}
]);