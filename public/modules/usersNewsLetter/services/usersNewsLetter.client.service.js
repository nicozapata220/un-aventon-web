'use strict';

//Articles service used for communicating with the articles REST endpoints
angular.module('usersNewsLetter').factory('UsersNewsLetter', ['$resource',
	function($resource) {
		return $resource('usersNewsLetter/:userNewsLetterId', {
			userNewsLetterId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);