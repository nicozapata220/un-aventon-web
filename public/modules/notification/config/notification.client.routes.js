'use strict';

// Setting up route
angular.module('notifications').config(['$stateProvider',
	function($stateProvider) {
		// Notification state routing
		$stateProvider.
		state('exampleNotifications', {
			url: '/notifications/create',
			templateUrl: 'modules/notifications/views/create-notifications.client.view.html'
		});
	}
]);