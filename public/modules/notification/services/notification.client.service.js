'use strict';

//Articles service used for communicating with the articles REST endpoints
angular.module('notifications').factory('Notifications', ['$resource',
	function($resource) {
		return $resource('notifications/:notificationId', {
			notificationId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);