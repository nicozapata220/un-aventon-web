'use strict';

angular.module('notifications').controller('NotificationsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Notifications', '$http','$rootScope','$interval',
	function($scope, $stateParams, $location, Authentication, Notifications, $http,$rootScope, $interval) {
		$scope.authentication = Authentication;

		$scope.create = function() {
			var userNewsLetter = new UsersNewsLetter({
				email: this.email
			});
			userNewsLetter.$save(function(response) {
				//$location.path('usersNewsLetter/' + response._id);

				$scope.email = '';
				$scope.success = 'Se ha suscripto correctamente';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.remove = function(article) {
			if (article) {
				article.$remove();

				for (var i in $scope.articles) {
					if ($scope.articles[i] === article) {
						$scope.articles.splice(i, 1);
					}
				}
			} else {
				$scope.article.$remove(function() {
					$location.path('articles');
				});
			}
		};

		$scope.update = function() {
			var article = $scope.article;

			article.$update(function() {
				$location.path('articles/' + article._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.find = function() {
			$scope.articles = Articles.query();
		};

		$scope.findOne = function() {
			$scope.article = Articles.get({
				articleId: $stateParams.articleId
			});
		};

		$scope.myNotifications = function(){
			if($scope.authentication.user){
				$http.post('/notificationsByUser',{
					userId : $scope.authentication.user._id
				})
				.success(function(notifications){
					$rootScope.notifications = notifications;
					var count = 0;
					for (var i = notifications.length - 1; i >= 0; i--) {
						if(notifications[i].state == 'unChecked')
							count++;
					};
					$rootScope.notificaciones = count;
				});
			};
		};

		$scope.allMyNotifications = function(){
			if($scope.authentication.user){
				$http.post('/allNotificationsByUser',{
					userId : $scope.authentication.user._id
				})
				.success(function(notifications){
					$scope.allNotifications = notifications;
				});
			};
		};

		$interval($scope.myNotifications,60000);

		$scope.checkNotifications = function(){
			var notifications = $rootScope.notifications;
			for (var i = notifications.length - 1; i >= 0; i--) {
				$http.put('/notifications/'+notifications[i]._id,{
					state : 'checked'
				})
				
			};
			$rootScope.notificaciones = "0";
		}

	}
]);