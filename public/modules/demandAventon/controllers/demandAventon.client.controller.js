'use strict';

angular.module('demandsAventon').controller('DemandsAventonController', ['$scope', '$stateParams', '$location', 'Authentication', 'DemandsAventon', '$http', '$rootScope', '$filter',
	function($scope, $stateParams, $location, Authentication, DemandsAventon, $http, $rootScope,$filter) {
		$scope.authentication = Authentication;
		$scope.minDate = $scope.minDate ? null : new Date();
		
		$scope.create = function(aventon) {
			$scope.pos_cities = [];
			var pos_obj = new Object();
			var pos_extra = [];
			pos_extra[0] = $scope.direccion.lng;
			pos_extra[1] = $scope.direccion.lat;
			pos_obj.pos = pos_extra;
			$scope.pos_cities.push(pos_obj);

			var pos_obj = new Object();
			var pos_extra = [];
			pos_extra[0] = $scope.direcciondos.lng;
			pos_extra[1] = $scope.direcciondos.lat;
			pos_obj.pos = pos_extra;
			$scope.pos_cities.push(pos_obj);
			
			var tags_pos = $scope.pos_cities;

			var demandAventon = new DemandsAventon({
				start_place: this.direccion.direccion,
				end_place: this.direcciondos.direccion,
				day: this.start_day,				
				comment: this.comment,
				time_flexibility: this.time_flexibility,
				from_hour: this.from_hour,
				until_hour: this.until_hour,
				from_day: this.from_day,
				until_day: this.until_day,
				recurrent_travel: this.recurrent_travel,
				frequency: this.frequency,
				luggage_type: this.luggage_type,
				tags_pos: tags_pos,				
				user: this.authentication.user._id
				
			});
			demandAventon.$save(function(response) {
				$scope.dir = '';
				$scope.dir2 = '';
				$scope.start_day = '';
				$scope.comment = '';
				$scope.time_flexibility = '';
				$scope.from_hour= '';
				$scope.until_hour= '';
				$scope.from_day = '';
				$scope.until_day = '';
				$scope.recurrent_travel = '';
				$scope.frequency = '';
				$scope.luggage_type = '';
							

				$rootScope.alerts.push({type: 'success', msg:'La demanda de aventón fue creada con éxito!'});
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.openThird = function($event) {
			$event.preventDefault();
    		$event.stopPropagation();
			$scope.openedThree = true;
		};

		$scope.openFourth = function($event) {
			$event.preventDefault();
    		$event.stopPropagation();
			$scope.openedFour = true;
			
		};

		$scope.giveMeToolTipText = function(demand){
			var text = "";
			if(demand.recurrent_travel)
				text+="Viaje frecuente: "+demand.frequency+"\n";
			else
				text+="No es un viaje frecuente \n";

			if(demand.time_flexibility){
				text+="Día y hora flexible: \n Desde "+$filter('date')(demand.from_day)+" hasta "+$filter('date')(demand.until_day)+"\n";
				text+="Desde "+ demand.from_hour+"hs hasta "+demand.until_hour+"hs \n"; 
			}
			text+="Tipo de equipaje: "+demand.luggage_type;
			return text;
		};


		$scope.initCreate = function(){
			$scope.user = $scope.authentication.user;
		};

		$scope.idRemove = "";

		$scope.changeIdRemove = function(id){
			$scope.idRemove = id;
			console.log("aca");
		};

		$scope.removeDemand = function(id){
			console.log(id);
			$http.delete('/demandsAventon/'+id).
			success(function(data){
				$scope.myListDemandsAventon();
			});
		}

		$scope.remove = function(article) {
			if (article) {
				article.$remove();

				for (var i in $scope.articles) {
					if ($scope.articles[i] === article) {
						$scope.articles.splice(i, 1);
					}
				}
			} else {
				$scope.article.$remove(function() {
					$location.path('articles');
				});
			}
		};

		$scope.goEdit = function(id){
			$location.path('demandAventon/edit/'+id);
		};

		$scope.update = function() {
			var demand = $scope.demandA;
			if($scope.dir != demand.start_place)
				demand.start_place = this.direccion.direccion;
			if($scope.dir2 != demand.end_place)
				demand.end_place = this.direcciondos.direccion;

			$http.put('/demandsAventon/'+$scope.demandA._id, demand)
			.success(function(data){
				$rootScope.alerts.push({type: 'success', msg:'La demanda de aventón fue actualizada con éxito!'});
			})
			.error(function(){
				$rootScope.alerts.push({type: 'danger', msg:'La demanda de aventón no fue actualizada!'});	
			});
		};

		$scope.find = function() {
			$scope.articles = Articles.query();
		};

		$scope.findOne = function() {
			$scope.user = $scope.authentication.user;
			$http.get('/demandsAventon/'+$stateParams.demandAventonId)
			.success(function(data){
				$scope.demandA = data;
				$scope.dir = $scope.demandA.start_place;
				$scope.dir2 = $scope.demandA.end_place;
			});
			
		};

		$scope.openFirst = function($event) {
			$event.preventDefault();
    		$event.stopPropagation();
			$scope.opened = true;
			
		};

		$scope.listDemandsAventon = function(){
			$http.get('/demandAventon')
			.success(function(data){
				$scope.demandsAventon = data;
			});
		};

		$scope.myListDemandsAventon = function(){
			$http.get('/myDemandsAventon')
			.success(function(data){
				$scope.demandsAventon = data;
			});	
		};

		$scope.myAventonesValid = function(){
			$http.post('/aventonesValidsByUser',{
				"idUser" : $scope.authentication.user._id
			})
			.success(function(aventones){
				$scope.myAventones = aventones;
			});
		};

		$scope.aventonInvita;

		$scope.invitar = function(value,invitado){
			if(!value || value == "null")
				$rootScope.alerts.push({type: 'danger', msg:'Seleccione un aventón o cree uno nuevo'});	
			else{
				$http.post('/invitations',{
					user_sender: $scope.authentication.user._id,
					user_receiver: invitado,
					aventon: value
				})
				.success(function(invitation){
					$rootScope.alerts.push({type: 'success', msg:'Se ha enviado la invitación'});
					console.log(value);
					$http.post('/notifications',{
						aventon: value,
						type: 'invitation',
						anexo_user: $scope.authentication.user._id,
						comment: 'Un usuario te ha invitado',
						user: invitado
					});
				})
				.error(function(){
					$rootScope.alerts.push({type: 'danger', msg:'Invitación enviada anteriormente'});	
				})
		};

		}



	}
]);