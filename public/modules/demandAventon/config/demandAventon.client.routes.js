'use strict';

// Setting up route
angular.module('demandsAventon').config(['$stateProvider',
	function($stateProvider) {
		// Articles state routing
		$stateProvider.
		state('createDemandAventon', {
			url: '/demandAventon/create',
			templateUrl: 'modules/demandAventon/views/create-aventonDemand.client.view.html'
		})
		.state('editDemandAventon', {
			url: '/demandAventon/edit/:demandAventonId',
			templateUrl: 'modules/demandAventon/views/edit-aventonDemand.client.view.html'
		});
	}
]);