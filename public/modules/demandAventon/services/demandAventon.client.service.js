'use strict';

//Demands service used for communicating with the articles REST endpoints
angular.module('demandsAventon').factory('DemandsAventon', ['$resource',
	function($resource) {
		return $resource('demandsAventon/:demandAventonId', {
			demandAventonId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);