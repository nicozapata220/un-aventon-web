'use strict';
angular.module('core').controller('HeaderController', ['$scope', 'Authentication', 'Menus', '$rootScope', '$timeout', '$http',
	function($scope, Authentication, Menus, $rootScope, $timeout, $http) {
		$scope.authentication = Authentication;
		$scope.isCollapsed = false;
		$scope.menu = Menus.getMenu('topbar');

		$scope.toggleCollapsibleMenu = function() {
			$scope.isCollapsed = !$scope.isCollapsed;
		};

		// Collapsing the menu after navigation
		$scope.$on('$stateChangeSuccess', function() {
			$scope.isCollapsed = false;
		});

		//Time to hide alert
		$rootScope.timeAlert = 15000;

		$rootScope.alerts = [];
		/*$rootScope.alerts = [
			{type: 'info', msg : 'Hola soy una alerta de información'},
			{type: 'success', msg : 'Se ha creado su aventón con exito'},
			{type: 'danger', msg : 'Se crasheo el sistema!'}

		];*/
		// $rootScope.alerts.push({type: 'info', msg:'Mi mensaje'});

		$rootScope.$watch('alerts', function(newValue, oldValue){
			$rootScope.alerts = newValue;
			$timeout(function(){
				$rootScope.alerts = [];
			},$rootScope.timeAlert)
		});

		$rootScope.closeAlert = function(index) {
			$rootScope.alerts.splice(index, 1);
		};

		//faq
		$scope.faq = [true];

		$scope.getFaqs = function(){
			$http.get('/faqs')
			.success(function(data){
				$scope.faqs = data;
			});
		};
	}
]);
