'use strict';

angular.module('core').controller('SearchController', ['$scope', '$stateParams', '$location', 'Authentication', 'Aventon', '$http', '$rootScope', '$timeout',
	function($scope, $stateParams, $location, Authentication, Aventon, $http, $rootScope, $timeout) {
		$scope.authentication = Authentication;

        /*
         * Filters: Last, Hots
         */
        $scope.aventonFilter = 'normal';

        $scope.setAventonFilter = function(newFilter){
            $scope.aventonFilter = newFilter;
            $scope.find();

        };


		$scope.totalItems = 0;
		$scope.perPage = 10;
		$scope.currentPage = 1;
		$scope.maxSizeAventones = 10;

		$scope.find = function() {
			$scope.searchOn = "false"
			var page = $scope.currentPage;
			Aventon.paginate({page: page, tipodebusqueda: $scope.aventonFilter}, function(response) {
				$scope.aventones = response.aventones;
				$scope.totalItems = response.total;
			});
		};


	  $scope.setPage = function (pageNo) {
	    $scope.currentPage = pageNo;
	  };



		$scope.findOne = function() {
			$scope.aventon = Aventon.get({
				aventonId: $stateParams.articleId
			});
		};

		$scope.minDate = $scope.minDate ? null : new Date();
		$scope.searchOn = "false";
		$scope.start_lat  = "";
		$scope.start_lng  = "";
		$scope.end_lat  = "";
		$scope.end_lng  = "";
		$scope.aventonesBackup = "";

		$scope.searchAventon = function(){
			$scope.searchOn = "true";
			var start_lat  = this.direccion.lat;
			var start_lng  = this.direccion.lng;
			var end_lat  = this.direcciondos.lat;
			var end_lng  = this.direcciondos.lng;
			$scope.start_lat  = start_lat;
			$scope.start_lng  = start_lng;
			$scope.end_lat  = end_lat;
			$scope.end_lng  = end_lng;
			var fecha = this.fecha;
			var direccion = this.direccion;
			var direcciondos = this.direcciondos;

			$http.post('/searchAventon', {
				'date' : fecha,
				'start_place': direccion,
				'end_place': direcciondos
			})
			.success(function(aventones){
				for (var i = 0; i < aventones.length; i++) {
					aventones[i].tipo = "conductor";
					aventones[i].distancias = [];
					for (var j = 0; j < aventones[i].tags_pos.length; j++) {
						var distancia_value = distancia(aventones[i].tags_pos[j].pos[1],aventones[i].tags_pos[j].pos[0],start_lat,start_lng);
						aventones[i].distancias.push(distancia_value);
						distancia_value = distancia(aventones[i].tags_pos[j].pos[1],aventones[i].tags_pos[j].pos[0],end_lat,end_lng);
						aventones[i].distancias.push(distancia_value);
					};
				};

				$http.post('/searchDemandAventon', {
					'date' : fecha,
					'start_place': direccion,
					'end_place': direcciondos
				})
				.success(function(daventones){
					for (var i = 0; i < daventones.length; i++) {
						daventones[i].tipo = "pasajero";
						daventones[i].start_day = daventones[i].day;
						daventones[i].distancias = [];
						for (var j = 0; j < daventones[i].tags_pos.length; j++) {
							var distancia_value = distancia(daventones[i].tags_pos[j].pos[1],daventones[i].tags_pos[j].pos[0],start_lat,start_lng);
							daventones[i].distancias.push(distancia_value);
							distancia_value = distancia(daventones[i].tags_pos[j].pos[1],daventones[i].tags_pos[j].pos[0],end_lat,end_lng);
							daventones[i].distancias.push(distancia_value);
						};
					};

					for (var i = 0; i < daventones.length; i++) {
						aventones.push(daventones[i]); //mezclo aventones con demandas
					};

					if(aventones.length==0){
						var pos_obj = new Object();
						var pos_extra = [];
						pos_extra[0] = $scope.start_lng;
						pos_extra[1] = $scope.start_lat;
						pos_obj.pos = pos_extra;
						$http.post('/addFailPlace/'+ $scope.authentication.user._id,
						{
							"place" : pos_obj
						})
						.success(function(){
							var pos_obj = new Object();
							var pos_extra = [];
							pos_extra[0] = $scope.end_lng;
							pos_extra[1] = $scope.end_lat;
							pos_obj.pos = pos_extra;
							$http.post('/addFailPlace/'+ $scope.authentication.user._id,
							{
								"place" : pos_obj
							});
						});
					};

					$scope.aventonesBackup = aventones;
					$scope.aventones = aventones;
				});				
			})
			.error(function(){
				$rootScope.alerts.push({type: 'danger', msg:'Ha ocurrido un error, intente luego'});
			});
		};


		window.fbAsyncInit = function() {
	        FB.init({
	          	appId      : '922797427753319',
	          	xfbml      : true,
	          	version    : 'v2.1'
	        });
	     };

	    (function(d, s, id){
         	var js, fjs = d.getElementsByTagName(s)[0];
     		if (d.getElementById(id)) {return;}
		        js = d.createElement(s); js.id = id;
		        js.src = "//connect.facebook.net/en_US/sdk.js";
		        fjs.parentNode.insertBefore(js, fjs);
	    }(document, 'script', 'facebook-jssdk'));

	    $scope.shareLocation = function(){
	    	if (navigator.geolocation) {
			    navigator.geolocation.getCurrentPosition(function(position){
			    	FB.ui({
					  method: 'share',
					  href: 'http://maps.google.com/maps?q=loc:'+position.coords.latitude+','+position.coords.longitude
					}, function(response){});
			    }, $scope.errorFunction);
			} 
	    };
		


		$scope.errorFunction = function(){
			console.log("Error en el pedido de datos");
		};

		$scope.openFirst = function($event) {
			$event.preventDefault();
    		$event.stopPropagation();
			$scope.opened = true;
			
		};

		function rad(x) {return x*Math.PI/180;}

		function distancia(lat1,lon1,lat2,lon2){
			var R     = 6378.137;//Radio de la tierra en km
			var dLat  = rad( lat2 - lat1 );
			var dLong = rad( lon2 - lon1 );

			var a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(rad(lat1)) * Math.cos(rad(lat2)) * Math.sin(dLong/2) * Math.sin(dLong/2);
			var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
			var d = R * c;

			return d.toFixed(3);//Retorna tres decimales
		};

		$scope.search_type = "Aventon";

    $scope.changeType = function(){
    	if($scope.search_type == "Aventon")
    		$scope.search_type = "Pasajero";
    	else
    		$scope.search_type = "Aventon";
    };


    $scope.distanciaFiltrada = function(aventon){
    	return true;
    };

    $scope.distanciaSeleccionada = 'todos';

    $scope.filtrar = function(){
    	var tipoModel;
    	if($scope.pasajero && $scope.conductor)
    		tipoModel = 'todos';
    	else
    		if($scope.pasajero)
    			tipoModel = 'pasajero';
    		else
    			tipoModel = 'conductor';

		//reestablezco todos los aventones de la busqueda
		var aventones = $scope.aventonesBackup;
		var aventonesRes = [];
		
		

		var included = false;
		var distancia = 0;
		switch($scope.distanciaSeleccionada){
			case 'minus1':
				distancia = 1;
				break;
			case 'minus3':
				distancia = 3;
				break;
			case 'minus500':
				distancia = 0.5;
				break;
			case 'todos':
				distancia = 10;
				break;
		}
		
	
		for (var i = 0; i < aventones.length; i++) {
			included = false;
			for (var j = 0; j < aventones[i].distancias.length; j++) {
				if(aventones[i].tipo == tipoModel || tipoModel == 'todos'){
					console.log(aventones[i].distancias[j]);
					if(aventones[i].distancias[j] < distancia && !included){
        				aventonesRes.push(aventones[i]);
        				included = true;
        			}
        		}
      		};
      	};
		
		//fin de filtros
		$scope.aventones = aventonesRes;        	
      };

       
	}
]);