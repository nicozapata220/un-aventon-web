'use strict';


angular.module('core').controller('HomeController', ['$scope', 'Authentication','$state',
    function($scope, Authentication, $state) {
        // This provides Authentication context.
        $scope.authentication = Authentication;
        $scope.redirect = function(){
            var user = $scope.authentication.user;
            if(user){
                if(user.role == 'user')
                    $state.go('search');
                else
                    $state.go('adminProfile.usersList')
            }else{
                $state.go('signup');
            }
        };

        $scope.redirect();

        
    }
]);