'use strict';

// Setting up route
angular.module('core').config(['$stateProvider', '$urlRouterProvider',
	function($stateProvider, $urlRouterProvider) {
		// Redirect to home view when route not found
		$urlRouterProvider.otherwise('/');

		// Home state routing
		$stateProvider.
		state('home', {
			url: '/',
			templateUrl: 'modules/core/views/home.client.view.html'
		})
        .state('search', {
            url: '/search',
            templateUrl: 'modules/core/views/search.client.view.html'
        })
        .state('activate', {
            url: '/emailvalidate/:id/:token',
            templateUrl: 'modules/core/views/activate.client.view.html'
        })
        .state('faq', {
			url: '/faq',
			templateUrl: 'modules/core/views/faq.client.view.html'
		});
	}
]);
