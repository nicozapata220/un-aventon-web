'use strict';

// Init the application configuration module for AngularJS application
var ApplicationConfiguration = (function() {
	// Init module configuration options
	var applicationModuleName = 'Un Aventon';
	var applicationModuleVendorDependencies = ['ngResource', 'angular-loading-bar','ngAnimate', 'ui.router', 'ui.bootstrap', 'ui.utils', 'checklist-model', 'textAngular', 'angularFileUpload'];

	// Add a new vertical module
	var registerModule = function(moduleName, dependencies) {
		// Create angular module
		angular.module(moduleName, dependencies || []);

		// Add the module to the AngularJS configuration file
		angular.module(applicationModuleName).requires.push(moduleName);
	};

	return {
		applicationModuleName: applicationModuleName,
		applicationModuleVendorDependencies: applicationModuleVendorDependencies,
		registerModule: registerModule
	};
})();
'use strict';

//Start by defining the main module and adding the module dependencies
angular.module(ApplicationConfiguration.applicationModuleName, ApplicationConfiguration.applicationModuleVendorDependencies);

// Setting HTML5 Location Mode
angular.module(ApplicationConfiguration.applicationModuleName).config(['$locationProvider',
	function($locationProvider) {
		$locationProvider.hashPrefix('!');
	}
]);

//Then define the init function for starting up the application
angular.element(document).ready(function() {
	//Fixing facebook bug with redirect
	if (window.location.hash === '#_=_') window.location.hash = '#!';

	//Then init the app
	angular.bootstrap(document, [ApplicationConfiguration.applicationModuleName]);
});
'use strict';

// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('articles');
'use strict';

// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('aventon');
'use strict';

// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('chatMsgs');
'use strict';

// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('conversations');
'use strict';

// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('core');
'use strict';

// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('demands');
'use strict';

// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('demandsAventon');
'use strict';

// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('messages');
'use strict';

// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('notifications');
'use strict';

// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('packs');
'use strict';

// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('reviews');
'use strict';

// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('users');

'use strict';

// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('usersNewsLetter');
'use strict';

// Configuring the Articles module
angular.module('articles').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Articles', 'articles', 'dropdown', '/articles(/create)?');
		Menus.addSubMenuItem('topbar', 'articles', 'List Articles', 'articles');
		Menus.addSubMenuItem('topbar', 'articles', 'New Article', 'articles/create');
	}
]);
'use strict';

// Setting up route
angular.module('articles').config(['$stateProvider',
	function($stateProvider) {
		// Articles state routing
		$stateProvider.
		state('listArticles', {
			url: '/articles',
			templateUrl: 'modules/articles/views/list-articles.client.view.html'
		}).
		state('createArticle', {
			url: '/articles/create',
			templateUrl: 'modules/articles/views/create-article.client.view.html'
		}).
		state('viewArticle', {
			url: '/articles/:articleId',
			templateUrl: 'modules/articles/views/view-article.client.view.html'
		}).
		state('editArticle', {
			url: '/articles/:articleId/edit',
			templateUrl: 'modules/articles/views/edit-article.client.view.html'
		});
	}
]);
'use strict';

angular.module('articles').controller('ArticlesController', ['$scope', '$stateParams', '$location', 'Authentication', 'Articles',
	function($scope, $stateParams, $location, Authentication, Articles) {
		$scope.authentication = Authentication;

		$scope.create = function() {
			var article = new Articles({
				title: this.title,
				content: this.content
			});
			article.$save(function(response) {
				$location.path('articles/' + response._id);

				$scope.title = '';
				$scope.content = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.remove = function(article) {
			if (article) {
				article.$remove();

				for (var i in $scope.articles) {
					if ($scope.articles[i] === article) {
						$scope.articles.splice(i, 1);
					}
				}
			} else {
				$scope.article.$remove(function() {
					$location.path('articles');
				});
			}
		};

		$scope.update = function() {
			var article = $scope.article;

			article.$update(function() {
				$location.path('articles/' + article._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.find = function() {
			$scope.articles = Articles.query();
		};

		$scope.findOne = function() {
			$scope.article = Articles.get({
				articleId: $stateParams.articleId
			});
		};
	}
]);
'use strict';

//Articles service used for communicating with the articles REST endpoints
angular.module('articles').factory('Articles', ['$resource',
	function($resource) {
		return $resource('articles/:articleId', {
			articleId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);
'use strict';

// Setting up route
angular.module('aventon').config(['$stateProvider',
	function($stateProvider) {
		// Articles state routing
		$stateProvider.
		state('createAventon', {
			url: '/aventon/nuevo',
			templateUrl: 'modules/aventon/views/create-aventon.client.view.html'
		}).
		state('viewAventon', {
			url: '/aventon/:aventonId',
			templateUrl: 'modules/aventon/views/view-aventon.client.view.html'
		}).
		state('editAventon', {
			url: '/aventon/:aventonId/edit',
			templateUrl: 'modules/aventon/views/edit-aventon.client.view.html'
		});
	}
]);

/*
Uso complicado de este modulo
<google-places location=direccion identificador="1" id="google_places_ac1"type="text" class="a-place"/>
el id debe iniciar 'google_places_ac' y el final debe coincidir con identificador

*/
angular.module('core').
    directive('googlePlaces', function(){
        return {
            restrict:'E',
            replace:true,
            //transclude:true,
            scope: {location:"=", identificador:"@", callback:"&"},
            template: '<input class="" placeholder="Direccion"  type="text" />',
            link: function($scope, elm, attrs){
                var id = "#google_places_ac"+$scope.identificador;
                var autocomplete = new google.maps.places.Autocomplete($(id)[0], {});
                google.maps.event.addListener(autocomplete, 'place_changed', function() {
                    var place = autocomplete.getPlace();
                    $scope.location = {};
                    $scope.location.lat = place.geometry.location.lat();
                    $scope.location.lng = place.geometry.location.lng();
                    $scope.location.direccion = place.formatted_address;
                    $scope.$apply();
                    $scope.callback();
                });
            }
        }
    })
'use strict';

angular.module('aventon').controller('AventonController', ['$scope', '$stateParams', '$location', 'Authentication', 'Aventon', '$http', '$rootScope', 'Demands',
	function($scope, $stateParams, $location, Authentication, Aventon, $http, $rootScope, Demands) {
		$scope.authentication = Authentication;
        $scope.defineDate = false;
        var validateAddress = false;
        $scope.price_aventon = 0;

        $scope.dir = "";
        $scope.dir2 = "";

        $scope.setDefineDate=function(value){
            $scope.defineDate = value ? true : false;
        };

		$scope.minDate = $scope.minDate ? null : new Date();

		$scope.unsetSeatPrice = function(){
			$scope.seat_price = 0;
		};
		
		$scope.create = function() {
			$http.get('/users/' + $scope.authentication.user._id )
			.success(function(u){
				$scope.authentication.user = u;
				// ACTIVAR
				//if($scope.authentication.user.credit < $scope.price_aventon){
				if(1!=1){
					$rootScope.alerts.push({type: 'danger', msg:'Necesita como mínimo'+ $scope.price_aventon + 'creditos para realizar esta publicación'});
				}else{
					if(validateAddress){
						var tags_search = JSON.parse(JSON.stringify($scope.cities));
						//var tags_pos = JSON.parse(JSON.stringify($scope.pos_cities));
						var pos_obj = new Object();
						var pos_extra = [];
						pos_extra[0] = $scope.direccion.lng;
						pos_extra[1] = $scope.direccion.lat;
						pos_obj.pos = pos_extra;
						$scope.pos_cities.push(pos_obj);

						var pos_obj = new Object();
						var pos_extra = [];
						pos_extra[0] = $scope.direcciondos.lng;
						pos_extra[1] = $scope.direcciondos.lat;
						pos_obj.pos = pos_extra;
						$scope.pos_cities.push(pos_obj);
						
						var tags_pos = $scope.pos_cities;
						
						tags_search.push($scope.direccion.direccion);
						tags_search.push($scope.direcciondos.direccion);

						var pos_start = [];
						pos_start[0] = $scope.direccion.lng;
						pos_start[1] = $scope.direccion.lat;
						
						var pos_end = [];
						pos_end[0] = $scope.direcciondos.lng;
						pos_end[1] = $scope.direcciondos.lat;
			
						
						var aventon = new Aventon({
							start_place: $scope.direccion.direccion,
							end_place: $scope.direcciondos.direccion,
							start_hour: $scope.start_hour,
							pos_start: pos_start,
							pos_end: pos_end,
							end_hour: $scope.end_hour,
							start_day: $scope.start_day,
							end_day: $scope.end_day,
							places_list: $scope.cities,
							seats_available: $scope.seats_available,
							more_info: $scope.more_info,
							recurrent_travel: $scope.recurrent_travel,
							frequency: $scope.frequency,
							patent_show: $scope.patent_show,
							user: $scope.authentication.user._id,
							tags_search : tags_search,
							tags_pos : tags_pos,
							time_flexibility: $scope.time_flexibility,
							from_hour: $scope.from_hour,
							until_hour: $scope.until_hour,
							from_day: $scope.from_day,
							until_day: $scope.until_day,
							from_hour_end: $scope.from_hour_end,
							until_hour_end: $scope.until_hour_end,
							from_day_end: $scope.from_day_end,
							until_day_end: $scope.until_day_end,
							private_price: $scope.private_price,
							seat_price: $scope.seat_price
						});
						
			      aventon.$save(function(response) {
							$location.path('/profile/viajes');
							$http.get('/configs')
							.success(function(configs){
								var creditNew = -configs[0].price_aventon;
								$http.post('/addCredit/'+ $scope.authentication.user._id,
								{
									"credit" : creditNew
								})
								.success(function(user){
									$scope.authentication.user = user;
								});
							});
							
							$rootScope.alerts.push({type: 'info', msg:'El aventón fue creado con éxito'});
							
							$http.post('/sendEmail',{
								email: $scope.authentication.user.email,
								subject:'Aventón creado - UnAventon',
								content: 'Has creado un aventón.'
							}).success(function(){
							});
						}, function(errorResponse) {
							$scope.error = errorResponse.data.message;
						});
			        }
					else{
						$rootScope.alerts.push({type: 'danger', msg:'La ubicación no es válida'});
					}	
				}
			})
			.error(function(){
				$rootScope.alerts.push({type: 'danger', msg:'Hubo un error, intentelo nuevamente'});
			});
		};


		$scope.idRemove = "";

		$scope.changeIdRemove = function(id){
			$scope.idRemove = id;
		};

		$scope.removeAventon = function(id){
			$http.delete('aventon/' + id)
			.success(function(oldAventon){
				$rootScope.alerts.push({type: 'success', msg:'Aventón eliminado correctamente'});
				$scope.aventonesByUser();
				$http.post('/demandsAcceptedByAventon', {aventonId : id})
				.success(function(demands){
					for (var i = demands.length - 1; i >= 0; i--) {
						var userD = demands[i].user;
						$http.post('/notifications',{
							type: 'cancelAventon',
							user: userD._id,
							anexo_user : oldAventon.user._id					
						})
						.success(function(){
							$http.post('/sendEmail',{
								email: userD.email,
								subject:'Aventón cancelado - UnAventon',
								content: 'El usuario '+$scope.authentication.user.displayName+' ha cancelado el aventón ofrecido.'
							}).success(function(){
								console.log("Mail Enviado");
							});
						});

						$http.put('/demands/'+demands[i]._id,{
							state : 'Cancelada'
						})
					}; //endFor
				});
			});

		};

		$scope.remove = function(aventon) {
			if (aventon) {
                aventon.$remove();

				for (var i in $scope.aventones) {
					if ($scope.aventones[i] === article) {
						$scope.aventones.splice(i, 1);
					}
				}
			} else {
				$scope.aventones.$remove(function() {
					$location.path('aventon');
				});
			}
		};

		$scope.update = function() {
			var aventon = $scope.aventon;

			aventon.$update(function() {
				$rootScope.alerts.push({type: 'success', msg:'El aventón se actualizó con éxito'});
			}, function(errorResponse) {
				$rootScope.alerts.push({type: 'danger', msg:'No se  ha podido realizar el cambio, intente nuevamente'});
			});
		};

		$scope.find = function() {
			$scope.aventones = Articles.query();
		};

		$scope.findOne = function() {
			Aventon.get({
				aventonId: $stateParams.aventonId
			}, function(a){
				$scope.aventon = a;
				$scope.dir = $scope.aventon.start_place;
				$scope.dir2 = $scope.aventon.end_place;
				$scope.cities = $scope.aventon.places_list;
			});

		};

		$scope.openFirst = function($event) {
			$event.preventDefault();
    		$event.stopPropagation();
			$scope.opened = true;
			
		};

		$scope.openSecond = function($event) {
			$event.preventDefault();
    		$event.stopPropagation();
			$scope.openedTwo = true;
			
		};

		$scope.openThird = function($event) {
			$event.preventDefault();
    		$event.stopPropagation();
			$scope.openedThree = true;
		};

		$scope.openFourth = function($event) {
			$event.preventDefault();
    		$event.stopPropagation();
			$scope.openedFour = true;
			
		};

		$scope.openFive = function($event) {
			$event.preventDefault();
    		$event.stopPropagation();
			$scope.openedFive = true;
			
		};

		$scope.openSix = function($event) {
			$event.preventDefault();
    		$event.stopPropagation();
			$scope.openedSix = true;
			
		};

		$scope.cities = [];
		$scope.pos_cities = [];
		$scope.newCity = "";
		$scope.indexUpdate = "-1";

		$scope.addCity = function(nc){
			if(($scope.newCity =! true || $scope.newCity != '') && $scope.indexUpdate == "-1"){
				$scope.cities.push(nc.direccion); 
				var pos_obj = new Object();
				var pos = [];
				pos[0] = nc.lng;
				pos[1] = nc.lat;
				pos_obj.pos = pos;
				$scope.pos_cities.push(pos_obj); 
			}
			if($scope.indexUpdate != "-1")
				$scope.cities[$scope.indexUpdate] = nc.direccion;
			$scope.calcRoute();
			$scope.newCity = '';
			$scope.indexUpdate = "-1";
		}

		$scope.preUpdateCity = function(city){
			$scope.newCity = city;
			$scope.indexUpdate = $scope.cities.indexOf(city);
		}

		$scope.removeCity = function(indexCity) {
			$scope.cities.splice(indexCity,1);
			$scope.pos_cities.splice(indexCity,1);
			$scope.calcRoute();
		};

		$scope.getIndexCity = function(cityLabel) {
			return $scope.cities.indexOf(cityLabel);
		};

		

		$scope.aventonesByUser = function(){
			$http.post('/aventonesByUser',{
				'userId' : $scope.authentication.user._id
			})
			.success(function(aventones){
				$scope.aventones = aventones;
			});
		};

		$scope.getInvitation = function(id){
			var res = [];
			for (var i = 0; i < $scope.allInvitations.length; i++) {
				if($scope.allInvitations[i].index = id)
					res = $scope.allInvitations[i].invitations;
			};
			console.log(res);
			return res;
		};

		$scope.invitationsByAventon = function(id, cb){
			$http.post('/invitationsByIdAventon',{
				idAventon : id
			})
			.success(function(invitations){
				cb(invitations,id);
			});
		};

		$scope.receiveInvitations = function(){
			$http.get('/invitationsToMe')
			.success(function(data){
				$scope.invitations = data;
			})
		};

		$scope.invitationsMade = function(){
			$http.get('/invitationsSent')
			.success(function(data){
				$scope.invitationsMade = data;
			})
		};

		$scope.rejectInvitation = function(idInvitation){
			$http.put('/invitations/'+idInvitation,{
				state : 'rejected',
				responded: true
			})
			.success(function(){
				$scope.receiveInvitations();
				$rootScope.alerts.push({type: 'success', msg:'Usted ha aceptado la invitación'});
			});
		};

		$scope.acceptInvitation = function(idInvitation,idAventon){
			$http.get('/getAventon/'+idAventon)
			.success(function(aventon){
				$http.post('/demandsAcceptedQuantityAventon',{idAventon:idAventon})
				.success(function(total){
					if(total.total >= aventon.seats_available){
						$rootScope.alerts.push({type: 'danger', msg:'Los lugares han sido ocupados'});	
						$scope.rejectInvitation(idInvitation);
					}else{
						//creo una demanda y la acepto
						var demand = new Demands({
							user: $scope.authentication.user._id,
							aventon: idAventon,
							state: "Aceptada"
						});
						demand.$save(function(response) {
							if(response.message){
								$rootScope.alerts.push({type: 'danger', msg:'Debe esperar a que el usuario responda a la solicitud'});
							}else{ 
								$http.put('/invitations/'+idInvitation,{
									state : 'accepted',
									responded: true
								})
								.success(function(){
									$scope.receiveInvitations();
									$rootScope.alerts.push({type: 'success', msg:'Usted ha aceptado la invitación'});
								});
							};				
						}, function(errorResponse) {
							$scope.error = errorResponse.data.message;
						});
					}
				})	
			});			
		};

		//ANOTHER SERVICE
		var directionsDisplay;
		var directionsService = new google.maps.DirectionsService();
		var map;
		var initView = false;
		$scope.initView = function() {
			initView = true;
			Aventon.get({
				aventonId: $stateParams.aventonId
			}, function(aventon) {
				$scope.aventon = aventon;
				calcRouteView(aventon);				
			});
		}

		$scope.consultFinished = false;

		$scope.saveComment = function(){
        	//tomar datos del comentario y mandar a object
        	var question = $scope.question;
        	var consultation = new Object();
        	consultation.question = question;
        	consultation.user_name = $scope.authentication.user.displayName;
        	consultation.user_id = $scope.authentication.user._id;

        	//pedir el aventon sin popular, insertar el comment y verificar array empty
    		if(!$scope.aventon.consultations)
				aventon.consultations = [];
			$scope.aventon.consultations.push(consultation);
			//save del aventon y verificar success o error con notify 
        	$scope.aventon.$update(function() {
        		$scope.initView();
        		$scope.question = "";
        		$scope.consultFinished = true;
				$rootScope.alerts.push({type: 'success', msg:'La consulta se ha guardado con éxito'});
			}, function(errorResponse) {
				$rootScope.alerts.push({type: 'danger', msg:'No se  ha podido realizar la consulta, intente nuevamente'});
			});
        };

        $scope.passVisible = function(id){
        	for (var i = 0; i < $scope.aventon.consultations.length; i++) {
        		if($scope.aventon.consultations[i]._id == id)
        			$scope.aventon.consultations[i].state = 'visible';
        	};
        	$scope.aventon.$update(function() {
        		$scope.initView();
				$rootScope.alerts.push({type: 'success', msg:'Es visible ahora el mensaje'});
			}, function(errorResponse) {
				$rootScope.alerts.push({type: 'danger', msg:'La acción ha fallado, intente nuevamente'});
			});
        };
		
		$scope.passBlocked = function(id){
        	for (var i = 0; i < $scope.aventon.consultations.length; i++) {
        		if($scope.aventon.consultations[i]._id == id)
        			$scope.aventon.consultations[i].state = 'blocked';
        	};
        	$scope.aventon.$update(function() {
        		$scope.initView();
				$rootScope.alerts.push({type: 'success', msg:'Es oculto ahora el mensaje'});
			}, function(errorResponse) {
				$rootScope.alerts.push({type: 'danger', msg:'La acción ha fallado, intente nuevamente'});
			});
        };

        $scope.responseQuestion = function(id){
        	for (var i = 0; i < $scope.aventon.consultations.length; i++) {
        		if($scope.aventon.consultations[i]._id == id){
        			$scope.aventon.consultations[i].answer = this.answer;
        			$scope.aventon.consultations[i].date_answer = new Date();
        			$scope.aventon.consultations[i].responded = true;
        		}
        	};
        	$scope.aventon.$update(function() {
        		$scope.initView();
				$rootScope.alerts.push({type: 'success', msg:'Ha respondido la consulta'});
			}, function(errorResponse) {
				$rootScope.alerts.push({type: 'danger', msg:'La acción ha fallado, intente nuevamente'});
			});
        };

		$scope.doubleControl = function(){
			if(($scope.direccion ))
				if(($scope.direcciondos )){
					$scope.calcRoute();
				}
		};


		$scope.initialize = function() {
			$http.get('/configs')
			.success(function(configs){
				$scope.price_aventon = configs[0].price_aventon;
			});
			$scope.user = $scope.authentication.user;
			directionsDisplay = new google.maps.DirectionsRenderer();
			var argentina = new google.maps.LatLng(-33.119318, -64.391015);
			var mapOptions = {
			zoom: 3,
			center: argentina
			}
			map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
			directionsDisplay.setMap(map);
		}

		function calcRouteView(av) {
 		  $scope.initialize();
 		  var aventon = av;
		  var start = aventon.start_place;
		  var end = aventon.end_place;
		  var waypts = [];
		  for (var i = aventon.places_list.length - 1; i >= 0; i--) {
		  	waypts.push({
		  		location: aventon.places_list[i],
		  		stopover:true
		  	});
		  };
		  var request = {
		      origin: start,
		      destination: end,
		      waypoints: waypts,
		      optimizeWaypoints: true,
		      travelMode: google.maps.TravelMode.DRIVING
		  };
		  directionsService.route(request, function(response, status) {
		    if (status == google.maps.DirectionsStatus.OK) {
		      directionsDisplay.setDirections(response);
		    }
		  });
		}

		$scope.calcRoute = function() {
		  var start = this.direccion.direccion;
		  var end = this.direcciondos.direccion;
		  var waypts = [];
		  for (var i = $scope.cities.length - 1; i >= 0; i--) {
		  	waypts.push({
		  		location: $scope.cities[i],
		  		stopover:true
		  	});
		  };
		  var request = {
		      origin: start,
		      destination: end,
		      waypoints: waypts,
		      optimizeWaypoints: true,
		      travelMode: google.maps.TravelMode.DRIVING
		  };
		  directionsService.route(request, function(response, status) {
		    if (status == google.maps.DirectionsStatus.OK) {
		      validateAddress = true;
		      directionsDisplay.setDirections(response);
		    }
		    else
		      validateAddress = false;

		  });
		};

			google.maps.event.addDomListener(window, 'load', $scope.initialize);
		
	}


]);

// NEW CONTROLLER

angular.module('aventon').controller('AventonViewController', ['$scope', '$stateParams', '$location', 'Authentication', 'Aventon', '$http', '$rootScope', 'Demands',
	function($scope, $stateParams, $location, Authentication, Aventon, $http, $rootScope, Demands) {
		$scope.authentication = Authentication;
        $scope.defineDate = false;
        var validateAddress = false;
        $scope.price_aventon = 0;

        $scope.dir = "";
        $scope.dir2 = "";

        
		//ANOTHER SERVICE
		var directionsDisplay;
		var directionsService = new google.maps.DirectionsService();
		var map;
		var initView = false;
		$scope.initView = function() {
			initView = true;
			Aventon.get({
				aventonId: $stateParams.aventonId
			}, function(aventon) {
				$scope.aventon = aventon;
				calcRouteView(aventon);				
			});
		}

		$scope.consultFinished = false;

		$scope.saveComment = function(){
        	//tomar datos del comentario y mandar a object
        	var question = $scope.question;
        	var consultation = new Object();
        	consultation.question = question;
        	consultation.user_name = $scope.authentication.user.displayName;
        	consultation.user_id = $scope.authentication.user._id;

        	//pedir el aventon sin popular, insertar el comment y verificar array empty
    		if(!$scope.aventon.consultations)
				aventon.consultations = [];
			$scope.aventon.consultations.push(consultation);
			//save del aventon y verificar success o error con notify 
        	$scope.aventon.$update(function() {
        		$scope.initView();
        		$scope.question = "";
        		$scope.consultFinished = true;
				$rootScope.alerts.push({type: 'success', msg:'La consulta se ha guardado con éxito'});
			}, function(errorResponse) {
				$rootScope.alerts.push({type: 'danger', msg:'No se  ha podido realizar la consulta, intente nuevamente'});
			});
        };

        $scope.passVisible = function(id){
        	for (var i = 0; i < $scope.aventon.consultations.length; i++) {
        		if($scope.aventon.consultations[i]._id == id)
        			$scope.aventon.consultations[i].state = 'visible';
        	};
        	$scope.aventon.$update(function() {
        		$scope.initView();
				$rootScope.alerts.push({type: 'success', msg:'Es visible ahora el mensaje'});
			}, function(errorResponse) {
				$rootScope.alerts.push({type: 'danger', msg:'La acción ha fallado, intente nuevamente'});
			});
        };
		
		$scope.passBlocked = function(id){
        	for (var i = 0; i < $scope.aventon.consultations.length; i++) {
        		if($scope.aventon.consultations[i]._id == id)
        			$scope.aventon.consultations[i].state = 'blocked';
        	};
        	$scope.aventon.$update(function() {
        		$scope.initView();
				$rootScope.alerts.push({type: 'success', msg:'Es oculto ahora el mensaje'});
			}, function(errorResponse) {
				$rootScope.alerts.push({type: 'danger', msg:'La acción ha fallado, intente nuevamente'});
			});
        };

        $scope.responseQuestion = function(id){
        	for (var i = 0; i < $scope.aventon.consultations.length; i++) {
        		if($scope.aventon.consultations[i]._id == id){
        			$scope.aventon.consultations[i].answer = this.answer;
        			$scope.aventon.consultations[i].date_answer = new Date();
        			$scope.aventon.consultations[i].responded = true;
        		}
        	};
        	$scope.aventon.$update(function() {
        		$scope.initView();
				$rootScope.alerts.push({type: 'success', msg:'Ha respondido la consulta'});
			}, function(errorResponse) {
				$rootScope.alerts.push({type: 'danger', msg:'La acción ha fallado, intente nuevamente'});
			});
        };

		$scope.doubleControl = function(){
			if(($scope.direccion ))
				if(($scope.direcciondos )){
					$scope.calcRoute();
				}
		};


		$scope.initialize = function() {
			$http.get('/configs')
			.success(function(configs){
				$scope.price_aventon = configs[0].price_aventon;
			});
			$scope.user = $scope.authentication.user;
			directionsDisplay = new google.maps.DirectionsRenderer();
			var argentina = new google.maps.LatLng(-33.119318, -64.391015);
			var mapOptions = {
			zoom: 3,
			center: argentina
			}
			map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
			directionsDisplay.setMap(map);
		}

		function calcRouteView(av) {
 		  $scope.initialize();
 		  var aventon = av;
		  var start = aventon.start_place;
		  var end = aventon.end_place;
		  var waypts = [];
		  for (var i = aventon.places_list.length - 1; i >= 0; i--) {
		  	waypts.push({
		  		location: aventon.places_list[i],
		  		stopover:true
		  	});
		  };
		  var request = {
		      origin: start,
		      destination: end,
		      waypoints: waypts,
		      optimizeWaypoints: true,
		      travelMode: google.maps.TravelMode.DRIVING
		  };
		  directionsService.route(request, function(response, status) {
		    if (status == google.maps.DirectionsStatus.OK) {
		      directionsDisplay.setDirections(response);
		    }
		  });
		}

		
	}


]);
'use strict';

//Articles service used for communicating with the articles REST endpoints
angular.module('aventon').factory('Aventon', ['$resource',
	function($resource) {
		return $resource('aventon/:aventonId/:method', {
			aventonId: '@_id'
		}, {
			update: {
				method: 'PUT'
			},
			paginate: {
				method: 'GET',
				params: {'method' : 'paginate'},
				isArray: false
			},
			paginateAdmin: {
				method: 'GET',
				params: {'method' : 'paginateAdmin'},
				isArray: false
			}
		});
	}
]);
'use strict';

// Configuring the Articles module
angular.module('articles').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Articles', 'articles', 'dropdown', '/articles(/create)?');
		Menus.addSubMenuItem('topbar', 'articles', 'List Articles', 'articles');
		Menus.addSubMenuItem('topbar', 'articles', 'New Article', 'articles/create');
	}
]);
'use strict';

// Setting up route
angular.module('chatMsgs').config(['$stateProvider',
	function($stateProvider) {
		// chatMsg state routing
		$stateProvider.
		state('createChatMessages', {
			url: '/messageChat/create',
			templateUrl: 'modules/chatMsg/views/create-messages.client.view.html'
		});
	}
]);
'use strict';

angular.module('chatMsgs').controller('ChatMsgsController', ['$scope', '$stateParams', '$location', 'Authentication', 'ChatMsgs','$http', '$rootScope',
	function($scope, $stateParams, $location, Authentication, ChatMsgs, $http, $rootScope) {
		$scope.authentication = Authentication;

		$scope.create = function(idReceiver) {
			var message = new Messages({
				content: this.content,
				subject: this.subject,
				user_sender: $scope.authentication.user._id,
				user_receiver: idReceiver
			});
			message.$save(function(response) {
				

				$scope.content = '';
				$scope.subject = '';

				//$location.path('usersNewsLetter/' + response._id);
				//mostrar cartel
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.remove = function(article) {
			if (article) {
				article.$remove();

				for (var i in $scope.articles) {
					if ($scope.articles[i] === article) {
						$scope.articles.splice(i, 1);
					}
				}
			} else {
				$scope.article.$remove(function() {
					$location.path('articles');
				});
			}
		};

		$scope.update = function(idReview,comment,valoration) {
			$http.get('/reviews/'+idReview)
			.success(function(review){
				review.comment = comment;
				review.valoration = valoration;
				review.$update(function(){
					alert('Usted ha realizado una devolución correctamente');
				}, function(errorResponse){
					alert('Hubo un error, intentelo nuevamente');
				});
			})
			.error(function(){
				alert('Hubo un error, intentelo nuevamente');
			});
		};

		$scope.find = function() {
			$scope.articles = Articles.query();
		};

		$scope.findOne = function() {
			$scope.message = Messages.get({
				messageId: $stateParams.messageId
			});
		};

		
	}
]);

'use strict';

//Articles service used for communicating with the articles REST endpoints
angular.module('chatMsgs').factory('ChatMsgs', ['$resource',
	function($resource) {
		return $resource('chatMsgs/:chatMsgId', {
			chatMsgId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);
'use strict';

// Configuring the Articles module
angular.module('articles').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Articles', 'articles', 'dropdown', '/articles(/create)?');
		Menus.addSubMenuItem('topbar', 'articles', 'List Articles', 'articles');
		Menus.addSubMenuItem('topbar', 'articles', 'New Article', 'articles/create');
	}
]);
'use strict';

// Setting up route
angular.module('conversations').config(['$stateProvider',
	function($stateProvider) {
		// Conversation state routing
		$stateProvider.
		state('chat', {
            url: '/chat',
            templateUrl: 'modules/conversation/views/chat.client.view.html'
        });
	}
]);
'use strict';

angular.module('conversations').controller('ConversationsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Conversations','$http', '$rootScope', 'ChatMsgs', '$interval',
	function($scope, $stateParams, $location, Authentication, Conversations, $http, $rootScope, ChatMsgs, $interval) {
		$scope.authentication = Authentication;


		$scope.create = function(idReceiver) {
			var message = new Messages({
				content: this.content,
				subject: this.subject,
				user_sender: $scope.authentication.user._id,
				user_receiver: idReceiver
			});
			message.$save(function(response) {
				

				$scope.content = '';
				$scope.subject = '';

				//$location.path('usersNewsLetter/' + response._id);
				//mostrar cartel
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.remove = function(article) {
			if (article) {
				article.$remove();

				for (var i in $scope.articles) {
					if ($scope.articles[i] === article) {
						$scope.articles.splice(i, 1);
					}
				}
			} else {
				$scope.article.$remove(function() {
					$location.path('articles');
				});
			}
		};

		$scope.update = function(idReview,comment,valoration) {
			$http.get('/reviews/'+idReview)
			.success(function(review){
				review.comment = comment;
				review.valoration = valoration;
				review.$update(function(){
					alert('Usted ha realizado una devolución correctamente');
				}, function(errorResponse){
					alert('Hubo un error, intentelo nuevamente');
				});
			})
			.error(function(){
				alert('Hubo un error, intentelo nuevamente');
			});
		};

		$scope.find = function() {
			$scope.articles = Articles.query();
		};

		$scope.findOne = function() {
			$scope.message = Messages.get({
				messageId: $stateParams.messageId
			});
		};

		$scope.scroll_to = function(id){
            var div = document.getElementById(id);
            div.scrollTop = div.scrollHeight;
        };

        $scope.myContacts = function() {
        	$http.post('/conversationsByUser', {
        		'userId' : $scope.authentication.user._id
        	})
        	.success(function(conversations){
        		for (var i = conversations.length - 1; i >= 0; i--) {
        			for (var j = conversations[i]["members"].length - 1; j >= 0; j--) {
        				if(conversations[i]["members"][j]._id != $scope.authentication.user._id)
        					conversations[i]["contact"] = conversations[i]["members"][j]; 
        			};
        		};
        		$scope.conversations = conversations;
        		console.log($scope.conversations);
        	})
        };

        
        $scope.refreshChat = function(){
        	if($scope.currentConversationId != "nothing" && $scope.currentContact != "nothing")
        		$scope.msgsBetweenUsers($scope.currentContact, $scope.currentConversationId)
        };

        $interval($scope.refreshChat,5000);

        $scope.msgsBetweenUsers = function(contact, conversationId){
			$scope.showChatWindow = true;
			$scope.currentContact = contact;
			$scope.currentConversationId = conversationId;
			$http.post('/chatMsgByConversationId', {
				"conversationId" : conversationId
			})
			.success(function(msgs){
				$scope.msgs = msgs;
				$scope.scroll_to("chatDiv");
			});

		};
		$scope.currentConversationId = "nothing";
		$scope.currentContact = "nothing";
		$scope.showChatWindow = false;

		$scope.createMsg = function(){
			var chatMsg = new ChatMsgs({
				content: this.content,
				user_a: $scope.authentication.user._id,
				user_b: $scope.currentContact._id,
				conversation : $scope.currentConversationId,
				displayName_a : $scope.authentication.user.displayName,
				displayName_b : $scope.currentContact.displayName
			});
			this.content = '';

			chatMsg.$save(function(response) {
				$scope.msgsBetweenUsers($scope.currentContact, $scope.currentConversationId);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};


	}
]);

'use strict';

//Articles service used for communicating with the articles REST endpoints
angular.module('conversations').factory('Conversations', ['$resource',
	function($resource) {
		return $resource('conversations/:conversationId', {
			conversationId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);
'use strict';

// Setting up route
angular.module('core').config(['$stateProvider', '$urlRouterProvider',
	function($stateProvider, $urlRouterProvider) {
		// Redirect to home view when route not found
		$urlRouterProvider.otherwise('/');

		// Home state routing
		$stateProvider.
		state('home', {
			url: '/',
			templateUrl: 'modules/core/views/home.client.view.html'
		})
        .state('search', {
            url: '/search',
            templateUrl: 'modules/core/views/search.client.view.html'
        })
        .state('activate', {
            url: '/emailvalidate/:id/:token',
            templateUrl: 'modules/core/views/activate.client.view.html'
        })
        .state('faq', {
			url: '/faq',
			templateUrl: 'modules/core/views/faq.client.view.html'
		});
	}
]);

'use strict';
angular.module('core').controller('HeaderController', ['$scope', 'Authentication', 'Menus', '$rootScope', '$timeout', '$http',
	function($scope, Authentication, Menus, $rootScope, $timeout, $http) {
		$scope.authentication = Authentication;
		$scope.isCollapsed = false;
		$scope.menu = Menus.getMenu('topbar');

		$scope.toggleCollapsibleMenu = function() {
			$scope.isCollapsed = !$scope.isCollapsed;
		};

		// Collapsing the menu after navigation
		$scope.$on('$stateChangeSuccess', function() {
			$scope.isCollapsed = false;
		});

		//Time to hide alert
		$rootScope.timeAlert = 15000;

		$rootScope.alerts = [];
		/*$rootScope.alerts = [
			{type: 'info', msg : 'Hola soy una alerta de información'},
			{type: 'success', msg : 'Se ha creado su aventón con exito'},
			{type: 'danger', msg : 'Se crasheo el sistema!'}

		];*/
		// $rootScope.alerts.push({type: 'info', msg:'Mi mensaje'});

		$rootScope.$watch('alerts', function(newValue, oldValue){
			$rootScope.alerts = newValue;
			$timeout(function(){
				$rootScope.alerts = [];
			},$rootScope.timeAlert)
		});

		$rootScope.closeAlert = function(index) {
			$rootScope.alerts.splice(index, 1);
		};

		//faq
		$scope.faq = [true];

		$scope.getFaqs = function(){
			$http.get('/faqs')
			.success(function(data){
				$scope.faqs = data;
			});
		};
	}
]);

'use strict';


angular.module('core').controller('HomeController', ['$scope', 'Authentication','$state',
    function($scope, Authentication, $state) {
        // This provides Authentication context.
        $scope.authentication = Authentication;
        $scope.redirect = function(){
            var user = $scope.authentication.user;
            if(user){
                if(user.role == 'user')
                    $state.go('search');
                else
                    $state.go('adminProfile.usersList')
            }else{
                $state.go('signup');
            }
        };

        $scope.redirect();

        
    }
]);
'use strict';

angular.module('core').controller('SearchController', ['$scope', '$stateParams', '$location', 'Authentication', 'Aventon', '$http', '$rootScope', '$timeout',
	function($scope, $stateParams, $location, Authentication, Aventon, $http, $rootScope, $timeout) {
		$scope.authentication = Authentication;

        /*
         * Filters: Last, Hots
         */
        $scope.aventonFilter = 'normal';

        $scope.setAventonFilter = function(newFilter){
            $scope.aventonFilter = newFilter;
            $scope.find();

        };


		$scope.totalItems = 0;
		$scope.perPage = 10;
		$scope.currentPage = 1;
		$scope.maxSizeAventones = 10;

		$scope.find = function() {
			$scope.searchOn = "false"
			var page = $scope.currentPage;
			Aventon.paginate({page: page, tipodebusqueda: $scope.aventonFilter}, function(response) {
				$scope.aventones = response.aventones;
				$scope.totalItems = response.total;
			});
		};


	  $scope.setPage = function (pageNo) {
	    $scope.currentPage = pageNo;
	  };



		$scope.findOne = function() {
			$scope.aventon = Aventon.get({
				aventonId: $stateParams.articleId
			});
		};

		$scope.minDate = $scope.minDate ? null : new Date();
		$scope.searchOn = "false";
		$scope.start_lat  = "";
		$scope.start_lng  = "";
		$scope.end_lat  = "";
		$scope.end_lng  = "";
		$scope.aventonesBackup = "";

		$scope.searchAventon = function(){
			$scope.searchOn = "true";
			var start_lat  = this.direccion.lat;
			var start_lng  = this.direccion.lng;
			var end_lat  = this.direcciondos.lat;
			var end_lng  = this.direcciondos.lng;
			$scope.start_lat  = start_lat;
			$scope.start_lng  = start_lng;
			$scope.end_lat  = end_lat;
			$scope.end_lng  = end_lng;
			var fecha = this.fecha;
			var direccion = this.direccion;
			var direcciondos = this.direcciondos;

			$http.post('/searchAventon', {
				'date' : fecha,
				'start_place': direccion,
				'end_place': direcciondos
			})
			.success(function(aventones){
				for (var i = 0; i < aventones.length; i++) {
					aventones[i].tipo = "conductor";
					aventones[i].distancias = [];
					for (var j = 0; j < aventones[i].tags_pos.length; j++) {
						var distancia_value = distancia(aventones[i].tags_pos[j].pos[1],aventones[i].tags_pos[j].pos[0],start_lat,start_lng);
						aventones[i].distancias.push(distancia_value);
						distancia_value = distancia(aventones[i].tags_pos[j].pos[1],aventones[i].tags_pos[j].pos[0],end_lat,end_lng);
						aventones[i].distancias.push(distancia_value);
					};
				};

				$http.post('/searchDemandAventon', {
					'date' : fecha,
					'start_place': direccion,
					'end_place': direcciondos
				})
				.success(function(daventones){
					for (var i = 0; i < daventones.length; i++) {
						daventones[i].tipo = "pasajero";
						daventones[i].start_day = daventones[i].day;
						daventones[i].distancias = [];
						for (var j = 0; j < daventones[i].tags_pos.length; j++) {
							var distancia_value = distancia(daventones[i].tags_pos[j].pos[1],daventones[i].tags_pos[j].pos[0],start_lat,start_lng);
							daventones[i].distancias.push(distancia_value);
							distancia_value = distancia(daventones[i].tags_pos[j].pos[1],daventones[i].tags_pos[j].pos[0],end_lat,end_lng);
							daventones[i].distancias.push(distancia_value);
						};
					};

					for (var i = 0; i < daventones.length; i++) {
						aventones.push(daventones[i]); //mezclo aventones con demandas
					};

					if(aventones.length==0){
						var pos_obj = new Object();
						var pos_extra = [];
						pos_extra[0] = $scope.start_lng;
						pos_extra[1] = $scope.start_lat;
						pos_obj.pos = pos_extra;
						$http.post('/addFailPlace/'+ $scope.authentication.user._id,
						{
							"place" : pos_obj
						})
						.success(function(){
							var pos_obj = new Object();
							var pos_extra = [];
							pos_extra[0] = $scope.end_lng;
							pos_extra[1] = $scope.end_lat;
							pos_obj.pos = pos_extra;
							$http.post('/addFailPlace/'+ $scope.authentication.user._id,
							{
								"place" : pos_obj
							});
						});
					};

					$scope.aventonesBackup = aventones;
					$scope.aventones = aventones;
				});				
			})
			.error(function(){
				$rootScope.alerts.push({type: 'danger', msg:'Ha ocurrido un error, intente luego'});
			});
		};


		window.fbAsyncInit = function() {
	        FB.init({
	          	appId      : '922797427753319',
	          	xfbml      : true,
	          	version    : 'v2.1'
	        });
	     };

	    (function(d, s, id){
         	var js, fjs = d.getElementsByTagName(s)[0];
     		if (d.getElementById(id)) {return;}
		        js = d.createElement(s); js.id = id;
		        js.src = "//connect.facebook.net/en_US/sdk.js";
		        fjs.parentNode.insertBefore(js, fjs);
	    }(document, 'script', 'facebook-jssdk'));

	    $scope.shareLocation = function(){
	    	if (navigator.geolocation) {
			    navigator.geolocation.getCurrentPosition(function(position){
			    	FB.ui({
					  method: 'share',
					  href: 'http://maps.google.com/maps?q=loc:'+position.coords.latitude+','+position.coords.longitude
					}, function(response){});
			    }, $scope.errorFunction);
			} 
	    };
		


		$scope.errorFunction = function(){
			console.log("Error en el pedido de datos");
		};

		$scope.openFirst = function($event) {
			$event.preventDefault();
    		$event.stopPropagation();
			$scope.opened = true;
			
		};

		function rad(x) {return x*Math.PI/180;}

		function distancia(lat1,lon1,lat2,lon2){
			var R     = 6378.137;//Radio de la tierra en km
			var dLat  = rad( lat2 - lat1 );
			var dLong = rad( lon2 - lon1 );

			var a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(rad(lat1)) * Math.cos(rad(lat2)) * Math.sin(dLong/2) * Math.sin(dLong/2);
			var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
			var d = R * c;

			return d.toFixed(3);//Retorna tres decimales
		};

		$scope.search_type = "Aventon";

    $scope.changeType = function(){
    	if($scope.search_type == "Aventon")
    		$scope.search_type = "Pasajero";
    	else
    		$scope.search_type = "Aventon";
    };


    $scope.distanciaFiltrada = function(aventon){
    	return true;
    };

    $scope.distanciaSeleccionada = 'todos';

    $scope.filtrar = function(){
    	var tipoModel;
    	if($scope.pasajero && $scope.conductor)
    		tipoModel = 'todos';
    	else
    		if($scope.pasajero)
    			tipoModel = 'pasajero';
    		else
    			tipoModel = 'conductor';

		//reestablezco todos los aventones de la busqueda
		var aventones = $scope.aventonesBackup;
		var aventonesRes = [];
		
		

		var included = false;
		var distancia = 0;
		switch($scope.distanciaSeleccionada){
			case 'minus1':
				distancia = 1;
				break;
			case 'minus3':
				distancia = 3;
				break;
			case 'minus500':
				distancia = 0.5;
				break;
			case 'todos':
				distancia = 10;
				break;
		}
		
	
		for (var i = 0; i < aventones.length; i++) {
			included = false;
			for (var j = 0; j < aventones[i].distancias.length; j++) {
				if(aventones[i].tipo == tipoModel || tipoModel == 'todos'){
					console.log(aventones[i].distancias[j]);
					if(aventones[i].distancias[j] < distancia && !included){
        				aventonesRes.push(aventones[i]);
        				included = true;
        			}
        		}
      		};
      	};
		
		//fin de filtros
		$scope.aventones = aventonesRes;        	
      };

       
	}
]);
'use strict';
angular.module('core').
  directive('ngConfirmClick', [
       function(){
           return {
               link: function (scope, element, attr) {
                   var msg = attr.ngConfirmClick || "Are you sure?";
                   var clickAction = attr.confirmedClick;
                   element.bind('click',function (event) {
                       if ( window.confirm(msg) ) {
                           scope.$eval(clickAction)
                       }
                   });
               }
           };
   }])
'use strict';

//Menu service used for managing  menus
angular.module('core').service('Menus', [

	function() {
		// Define a set of default roles
		this.defaultRoles = ['*'];

		// Define the menus object
		this.menus = {};

		// A private function for rendering decision 
		var shouldRender = function(user) {
			if (user) {
				if (!!~this.roles.indexOf('*')) {
					return true;
				} else {
					for (var userRoleIndex in user.roles) {
						for (var roleIndex in this.roles) {
							if (this.roles[roleIndex] === user.roles[userRoleIndex]) {
								return true;
							}
						}
					}
				}
			} else {
				return this.isPublic;
			}

			return false;
		};

		// Validate menu existance
		this.validateMenuExistance = function(menuId) {
			if (menuId && menuId.length) {
				if (this.menus[menuId]) {
					return true;
				} else {
					throw new Error('Menu does not exists');
				}
			} else {
				throw new Error('MenuId was not provided');
			}

			return false;
		};

		// Get the menu object by menu id
		this.getMenu = function(menuId) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Return the menu object
			return this.menus[menuId];
		};

		// Add new menu object by menu id
		this.addMenu = function(menuId, isPublic, roles) {
			// Create the new menu
			this.menus[menuId] = {
				isPublic: isPublic || false,
				roles: roles || this.defaultRoles,
				items: [],
				shouldRender: shouldRender
			};

			// Return the menu object
			return this.menus[menuId];
		};

		// Remove existing menu object by menu id
		this.removeMenu = function(menuId) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Return the menu object
			delete this.menus[menuId];
		};

		// Add menu item object
		this.addMenuItem = function(menuId, menuItemTitle, menuItemURL, menuItemType, menuItemUIRoute, isPublic, roles, position) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Push new menu item
			this.menus[menuId].items.push({
				title: menuItemTitle,
				link: menuItemURL,
				menuItemType: menuItemType || 'item',
				menuItemClass: menuItemType,
				uiRoute: menuItemUIRoute || ('/' + menuItemURL),
				isPublic: ((isPublic === null || typeof isPublic === 'undefined') ? this.menus[menuId].isPublic : isPublic),
				roles: ((roles === null || typeof roles === 'undefined') ? this.menus[menuId].roles : roles),
				position: position || 0,
				items: [],
				shouldRender: shouldRender
			});

			// Return the menu object
			return this.menus[menuId];
		};

		// Add submenu item object
		this.addSubMenuItem = function(menuId, rootMenuItemURL, menuItemTitle, menuItemURL, menuItemUIRoute, isPublic, roles, position) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Search for menu item
			for (var itemIndex in this.menus[menuId].items) {
				if (this.menus[menuId].items[itemIndex].link === rootMenuItemURL) {
					// Push new submenu item
					this.menus[menuId].items[itemIndex].items.push({
						title: menuItemTitle,
						link: menuItemURL,
						uiRoute: menuItemUIRoute || ('/' + menuItemURL),
						isPublic: ((isPublic === null || typeof isPublic === 'undefined') ? this.menus[menuId].items[itemIndex].isPublic : isPublic),
						roles: ((roles === null || typeof roles === 'undefined') ? this.menus[menuId].items[itemIndex].roles : roles),
						position: position || 0,
						shouldRender: shouldRender
					});
				}
			}

			// Return the menu object
			return this.menus[menuId];
		};

		// Remove existing menu object by menu id
		this.removeMenuItem = function(menuId, menuItemURL) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Search for menu item to remove
			for (var itemIndex in this.menus[menuId].items) {
				if (this.menus[menuId].items[itemIndex].link === menuItemURL) {
					this.menus[menuId].items.splice(itemIndex, 1);
				}
			}

			// Return the menu object
			return this.menus[menuId];
		};

		// Remove existing menu object by menu id
		this.removeSubMenuItem = function(menuId, submenuItemURL) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Search for menu item to remove
			for (var itemIndex in this.menus[menuId].items) {
				for (var subitemIndex in this.menus[menuId].items[itemIndex].items) {
					if (this.menus[menuId].items[itemIndex].items[subitemIndex].link === submenuItemURL) {
						this.menus[menuId].items[itemIndex].items.splice(subitemIndex, 1);
					}
				}
			}

			// Return the menu object
			return this.menus[menuId];
		};

		//Adding the topbar menu
		this.addMenu('topbar');
	}
]);
'use strict';

// Configuring the Articles module
angular.module('articles').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Articles', 'articles', 'dropdown', '/articles(/create)?');
		Menus.addSubMenuItem('topbar', 'articles', 'List Articles', 'articles');
		Menus.addSubMenuItem('topbar', 'articles', 'New Article', 'articles/create');
	}
]);
'use strict';

// Setting up route
angular.module('demands').config(['$stateProvider',
	function($stateProvider) {
		// Articles state routing
		$stateProvider.
		state('exampleDemand', {
			url: '/demand/example',
			templateUrl: 'modules/demand/views/example-demand.client.view.html'
		});
	}
]);
'use strict';

angular.module('demands').controller('DemandsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Demands', '$http', '$rootScope', 
	function($scope, $stateParams, $location, Authentication, Demands, $http, $rootScope) {
		$scope.authentication = Authentication;

		$scope.create = function(aventon) {
			var demand = new Demands({
				user: $scope.authentication.user._id,
				aventon: aventon._id
			});
			demand.$save(function(response) {
				if(response.message){
					$rootScope.alerts.push({type: 'danger', msg:'Usted ya ha enviado una solicitud anteriormente por este aventón'});
				}else{ 
					$rootScope.alerts.push({type: 'info', msg:'Le has enviado el pedido de viaje'});
					$http.post('/notifications',{
						aventon: aventon._id,
						type : 'trip',
						anexo_user : $scope.authentication.user._id,
						comment: 'Un usuario quiere aprovechar su aventón',
						user: aventon.user._id,
					})
					.success(function(){
						$http.post('/sendEmail',{
							email: aventon.user.email,
							subject:'Un usuario quiere aprovechar tu aventón',
							content: 'El usuario '+$scope.authentication.user.displayName+' quiere aprovechar tu aventón'
						}).success(function(){
							console.log("Mail Enviado");
						});
					});
				};				
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.remove = function(article) {
			if (article) {
				article.$remove();

				for (var i in $scope.articles) {
					if ($scope.articles[i] === article) {
						$scope.articles.splice(i, 1);
					}
				}
			} else {
				$scope.article.$remove(function() {
					$location.path('articles');
				});
			}
		};

		$scope.update = function() {
			var article = $scope.article;

			article.$update(function() {
				$location.path('articles/' + article._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.find = function() {
			$scope.articles = Articles.query();
		};

		$scope.findOne = function() {
			$scope.article = Articles.get({
				articleId: $stateParams.articleId
			});
		};

		$scope.demandsForMe = function() {
			$http.get('/demandsForMe').success(function(data){
				$scope.demands = data;
			});
		};

		$scope.acceptDemand = function(idDemand){
			Demands.get({
				demandId : idDemand
			}, function(demand){
				demand.state = 'Aceptada';
				demand.$update(function(){
					$rootScope.alerts.push({type: 'info', msg:'Usted ha aceptado correctamente la solicitud de aventón'});
					$scope.giveMeDemandsByAventon(demand.aventon);
					$http.get('/aventon/'+demand.aventon)
					.success(function(aventon){
						console.log(demand.user);
						console.log(aventon.user);
						console.log(aventon._id);
						$http.post('/review',{
							'user_origin' : demand.user,
							'user_receiver' : aventon.user._id ,
							'aventon' : aventon._id
						})
						.success(function(review){
							$http.post('/review',{
								'user_receiver' : demand.user,
								'user_origin' : aventon.user._id ,
								'aventon' : aventon._id
							})
							$http.post('/notifications',{
								'user' : demand.user,
								'anexo_user' : $scope.authentication.user._id,
								'comment' : 'Han aceptado tu solicitud de Aventón',
								'aventon' : aventon._id,
								'type' : "demandA"
							});
						})
						.error(function(){

						});
					})
					.error(function(){

					});
					
				},
				function(errorResponse){
					
				});
			});
		};

		$scope.dismissDemand = function(idDemand){
			Demands.get({
				demandId : idDemand
			}, function(demand){
				demand.state = 'Rechazada';
				demand.$update(function(){
					$rootScope.alerts.push({type: 'info', msg:'Usted ha rechazado la solicitud de aventón'});
					$scope.giveMeDemandsByAventon(demand.aventon);
				},
				function(errorResponse){
					$rootScope.alerts.push({type: 'danger', msg:'Hubo un error, intentelo nuevamente'});
				});
			});
		};

		$scope.demandsWaitingByAventon = function(idAventon){
			$http.post('/demandsWaitingByAventon',{
				'aventonId' : idAventon
			})
			.success(function(demandsWaiting){
				$scope.demandsWaiting = demandsWaiting;
			})
		};

		$scope.demandsAcceptedByAventon = function(idAventon){
			$http.post('/demandsAcceptedByAventon',{
				'aventonId' : idAventon
			})
			.success(function(demandsAccepted){
				$scope.demandsAccepted = demandsAccepted;
			})
		};

		$scope.demandsRejectedByAventon = function(idAventon){
			$http.post('/demandsRejectedByAventon',{
				'aventonId' : idAventon
			})
			.success(function(demandsRejected){
				$scope.demandsRejected = demandsRejected;
			})
		};

		$scope.giveMeDemandsByAventon = function(idAventon){
			$scope.demandsAcceptedByAventon(idAventon);
			$scope.demandsWaitingByAventon(idAventon);
		};

	}
]);
'use strict';

//Demands service used for communicating with the articles REST endpoints
angular.module('demands').factory('Demands', ['$resource',
	function($resource) {
		return $resource('demands/:demandId', {
			demandId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);
'use strict';

// Configuring the Articles module
angular.module('articles').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Articles', 'articles', 'dropdown', '/articles(/create)?');
		Menus.addSubMenuItem('topbar', 'articles', 'List Articles', 'articles');
		Menus.addSubMenuItem('topbar', 'articles', 'New Article', 'articles/create');
	}
]);
'use strict';

// Setting up route
angular.module('demandsAventon').config(['$stateProvider',
	function($stateProvider) {
		// Articles state routing
		$stateProvider.
		state('createDemandAventon', {
			url: '/demandAventon/create',
			templateUrl: 'modules/demandAventon/views/create-aventonDemand.client.view.html'
		})
		.state('editDemandAventon', {
			url: '/demandAventon/edit/:demandAventonId',
			templateUrl: 'modules/demandAventon/views/edit-aventonDemand.client.view.html'
		});
	}
]);
'use strict';

angular.module('demandsAventon').controller('DemandsAventonController', ['$scope', '$stateParams', '$location', 'Authentication', 'DemandsAventon', '$http', '$rootScope', '$filter',
	function($scope, $stateParams, $location, Authentication, DemandsAventon, $http, $rootScope,$filter) {
		$scope.authentication = Authentication;
		$scope.minDate = $scope.minDate ? null : new Date();
		
		$scope.create = function(aventon) {
			$scope.pos_cities = [];
			var pos_obj = new Object();
			var pos_extra = [];
			pos_extra[0] = $scope.direccion.lng;
			pos_extra[1] = $scope.direccion.lat;
			pos_obj.pos = pos_extra;
			$scope.pos_cities.push(pos_obj);

			var pos_obj = new Object();
			var pos_extra = [];
			pos_extra[0] = $scope.direcciondos.lng;
			pos_extra[1] = $scope.direcciondos.lat;
			pos_obj.pos = pos_extra;
			$scope.pos_cities.push(pos_obj);
			
			var tags_pos = $scope.pos_cities;

			var demandAventon = new DemandsAventon({
				start_place: this.direccion.direccion,
				end_place: this.direcciondos.direccion,
				day: this.start_day,				
				comment: this.comment,
				time_flexibility: this.time_flexibility,
				from_hour: this.from_hour,
				until_hour: this.until_hour,
				from_day: this.from_day,
				until_day: this.until_day,
				recurrent_travel: this.recurrent_travel,
				frequency: this.frequency,
				luggage_type: this.luggage_type,
				tags_pos: tags_pos,				
				user: this.authentication.user._id
				
			});
			demandAventon.$save(function(response) {
				$scope.dir = '';
				$scope.dir2 = '';
				$scope.start_day = '';
				$scope.comment = '';
				$scope.time_flexibility = '';
				$scope.from_hour= '';
				$scope.until_hour= '';
				$scope.from_day = '';
				$scope.until_day = '';
				$scope.recurrent_travel = '';
				$scope.frequency = '';
				$scope.luggage_type = '';
							

				$rootScope.alerts.push({type: 'success', msg:'La demanda de aventón fue creada con éxito!'});
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.openThird = function($event) {
			$event.preventDefault();
    		$event.stopPropagation();
			$scope.openedThree = true;
		};

		$scope.openFourth = function($event) {
			$event.preventDefault();
    		$event.stopPropagation();
			$scope.openedFour = true;
			
		};

		$scope.giveMeToolTipText = function(demand){
			var text = "";
			if(demand.recurrent_travel)
				text+="Viaje frecuente: "+demand.frequency+"\n";
			else
				text+="No es un viaje frecuente \n";

			if(demand.time_flexibility){
				text+="Día y hora flexible: \n Desde "+$filter('date')(demand.from_day)+" hasta "+$filter('date')(demand.until_day)+"\n";
				text+="Desde "+ demand.from_hour+"hs hasta "+demand.until_hour+"hs \n"; 
			}
			text+="Tipo de equipaje: "+demand.luggage_type;
			return text;
		};


		$scope.initCreate = function(){
			$scope.user = $scope.authentication.user;
		};

		$scope.idRemove = "";

		$scope.changeIdRemove = function(id){
			$scope.idRemove = id;
			console.log("aca");
		};

		$scope.removeDemand = function(id){
			console.log(id);
			$http.delete('/demandsAventon/'+id).
			success(function(data){
				$scope.myListDemandsAventon();
			});
		}

		$scope.remove = function(article) {
			if (article) {
				article.$remove();

				for (var i in $scope.articles) {
					if ($scope.articles[i] === article) {
						$scope.articles.splice(i, 1);
					}
				}
			} else {
				$scope.article.$remove(function() {
					$location.path('articles');
				});
			}
		};

		$scope.goEdit = function(id){
			$location.path('demandAventon/edit/'+id);
		};

		$scope.update = function() {
			var demand = $scope.demandA;
			if($scope.dir != demand.start_place)
				demand.start_place = this.direccion.direccion;
			if($scope.dir2 != demand.end_place)
				demand.end_place = this.direcciondos.direccion;

			$http.put('/demandsAventon/'+$scope.demandA._id, demand)
			.success(function(data){
				$rootScope.alerts.push({type: 'success', msg:'La demanda de aventón fue actualizada con éxito!'});
			})
			.error(function(){
				$rootScope.alerts.push({type: 'danger', msg:'La demanda de aventón no fue actualizada!'});	
			});
		};

		$scope.find = function() {
			$scope.articles = Articles.query();
		};

		$scope.findOne = function() {
			$scope.user = $scope.authentication.user;
			$http.get('/demandsAventon/'+$stateParams.demandAventonId)
			.success(function(data){
				$scope.demandA = data;
				$scope.dir = $scope.demandA.start_place;
				$scope.dir2 = $scope.demandA.end_place;
			});
			
		};

		$scope.openFirst = function($event) {
			$event.preventDefault();
    		$event.stopPropagation();
			$scope.opened = true;
			
		};

		$scope.listDemandsAventon = function(){
			$http.get('/demandAventon')
			.success(function(data){
				$scope.demandsAventon = data;
			});
		};

		$scope.myListDemandsAventon = function(){
			$http.get('/myDemandsAventon')
			.success(function(data){
				$scope.demandsAventon = data;
			});	
		};

		$scope.myAventonesValid = function(){
			$http.post('/aventonesValidsByUser',{
				"idUser" : $scope.authentication.user._id
			})
			.success(function(aventones){
				$scope.myAventones = aventones;
			});
		};

		$scope.aventonInvita;

		$scope.invitar = function(value,invitado){
			if(!value || value == "null")
				$rootScope.alerts.push({type: 'danger', msg:'Seleccione un aventón o cree uno nuevo'});	
			else{
				$http.post('/invitations',{
					user_sender: $scope.authentication.user._id,
					user_receiver: invitado,
					aventon: value
				})
				.success(function(invitation){
					$rootScope.alerts.push({type: 'success', msg:'Se ha enviado la invitación'});
					console.log(value);
					$http.post('/notifications',{
						aventon: value,
						type: 'invitation',
						anexo_user: $scope.authentication.user._id,
						comment: 'Un usuario te ha invitado',
						user: invitado
					});
				})
				.error(function(){
					$rootScope.alerts.push({type: 'danger', msg:'Invitación enviada anteriormente'});	
				})
		};

		}



	}
]);
'use strict';

//Demands service used for communicating with the articles REST endpoints
angular.module('demandsAventon').factory('DemandsAventon', ['$resource',
	function($resource) {
		return $resource('demandsAventon/:demandAventonId', {
			demandAventonId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);
'use strict';

// Configuring the Articles module
angular.module('articles').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Articles', 'articles', 'dropdown', '/articles(/create)?');
		Menus.addSubMenuItem('topbar', 'articles', 'List Articles', 'articles');
		Menus.addSubMenuItem('topbar', 'articles', 'New Article', 'articles/create');
	}
]);
'use strict';

// Setting up route
angular.module('messages').config(['$stateProvider',
	function($stateProvider) {
		// Message state routing
		$stateProvider.
		state('createMessages', {
			url: '/message/create',
			templateUrl: 'modules/message/views/create-messages.client.view.html'
		});
	}
]);
'use strict';

angular.module('messages').controller('MessagesController', ['$scope', '$stateParams', '$location', 'Authentication', 'Messages','$http', '$rootScope',
	function($scope, $stateParams, $location, Authentication, Messages, $http, $rootScope) {
		$scope.authentication = Authentication;
		$rootScope.activeTab = 'messages';

		$scope.preCreate = function(idReceiver){
			$scope.idReceiver = idReceiver;
		};

		$scope.create = function(idReceiver) {
			var message = new Messages({
				content: this.content,
				subject: this.subject,
				user_sender: $scope.authentication.user._id,
				user_receiver: idReceiver
			});
			message.$save(function(response) {
				

				$scope.content = '';
				$scope.subject = '';

				//$location.path('usersNewsLetter/' + response._id);
				//mostrar cartel
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.fastCreate = function(idReceiver){
			var message = new Messages({
				content: this.content,
				subject: 'mensaje rapido - pedido de aventón',
				user_sender: $scope.authentication.user._id,
				user_receiver: idReceiver
			});
			message.$save(function(response) {
				$http.post('/conversations',{members:[idReceiver,$scope.authentication.user._id]})
				.success(function(data){
					$rootScope.alerts.push({type: 'info', msg:'Usted puede chatear con este usuario'});
				});
				$http.post('/searchPublicUser',{id:idReceiver})
				.success(function(user){
					$http.post('/notifications',{
						type: 'message',
						anexo_user: $scope.authentication.user._id,
						comment: 'Un usuario te ha enviado un mensaje privado',
						user: user._id,
						message: response._id
					})
					.success(function(){
						$http.post('/sendEmail',{
							email: user.email,
							subject:'Recibiste un mensaje privado - UnAventon',
							content: 'El usuario '+$scope.authentication.user.displayName+' te ha enviado un mensaje privado.'
						}).success(function(){
							console.log("Mail Enviado");
						});

					});

				});
				
				$rootScope.alerts.push({type: 'success', msg:'Su mensaje ha sido enviado correctamente'});
				$scope.content = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};


		$scope.sendPrivateMessage = function(idReceiver){
			var message = new Messages({
				content: this.content,
				subject: 'mensaje rapido - pedido de aventón',
				user_sender: $scope.authentication.user._id,
				user_receiver: idReceiver
			});
			message.$save(function(response) {
				$scope.allMessagesByUser();
				$http.post('/searchPublicUser',{id:idReceiver})
				.success(function(user){
					$http.post('/notifications',{
						type: 'message',
						anexo_user: $scope.authentication.user._id,
						comment: 'Un usuario te ha enviado un mensaje privado',
						user: user._id,
						message: response._id
					})
					.success(function(){
						$http.post('/sendEmail',{
							email: user.email,
							subject:'Recibiste un mensaje privado - UnAventon',
							content: 'El usuario '+$scope.authentication.user.displayName+' te ha enviado un mensaje privado.'
						}).success(function(){
							console.log("Mail Enviado");
						});

					});

				});
				
				$rootScope.alerts.push({type: 'success', msg:'Su mensaje ha sido enviado correctamente'});
				$scope.content = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});			
		};

		$scope.remove = function(article) {
			if (article) {
				article.$remove();

				for (var i in $scope.articles) {
					if ($scope.articles[i] === article) {
						$scope.articles.splice(i, 1);
					}
				}
			} else {
				$scope.article.$remove(function() {
					$location.path('articles');
				});
			}
		};

		$scope.update = function(idReview,comment,valoration) {
			$http.get('/reviews/'+idReview)
			.success(function(review){
				review.comment = comment;
				review.valoration = valoration;
				review.$update(function(){
					$rootScope.alerts.push({type: 'success', msg:'Usted ha realizado una devolución correctamente'});
				}, function(errorResponse){
					$rootScope.alerts.push({type: 'danger', msg:'Hubo un error, intente nuevamente'});
				});
			})
			.error(function(){
				$rootScope.alerts.push({type: 'danger', msg:'Hubo un error, intente nuevamente'});
			});
		};

		$scope.find = function() {
			$scope.articles = Articles.query();
		};

		$scope.findOne = function() {
			$scope.message = Messages.get({
				messageId: $stateParams.messageId
			});
		};

		$scope.findOneSent = function() {
			$scope.message = Messages.get({
				messageId: $stateParams.messageId
			});
		};


		$scope.messagesToMe = function(){
			$http.get('/messagesToMe')
			.success(function(data){
				$scope.messages = data;
			});
		};

		$scope.idMailWindow = "";

		$scope.changeIdMailWindow = function(newId){
			if($scope.idMailWindow == newId )
				$scope.idMailWindow = "";
			else
				$scope.idMailWindow = newId;
		};

		$scope.messagesSent = function(){
			$http.get('/messagesSent')
			.success(function(data){
				$scope.messages = data;
			});
		};

		$scope.allMessages = function(){
			$http.get('/allMessagesByUser')
			.success(function(data){
				$scope.messages = data;
			});	
		}

		$scope.allMessagesByUser = function(){
			$http.get('/allMessagesByUser/'+ $stateParams.messageId)
			.success(function(data){
				$scope.messages = data;
				$scope.userResponse = $stateParams.messageId;
				var sample = $scope.messages[0];
				if(sample.user_receiver._id != $scope.authentication.user._id)
					$scope.contact = sample.user_receiver;
				else
					$scope.contact = sample.user_sender;
				console.log($scope.messages);
			});	
		}

	}
]);

'use strict';

//Articles service used for communicating with the articles REST endpoints
angular.module('messages').factory('Messages', ['$resource',
	function($resource) {
		return $resource('messages/:messageId', {
			messageId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);
'use strict';

// Configuring the Articles module
angular.module('notifications').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Articles', 'articles', 'dropdown', '/articles(/create)?');
		Menus.addSubMenuItem('topbar', 'articles', 'List Articles', 'articles');
		Menus.addSubMenuItem('topbar', 'articles', 'New Article', 'articles/create');
	}
]);
'use strict';

// Setting up route
angular.module('notifications').config(['$stateProvider',
	function($stateProvider) {
		// Notification state routing
		$stateProvider.
		state('exampleNotifications', {
			url: '/notifications/create',
			templateUrl: 'modules/notifications/views/create-notifications.client.view.html'
		});
	}
]);
'use strict';

angular.module('notifications').controller('NotificationsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Notifications', '$http','$rootScope','$interval',
	function($scope, $stateParams, $location, Authentication, Notifications, $http,$rootScope, $interval) {
		$scope.authentication = Authentication;

		$scope.create = function() {
			var userNewsLetter = new UsersNewsLetter({
				email: this.email
			});
			userNewsLetter.$save(function(response) {
				//$location.path('usersNewsLetter/' + response._id);

				$scope.email = '';
				$scope.success = 'Se ha suscripto correctamente';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.remove = function(article) {
			if (article) {
				article.$remove();

				for (var i in $scope.articles) {
					if ($scope.articles[i] === article) {
						$scope.articles.splice(i, 1);
					}
				}
			} else {
				$scope.article.$remove(function() {
					$location.path('articles');
				});
			}
		};

		$scope.update = function() {
			var article = $scope.article;

			article.$update(function() {
				$location.path('articles/' + article._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.find = function() {
			$scope.articles = Articles.query();
		};

		$scope.findOne = function() {
			$scope.article = Articles.get({
				articleId: $stateParams.articleId
			});
		};

		$scope.myNotifications = function(){
			if($scope.authentication.user){
				$http.post('/notificationsByUser',{
					userId : $scope.authentication.user._id
				})
				.success(function(notifications){
					$rootScope.notifications = notifications;
					var count = 0;
					for (var i = notifications.length - 1; i >= 0; i--) {
						if(notifications[i].state == 'unChecked')
							count++;
					};
					$rootScope.notificaciones = count;
				});
			};
		};

		$scope.allMyNotifications = function(){
			if($scope.authentication.user){
				$http.post('/allNotificationsByUser',{
					userId : $scope.authentication.user._id
				})
				.success(function(notifications){
					$scope.allNotifications = notifications;
				});
			};
		};

		$interval($scope.myNotifications,60000);

		$scope.checkNotifications = function(){
			var notifications = $rootScope.notifications;
			for (var i = notifications.length - 1; i >= 0; i--) {
				$http.put('/notifications/'+notifications[i]._id,{
					state : 'checked'
				})
				
			};
			$rootScope.notificaciones = "0";
		}

	}
]);
'use strict';

//Articles service used for communicating with the articles REST endpoints
angular.module('notifications').factory('Notifications', ['$resource',
	function($resource) {
		return $resource('notifications/:notificationId', {
			notificationId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);
'use strict';

// Configuring the Articles module
angular.module('articles').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Articles', 'articles', 'dropdown', '/articles(/create)?');
		Menus.addSubMenuItem('topbar', 'articles', 'List Articles', 'articles');
		Menus.addSubMenuItem('topbar', 'articles', 'New Article', 'articles/create');
	}
]);
'use strict';

// Setting up route
angular.module('packs').config(['$stateProvider',
	function($stateProvider) {
		// Packs state routing
		$stateProvider.
		state('credits', {
            url: '/credits',
            templateUrl: 'modules/pack/views/credits.client.view.html'
        })
        .state('returnMP', {
            url: '/returnMP',
            templateUrl: 'modules/pack/views/returnMP.client.view.html'
        });
	}
]);
'use strict';

angular.module('packs').controller('PacksController', ['$scope', '$stateParams', '$location', 'Authentication', 'Packs', '$http', '$rootScope', '$window', '$timeout',
	function($scope, $stateParams, $location, Authentication, Packs, $http, $rootScope, $window, $timeout) {
		$scope.authentication = Authentication;


		$scope.createPack = function() {
			var pack = new Packs({
				nombre: this.name,
				cantidad_pines: this.cantidad_pines,
				precio : this.precio
			});
			pack.$save(function(response) {
				$location.path("packListAdmin");

				$scope.nombre = '';
				$scope.cantidad_pines = '';
				$scope.precio = '';

			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.create = function(aventon) {
			var demand = new Demands({
				user: $scope.authentication.user._id,
				aventon: aventon._id
			});
			demand.$save(function(response) {
				$rootScope.alerts.push({type: 'info', msg:'Le has enviado el pedido de viaje...'});
				$http.post('/notifications',{
					aventon: aventon._id,
					type : 'trip',
					anexo_user : $scope.authentication.user._id,
					comment: 'Un usuario quiere aprovechar su aventón',
					user: aventon.user._id,
				})
				.success(function(){
					$http.post('/sendEmail',{
						email: aventon.user.email,
						subject:'Un usuario quiere aprovechar tu aventón',
						content: 'El usuario '+$scope.authentication.user.displayName+' quiere aprovechar tu aventón'
					}).success(function(){
						console.log("Mail Enviado");
					});

				});

				
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.remove = function(pack) {
			if (pack) {
				pack.$remove();

				for (var i in $scope.packs) {
					if ($scope.packs[i] === pack) {
						$scope.packs.splice(i, 1);
					}
				}
			} else {
				$scope.pack.$remove(function() {
					$location.path('packListAdmin');
				});
			}
		};

		$scope.update = function() {
			var article = $scope.article;

			article.$update(function() {
				$location.path('articles/' + article._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.find = function() {
			$scope.packs = Packs.query();
		};

		$scope.findOne = function() {
			$scope.article = Articles.get({
				articleId: $stateParams.articleId
			});
		};

		$scope.buyPack = function(packId) {
			$window.location.href = '/packs/'+packId+'/buy';
		};	

		$scope.controlReturnMP = function(){
			var mercado_pago_id = $location.search().external_reference;
			var estado = $location.search().collection_status;
			$http.post('/changeStatusTransaction',{
				"mercado_pago_id" : mercado_pago_id ,
				"estado" : estado 
			})
			.success(function(t){
				$http.post('/searchPack',{
					"id_pack" : t.pack
				})
				.success(function(pack){
					var creditNew = pack.cantidad_pines;
					$http.post('/addCredit/'+ $scope.authentication.user._id,
					{
						"credit" : creditNew
					})
					.success(function(user){
						$scope.authentication.user = user;
						$rootScope.alerts.push({type: 'success', msg:'Se ha completado la transacción, espere a ser redirigido'});
						$timeout(function(){
							$location.path('/');
						}, 10000);		
					});
						
				});
				
			});
		};
	
	}
]);
'use strict';

//Packs service used for communicating with the articles REST endpoints
angular.module('packs').factory('Packs', ['$resource',
	function($resource) {
		return $resource('packs/:packId', {
			packId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);
'use strict';

// Configuring the Articles module
angular.module('articles').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Articles', 'articles', 'dropdown', '/articles(/create)?');
		Menus.addSubMenuItem('topbar', 'articles', 'List Articles', 'articles');
		Menus.addSubMenuItem('topbar', 'articles', 'New Article', 'articles/create');
	}
]);
'use strict';

// Setting up route
angular.module('reviews').config(['$stateProvider',
	function($stateProvider) {
		// Articles state routing
		$stateProvider.
		state('createReviews', {
			url: '/reviews/create',
			templateUrl: 'modules/reviews/views/create-reviews.client.view.html'
		});
	}
]);
'use strict';

angular.module('reviews').controller('ReviewsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Reviews','$http' , 'Users', '$rootScope',
	function($scope, $stateParams, $location, Authentication, Reviews, $http, Users, $rootScope) {
		$scope.authentication = Authentication;

		$scope.create = function() {
			var userNewsLetter = new UsersNewsLetter({
				email: this.email
			});
			userNewsLetter.$save(function(response) {
				//$location.path('usersNewsLetter/' + response._id);

				$scope.email = '';
				$scope.success = 'Se ha suscripto correctamente';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.remove = function(article) {
			if (article) {
				article.$remove();

				for (var i in $scope.articles) {
					if ($scope.articles[i] === article) {
						$scope.articles.splice(i, 1);
					}
				}
			} else {
				$scope.article.$remove(function() {
					$location.path('articles');
				});
			}
		};

		$scope.update = function(idReview,comment,valoration) {
			Reviews.get({
				reviewId : idReview
			},function(review){
				review.comment = comment;
				review.valoration = valoration.valoration;
				review.state = 'checked';
				review.$update(function(reviewUpdated){
					$scope.pendingReviews();
					$scope.reviewsList();
					$rootScope.alerts.push({type: 'success', msg:'Usted ha realizado una devolución correctamente'});
					Users.get({
						userId : reviewUpdated.user_receiver
					},function(user){
						user.review_point += reviewUpdated.valoration;
						user.review_count += 1;
						user.$update(function(){
							console.log("usuario actualizado");
						});
					});
				}, function(errorResponse){
					$rootScope.alerts.push({type: 'danger', msg:'Hubo un error, intente nuevamente'});
				});
			});

		};

		$scope.find = function() {
			$scope.articles = Articles.query();
		};

		$scope.findOne = function() {
			$scope.article = Articles.get({
				articleId: $stateParams.articleId
			});
		};

		$scope.pendingReviews = function() {
			$http.post('/pendingReviews', {
				"user_origin" : $scope.authentication.user._id,
				"user_receiver" : $stateParams.userId
			})
			.success(function(pendings){
				$scope.pendings = pendings;
			});
		};

		$scope.valorationValues = [
			{valoration:'5', text: 'Todo Excelente'},
			{valoration:'4', text: 'Muy Buen viaje'},
			{valoration:'3', text: 'Sin problemas'},
			{valoration:'2', text: 'Disconforme'},
			{valoration:'1', text: 'No recomendable'}
		];

		$scope.valoration = [
			'No recomendable',
			'Disconforme',
			'Sin problemas',
			'Muy Buen viaje',
			'Todo Excelente',
		];

		$scope.reviewsList = function(){
			$http.post('/reviewsList',{
				userId : $stateParams.userId
			})
			.success(function(reviews){
				$scope.reviews =  reviews;
			})
		};

		$scope.getArray = function(n){
			var input = new Array();
			for (var i = n ; i >= 0; i--) {
				input.push(i);
			};
			return input;
		};

		$scope.myReviews = function(){
			$http.post('/reviewsList',{
				userId : $scope.authentication.user._id
			})
			.success(function(reviews){
				$scope.reviews =  reviews;
			})	
		}


	}
]);
'use strict';

//Articles service used for communicating with the articles REST endpoints
angular.module('reviews').factory('Reviews', ['$resource',
	function($resource) {
		return $resource('reviews/:reviewId', {
			reviewId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);
'use strict';

// Config HTTP Error Handling
angular.module('users').config(['$httpProvider',
	function($httpProvider) {
		// Set the httpProvider "not authorized" interceptor
		$httpProvider.interceptors.push(['$q', '$location', 'Authentication',
			function($q, $location, Authentication) {
				return {
					responseError: function(rejection) {
						switch (rejection.status) {
							case 401:
								// Deauthenticate the global user
								Authentication.user = null;

								// Redirect to signin page
								$location.path('signin');
								break;
							case 403:
								// Add unauthorized behaviour 
								break;
						}

						return $q.reject(rejection);
					}
				};
			}
		]);
	}
]);
'use strict';

// Setting up route
angular.module('users').config(['$stateProvider',
	function($stateProvider) {
		// Users state routing
		$stateProvider.

		state('adminProfile', {
			templateUrl: 'modules/users/views/settings/profile-layout-admin.client.view.html'
		}).
		state('adminProfile.usersList', {
			url: '/usersListAdmin',
			templateUrl: 'modules/users/views/settings/users-list.client.view.html'
		}).
		state('adminProfile.faqsAdmin', {
			url: '/faqsAdmin',
			templateUrl: 'modules/users/views/settings/faqsAdmin.client.view.html'
		}).
		state('adminProfile.newsletterList', {
			url: '/newsletterList',
			templateUrl: 'modules/users/views/settings/newsletter-list.client.view.html'
		}).
		state('adminProfile.newsletterView', {
			url: '/newsletterView/:newsletterId',
			templateUrl: 'modules/users/views/settings/newsletter-view.client.view.html'
		}).
		state('adminProfile.aventonEditAdmin', {
			url: '/editAventonAdmin/:aventonId',
			templateUrl: 'modules/users/views/settings/edit-aventon-admin.client.view.html'
		}).
		state('adminProfile.userProfileAdmin', {
			url: '/userProfileAdmin/:userId',
			templateUrl: 'modules/users/views/settings/userAdmin.client.view.html'
		}).
		state('adminProfile.statistics', {
			url: '/statisticsAdmin',
			templateUrl: 'modules/users/views/settings/statistics.client.view.html'
		}).
		state('adminProfile.aventones', {
			url: '/aventonesAdmin',
			templateUrl: 'modules/users/views/settings/aventones.client.view.html'
		}).
		state('adminProfile.packList', {
			url: '/packListAdmin',
			templateUrl: 'modules/pack/views/pack-list.client.view.html'
		}).
		state('adminProfile.createPack', {
            url: '/createPack',
            templateUrl: 'modules/pack/views/createPack.client.view.html'
        }).
		state('adminProfile.usersNewsletterList', {
			url: '/usersNewsletterListAdmin',
			templateUrl: 'modules/users/views/settings/usersNewsletter-list.client.view.html'
		}).
		state('adminProfile.panel', {
			url: '/panel',
			templateUrl: 'modules/users/views/settings/panel.client.view.html'
		}).     
		state('adminProfile.roleList', {
			url: '/roles',
			templateUrl: 'modules/users/views/settings/roles-list.client.view.html'
		}).
		state('adminProfile.createRole', {
			url: '/roles/create',
			templateUrl: 'modules/users/views/settings/createRole.client.view.html'
		}).
		state('adminProfile.editRole', {
			url: '/roles/:roleId/edit',
			templateUrl: 'modules/users/views/settings/editRole.client.view.html'
		}).
		state('adminProfile.manageRoles', {
			url: '/users/:userId/roles',
			templateUrl: 'modules/users/views/settings/user-roles.client.view.html'
		}).
		state('publicProfile', {
			templateUrl: 'modules/users/views/settings/profile-layout-public.client.view.html'
		}).
		state('publicProfile.review', {
			url: '/profilePublic/:userId',
			templateUrl: 'modules/users/views/settings/review-public.client.view.html'
		}).
		state('profile', {
			templateUrl: 'modules/users/views/settings/profile-layout.client.view.html'
		}).
		state('profile.review', {
			url: '/profile',
			templateUrl: 'modules/users/views/settings/review.client.view.html'
		}).
		state('profile.mytrips', {
			url: '/profile/viajes',
			templateUrl: 'modules/users/views/settings/my-trips.client.view.html'
		}).
		state('profile.myinvitations', {
			url: '/profile/invitaciones',
			templateUrl: 'modules/users/views/settings/my-invitations.client.view.html'
		}).
		state('profile.messages', {
			templateUrl: 'modules/users/views/settings/messages-layout.client.view.html'
		}).
		state('profile.messages.list', {
			url: '/messages',
			templateUrl: 'modules/users/views/settings/messages-list.client.view.html'
		}).
		state('profile.messages.listSent', {
			url: '/messagesSent',
			templateUrl: 'modules/users/views/settings/messages-list-sent.client.view.html'
		}).
		state('profile.messages.listAll', {
			url: '/allMessages',
			templateUrl: 'modules/users/views/settings/messages-list-all.client.view.html'
		}).
		state('profile.messages.detail', {
			url: '/messages/:messageId',
			templateUrl: 'modules/users/views/settings/message-detail.client.view.html'
		}).
		state('profile.messages.detailSent', {
			url: '/messagesSent/:messageId',
			templateUrl: 'modules/users/views/settings/message-detail-sent.client.view.html'
		}).
		state('profile.notifications', {
			url: '/profile/notificaciones',
			templateUrl: 'modules/users/views/settings/notifications.client.view.html'
		}).
		state('profile.settings', {
			templateUrl: 'modules/users/views/settings/settings-layout.client.view.html'
		}).
		state('profile.settings.info', {
			url: '/settings/edit',
			templateUrl: 'modules/users/views/settings/edit-profile.client.view.html'
		}).
		state('profile.settings.cfg', {
			url: '/settings/editCfg',
			templateUrl: 'modules/users/views/settings/edit-cfg.client.view.html'
		}).
		state('profile.settings.imageProfile', {
			url: '/settings/edit/imageProfile',
			templateUrl: 'modules/users/views/settings/edit-image-profile.client.view.html'
		}).
		state('profile.settings.password', {
			url: '/settings/password',
			templateUrl: 'modules/users/views/settings/change-password.client.view.html'
		}).
		state('profile.settings.social', {
			url: '/settings/accounts',
			templateUrl: 'modules/users/views/settings/social-accounts.client.view.html'
		}).
		state('signup', {
			url: '/signup',
			templateUrl: 'modules/users/views/authentication/signup.client.view.html'
		}).
		state('signin', {
			url: '/signin',
			templateUrl: 'modules/users/views/authentication/signin.client.view.html'
		}).
		state('forgot', {
			url: '/password/forgot',
			templateUrl: 'modules/users/views/password/forgot-password.client.view.html'
		}).
		state('reset-invalid', {
			url: '/password/reset/invalid',
			templateUrl: 'modules/users/views/password/reset-password-invalid.client.view.html'
		}).
		state('reset-success', {
			url: '/password/reset/success',
			templateUrl: 'modules/users/views/password/reset-password-success.client.view.html'
		}).
		state('reset', {
			url: '/password/reset/:token',
			templateUrl: 'modules/users/views/password/reset-password.client.view.html'
		});
	}
]);

'use strict';

angular.module('users').controller('AdminsController', ['$scope', '$http', '$location', 'Authentication', 'Users','Packs', '$rootScope', '$stateParams', 'Aventon',
	function($scope, $http, $location, Authentication,Users,Packs, $rootScope, $stateParams, Aventon) {
		$scope.authentication = Authentication;
		
		$scope.showMailBox = false;

		$scope.usersListAdmin = function(){
			$http.get('/usersListAdmin')
			.success(function(users){
				$scope.users = users;
			})
		};

		$scope.usersNewsletterListAdmin = function(){
			$http.get('/usersNewsLetter')
			.success(function(users){
				$scope.users = users;
			})
		};

		$scope.sendMailNewsLetter = function(){
			var contenido = this.content;
			var asunto = this.subject;
			$http.post('/newsletters',{
				subject : asunto,
				content : contenido
			})
			.success(function(){
				$rootScope.alerts.push({type: 'success', msg:'Se ha registrado su newsletter'});
			})
			.error(function(){
				$rootScope.alerts.push({type: 'danger', msg:'Ha ocurrido un error al intentar registrar su newsletter'});
			});

			$http.get('/usersNewsLetter')
			.success(function(users){
				for (var i = users.length - 1; i >= 0; i--) {
					$http.post('/sendEmail',{
						email: users[i].email,
						subject:asunto,
						content: contenido
					}).success(function(){
						console.log("Mail Enviado");
					});
				};
				$rootScope.alerts.push({type: 'success', msg:'Su mensaje ha sido enviado'});
				$scope.content = "";
				$scope.subject = "";
			})
			.error(function(){
				$rootScope.alerts.push({type: 'danger', msg:'Su mensaje no ha podido ser enviado'});
			});
			
		};

		$scope.showMailBoxOn = function(){
			$scope.showMailBox = true;
		}

		$scope.config = "";
		$scope.showMeConfig = function(){
			$http.get('/configs')
			.success(function(configs){
				$scope.config =  configs[0];
			});
		}

		$scope.changeConfig = function(){
			$http.put('/configs/'+ $scope.config._id, {
				price_aventon : this.config.price_aventon
			})
			.success(function(){
				$rootScope.alerts.push({type: 'success', msg:'La configuración se actualizó correctamente'});
			});

		};
		
		$scope.statistics = [];

		$scope.getStatistics = function(){
			// cantidad de usuarios, cantidad de aventones, cantidad de aventones vigentes, 
			//cantidad de aventonados, cantidad de usuarios newsletter
			$http.get('/aventonesQuantity')
			.success(function(data){
				$scope.statistics.push({title:"Cantidad de aventones", value:data.total});
			});
			$http.get('/aventonesLiveQuantity')
			.success(function(data){
				$scope.statistics.push({title:"Cantidad de vigentes", value:data.total});
			});
			$http.get('/aventonesTodayQuantity')
			.success(function(data){
				$scope.statistics.push({title:"Cantidad de aventones que salen hoy", value:data.total});
			});
			$http.get('/usersQuantity')
			.success(function(data){
				$scope.statistics.push({title:"Cantidad de usuarios registrados", value:data.total});
			});
			$http.get('/usersNewsLetterQuantity')
			.success(function(data){
				$scope.statistics.push({title:"Cantidad de usuarios de newsletter registrados", value:data.total});
			});
			$http.get('/demandsQuantity')
			.success(function(data){
				$scope.statistics.push({title:"Cantidad de solicitudes de aventón enviadas", value:data.total});
			});
			$http.get('/demandsAcceptedQuantity')
			.success(function(data){
				$scope.statistics.push({title:"Cantidad de solicitudes de aventón aceptadas", value:data.total});
			});

		};

		$scope.createFaq = function(){
			$http.post('/faqs',{
				question : this.question,
				answer : this.answer
			})
			.success(function(faq){
				$rootScope.alerts.push({type: 'success', msg:'Se ha creado una nueva faq'});
				$scope.listFaq();
				$scope.question = "";
				$scope.answer = "";
				$scope.showMailBox = false;
			});
		};

		$scope.removeFaq = function(id){
			$http.delete('/faqs/'+id)
			.success(function(){
				$rootScope.alerts.push({type: 'success', msg:'Se ha eliminado una faq'});
				$scope.listFaq();
			});
		};

		$scope.updateFaq = function(id){
			$http.put('/faqs/'+id,{
				question: this.faq.question,
				answer: this.faq.answer
			})
			.success(function(faqs){
				$scope.idUpdate = "";
				$scope.listFaq();
			});
		};

		$scope.listFaq = function(){
			$http.get('/faqs')
			.success(function(faqs){
				$scope.faqs = faqs;
			});
		};
		
		$scope.idRemove = "";

		$scope.changeIdRemove = function(id){
			$scope.idRemove = id;
		};

		$scope.idUpdate = "";

		$scope.changeIdUpdate = function(id){
			$scope.idUpdate = id;
			console.log($scope.idUpdate);
		};

		$scope.findUser = function() {
			$http.get('/users/'+$stateParams.userId)
			.success(function(user){
				$scope.user = user;
			});
		};

		$scope.updateUser = function() {
			$http.put('/admin/users/'+$stateParams.userId,
				{
					firstName : this.user.firstName,
					lastName : this.user.lastName,
					credit : this.user.credit,
					email : this.user.email
				})
			.success(function(){
				$rootScope.alerts.push({type: 'success', msg:'Usuario modificado correctamente'});			
			});
		};

		$scope.goUser = function(id){
			$location.path('/userProfileAdmin/'+ id);
		};

		$scope.editRoles = function(id){
			$location.path('/users/'+ id +'/roles');
		};

		$scope.removeUser = function(id){
			$http.delete('users/'+id)
			.success(function(){
				$location.path('/usersListAdmin');
				$rootScope.alerts.push({type: 'success', msg:'Usuario eliminado correctamente'});			
			});
		};

		$scope.totalItems = 0;
		$scope.perPage = 2;
		$scope.currentPage = 1;
		$scope.maxSizeAventones = 10;

		$scope.findAventones = function() {
			var page = $scope.currentPage;
			Aventon.paginateAdmin({page: page}, function(response) {
				$scope.aventones = response.aventones;
				$scope.totalItems = response.total;
			});
		};

		$scope.showCheck = "false";

		$scope.changeShowCheck = function(value){
			$scope.showCheck = value;
		};

		$scope.findCheckAventones = function(){
			$http.get("/showCheckedNeed")
			.success(function(aventones){
				$scope.aventones = aventones;
			});
		};

		$scope.setPage = function (pageNo) {
		    $scope.currentPage = pageNo;
		  };
		$scope.removeAventon = function(id){
			$http.delete('aventon/' + id)
			.success(function(){
				$rootScope.alerts.push({type: 'success', msg:'Aventón eliminado correctamente'});
				$scope.findAventones();
				$http.post('/demandsAcceptedByAventon', {aventonId : id})
				.success(function(demands){
					for (var i = demands.length - 1; i >= 0; i--) {
						var idUser = demands[i].user._id;
						$http.post('/notifications',{
							type: 'cancelAventon',
							anexo_user: idUser							
						})
						.success(function(){
							$http.post('/sendEmail',{
								email: user.email,
								subject:'Aventón cancelado - UnAventon',
								content: 'El usuario '+demands[i].user.displayName+' ha cancelado el aventón ofrecido'
							}).success(function(){
								console.log("Mail Enviado");
							});
						});

						$http.put('/demands/'+id,{
							state : 'Cancelada'
						})
					}; //endFor
				});
			});

		};

		$scope.goUpdateAventon = function(id){
			$location.path('/editAventonAdmin/' + id);
		};

		$scope.blockUser = function(id){
			$http.put('/users/'+id,{
				state : 'Bloqueado'
			})
			.success(function(){
				$scope.usersListAdmin();
				$rootScope.alerts.push({type: 'success', msg:'Usuario bloqueado'});
			});
		};

		$scope.unBlockUser = function(id){
			$http.put('/users/'+id,{
				state : 'Activado'
			})
			.success(function(){
				$scope.usersListAdmin();
				$rootScope.alerts.push({type: 'success', msg:'Usuario desbloqueado'});
			});
		};

		$scope.goNewsletterList = function(id){
			$location.path('/newsletterList');
		};

		$scope.newsletterList = function(){
			$http.get('/newsletters')
			.success(function(data){
				$scope.newsletters =  data;
			})
			.error(function(){
				$rootScope.alerts.push({type: 'success', msg:'Hubo un error y no pudieron cargarse las newsletter enviadas'});
			});
		};

		$scope.goNewsletterView = function(id){
			$location.path('/newsletterView/'+ id);
		};

		$scope.findOneNewsletter = function(){
			$http.get('/newsletters/'+$stateParams.newsletterId)
			.success(function(newsletter){
				$scope.newsletter = newsletter;
			})
			.error(function(){
				$rootScope.alerts.push({type: 'danger', msg:'Hubo un error y no pudo cargarse el newsletter'});
			});
		};

	}
]);
'use strict';

angular.module('users').controller('AuthenticationController', ['$scope', '$http', '$location', 'Authentication', '$rootScope','$stateParams',
	function($scope, $http, $location, Authentication, $rootScope,$stateParams) {
		$scope.authentication = Authentication;

		// If user is signed in then redirect back home
		if ($scope.authentication.user) $location.path('/');

		$scope.signup = function() {
			if($scope.credentials.password != $scope.credentials.password2){
				$rootScope.alerts.push({type: 'danger', msg:'Las contraseñas no coinciden'});
				console.log("no");
			}
			else{
	            $scope.credentials.username=$scope.credentials.email;
				$http.post('/auth/signup', $scope.credentials).success(function(response) {
					// If successful we assign the response to the global user model
					/*response = permissionsObject(response);
					$scope.authentication.user = response;

					// And redirect to the index page
					//$location.path('/');*/
					$scope.success = response.message;
				}).error(function(response) {
					$scope.error = response.message;
				});
			}
		};

		$scope.signin = function() {
			$http.post('/auth/signin', $scope.credentials).success(function(response) {
				// If successful we assign the response to the global user model
				response = permissionsObject(response);
				$scope.authentication.user = response;

				// And redirect to the index page
				$location.path('/');
			}).error(function(response) {
				$scope.error = response.message;
			});
		};


		function permissionsObject(user) {
		    if(user && user.custom_roles) {
		        user.permissions = {};
		        for (var i = user.custom_roles.length - 1; i >= 0; i--) {
		            user.permissions.showMenu = true;
		            if(user.custom_roles[i].permissions) {
			            for (var j = user.custom_roles[i].permissions.length - 1; j >= 0; j--) {
			                user.permissions[user.custom_roles[i].permissions[j]] = true;
			            };
		            }
		        };
		    }
		    return user;		
		};


		$scope.showForgot = 'false';

		$scope.changeShowForgot = function(){
			if($scope.showForgot == 'false')
				$scope.showForgot = 'true';
			else
				$scope.showForgot = 'false';
			console.log($scope.showForgot);
		};

		$scope.initializeValidate = function(){			
			$http.get('/auth/validateuser/'+$stateParams.id+'/'+$stateParams.token)
			.success(function(data){
				$scope.success = data.message;
				console.log(data.msg);
			})
			.error(function(data){
				$scope.error = data.message;
				console.log(data.msg);
			});
		};

	}
]);
'use strict';

angular.module('users').controller('PasswordController', ['$scope', '$stateParams', '$http', '$location', 'Authentication', '$rootScope',
	function($scope, $stateParams, $http, $location, Authentication, $rootScope) {
		$scope.authentication = Authentication;

		//If user is signed in then redirect back home
		if ($scope.authentication.user) $location.path('/');

		// Submit forgotten password account id
		$scope.askForPasswordReset = function() {
			$scope.success = $scope.error = null;

			$http.post('/auth/forgot', $scope.credentials).success(function(response) {
				// Show user success message and clear form
				$scope.credentials = null;
				$scope.success = response.message;

			}).error(function(response) {
				// Show user error message and clear form
				$scope.credentials = null;
				$scope.error = response.message;
			});
		};

		// Change user password
		$scope.resetUserPassword = function() {
			$scope.success = $scope.error = null;

			$http.post('/auth/reset/' + $stateParams.token, $scope.passwordDetails).success(function(response) {
				// If successful show success message and clear form
				$scope.passwordDetails = null;

				// Attach user profile
				Authentication.user = response;

				// And redirect to the index page
				$location.path('/password/reset/success');
			}).error(function(response) {
				$scope.error = response.message;
			});
		};

		

		$scope.recoverPassword = function() {
			$http.post('/auth/forgot',{
				username : this.emailForgot
			})
			.success(function(data){
				$rootScope.alerts.push({type: 'success', msg:'Revise su casilla de correo electrónico por su nueva contraseña'});
			})
			.error(function(){
				$rootScope.alerts.push({type: 'danger', msg:'Hubo un error, intentelo nuevamente'});
			});
			this.emailForgot = "";
		};

	}
]);
'use strict';

angular.module('users').controller('RolesController', ['$scope', '$http', '$location', '$rootScope', '$stateParams', 'Authentication', 'Users', 'Roles',
	function($scope, $http, $location, $rootScope, $stateParams, Authentication, Users, Roles) {
		$scope.authentication = Authentication;

	    $scope.allPermissions = ['Manejo_Newsletter', 'Manejo_Usuarios', 'Manejo_Viajes', 'Manejo_Aventones', 'Estadisticas'];
	    $scope.permissions = [];

		$scope.create = function() {
			var role = new Roles({
				permissions: this.permissions,
				name: this.name
			});
			role.$save(function(response) {
	            $scope.name = "";
	            $scope.permissions = [];
				$location.path('roles');
	            $rootScope.alerts.push({ type: 'success', msg: 'Rol creado correctamente' });
			}, function(errorResponse) {
	            $rootScope.alerts.push({ type: 'error', msg: errorResponse.data.message });
			});
		};

		$scope.remove = function(role) {
			if (role) {
				role.$remove();

				for (var i in $scope.roles) {
					if ($scope.roles[i]._id === role._id) {
						$scope.roles.splice(i, 1);
					}
				}
				rootScope.alerts.push({ type: 'success', msg: 'Rol removido correctamente' });

			} else {
				$scope.role.$remove(function() {
					$location.path('roles');
				});
			}
		};

		$scope.update = function() {
			var role = $scope.role;

			role.$update(function() {
				$location.path('roles/' + role._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.find = function() {
			$scope.roles = Roles.query();
		};

		$scope.findOne = function() {
			$scope.role = Roles.get({
				roleId: $stateParams.roleId
			});
		};
		

	    $scope.updateUser = function() {
	        var user = $scope.user;
	        /*user.$update(function() {
	            $rootScope.alerts.push({ type: 'success', msg: 'Roles de usuario actualizados correctamente' });
	        });*/
	        var user_obj = {
	        	custom_roles: user.custom_roles
	        }
	      	$http.put('/admin/users/'+user._id, user_obj).then(function() {
	            $rootScope.alerts.push({ type: 'success', msg: 'Roles de usuario actualizados correctamente' });
	        });
	    };

	    $scope.findUser = function() {
	        Users.get({
	            userId: $stateParams.userId
	        }, function(user) {
	            $scope.user = user;
	        });
	    };		
	}
]);
'use strict';

angular.module('users').controller('SettingsController', ['$scope', '$http', '$location', 'Users', 'Authentication', '$rootScope', '$stateParams', '$upload', '$timeout',
	function($scope, $http, $location, Users, Authentication, $rootScope, $stateParams, $upload, $timeout) {

		$scope.today = new Date().toJSON();

		$scope.SetActiveTab = function(tab){
			$rootScope.activeTab = tab;
		};

		$scope.user = Authentication.user;

		$scope.getUser = function(){
			$scope.user = Users.get({
				userId: $stateParams.userId
			});
		};

		$scope.internTab = "viajes";

		$scope.internTabChg = function(t){
			$scope.internTab = t;
		};

		$scope.internTabI = "invitaciones";
		
		$scope.internTabIChg = function(t){
			$scope.internTabI = t;
		};

		
		//MOCK reviws and messages
		$scope.message = {
			user:'Frodo Bolsón',
			asunto: 'Hola como estas?',
			date: '20/12/2014',
			text: 'Curabitur tincidunt tortor quis metus dapibus feugiat. Morbi ornare tortor quis sapien maximus tristique. Fusce eget convallis eros. Donec vel odio ut dolor suscipit dictum eget ac mi. Sed imperdiet, dui quis accumsan lobortis, leo enim pellentesque odio, eget euismod nunc orci sit amet risus. Mauris dictum justo in velit suscipit fringilla eget a ex. Mauris in nisl nisi. Nulla sollicitudin nisi sed erat dignissim consequat. Proin ultrices nulla enim. Mauris et nunc libero. Morbi tincidunt imperdiet tellus vel condimentum. Ut commodo hendrerit est, vulputate posuere mi consectetur non. Integer eu erat vulputate, hendrerit lorem ac, tincidunt urna. '
		};
		
		$scope.user.reviews = [{
			user:'Frodo Bolsón',
			date: 'hace 2 días',
			vote: 'Todo excelente',
			description: 'Con golum fuimos y volvimos a Buenos Aires el anterior invierno, la verdad que salio todo perfecto, ahorramos unos pesos y nos hicimos buenos amigos'
		},
			{
				user:'Frodo Bolsón',
				date: 'hace 2 días',
				vote: 'Todo excelente',
				description: 'Con golum fuimos y volvimos a Buenos Aires el anterior invierno, la verdad que salio todo perfecto, ahorramos unos pesos y nos hicimos buenos amigos'
			},
			{
				user:'Frodo Bolsón',
				date: 'hace 2 días',
				vote: 'Todo excelente',
				description: 'Con golum fuimos y volvimos a Buenos Aires el anterior invierno, la verdad que salio todo perfecto, ahorramos unos pesos y nos hicimos buenos amigos'
			},
			{
				user:'Frodo Bolsón',
				date: 'hace 2 días',
				vote: 'Todo excelente',
				description: 'Con golum fuimos y volvimos a Buenos Aires el anterior invierno, la verdad que salio todo perfecto, ahorramos unos pesos y nos hicimos buenos amigos'
			},
			{
				user:'Frodo Bolsón',
				date: 'hace 2 días',
				vote: 'Todo excelente',
				description: 'Con golum fuimos y volvimos a Buenos Aires el anterior invierno, la verdad que salio todo perfecto, ahorramos unos pesos y nos hicimos buenos amigos'
			}
		];
		// If user is not signed in then redirect back home
		if (!$scope.user) $location.path('/');

		// Check if there are additional accounts 
		$scope.hasConnectedAdditionalSocialAccounts = function(provider) {
			for (var i in $scope.user.additionalProvidersData) {
				return true;
			}

			return false;
		};

		// Check if provider is already in use with current user
		$scope.isConnectedSocialAccount = function(provider) {
			return $scope.user.provider === provider || ($scope.user.additionalProvidersData && $scope.user.additionalProvidersData[provider]);
		};

		// Remove a user social account
		$scope.removeUserSocialAccount = function(provider) {
			$scope.success = $scope.error = null;

			$http.delete('/users/accounts', {
				params: {
					provider: provider
				}
			}).success(function(response) {
				// If successful show success message and clear form
				$scope.success = true;
				$scope.user = Authentication.user = response;
			}).error(function(response) {
				$scope.error = response.message;
			});
		};

		// Update a user profile
		$scope.updateUserProfile = function(isValid) {
			if (isValid){
				$scope.success = $scope.error = null;
				var user = new Users($scope.user);
	
				user.$update(function(response) {
					$scope.success = true;
					Authentication.user = response;
				}, function(response) {
					$scope.error = response.data.message;
				});

				if($scope.newsletter && !$scope.newsletterOld){
					$http.post('/subscribe', {email : user.email, state:'Activado'});
					$scope.checkUserNewsLetter();
				}
				if(!$scope.newsletter && $scope.newsletterOld) {
					$http.post('/deleteSuscriptorByEmail', {email : user.email})
					$scope.checkUserNewsLetter();
				}

			} else {
				$scope.submitted = true;
			}

		};

		$scope.range = function(min, max, step){
	        step = step || 1;
	        var input = [];
	        for (var i = min; i <= max; i += step) input.push(i);
	            return input;
	    };

	    $scope.addPatent = function(patent){
	    	if(patent != ""){
		    	if(!$scope.user.patents)
		    		$scope.user.patents = [];
		    	var obj = new Object();
		    	obj.patent = patent;
		    	$scope.user.patents.push(obj);
		    	$scope.newPatent = "";
		    }
	    };

	    $scope.removePatent = function(index){
	        $scope.user.patents.splice(index,1);
	    };

		$scope.checkUserNewsLetter = function(){
			$http.post('/getUserNewsLetterByEmail',{email: $scope.user.email})
			.success(function(){
				$scope.newsletter = true;
				$scope.newsletterOld = true;
			})
			.error(function(){
				$scope.newsletter = false;
				$scope.newsletterOld = false;
			});
		}

		// Change user password
		$scope.changeUserPassword = function() {
			$scope.success = $scope.error = null;

			$http.post('/users/password', $scope.passwordDetails).success(function(response) {
				// If successful show success message and clear form
				$scope.success = true;
				$scope.passwordDetails = null;
			}).error(function(response) {
				$scope.error = response.message;
			});
		};

		//return true if the current user is registered whit a local account, otherwise return false
		$scope.isLocal = function() {
			return $scope.user.provider == "local"

		}

		$scope.goChangeImageProfile = function(){
			$location.path('/settings/edit/imageProfile');
		};

		$scope.saveImageProfile = function(files){
			$scope.file = files[0];
		};

		$scope.upload = function () {
			var file = $scope.file;
	        if (file) {
	        		$scope.generateThumb(file);
	                $upload.upload({
	                    url: '/uploadImage/'+$scope.user._id,
	                    fields: {},
	                    file: file
	                }).progress(function (evt) {
	                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
	                    console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
	                }).success(function (data, status, headers, config) {
	                    console.log('file ' + config.file.name + 'uploaded. Response: ' + data);
	                    
	                    var user = new Users($scope.user);
						user.imageProfile = data.file.path;
						user.imageProfile = user.imageProfile.replace('public/','');

						user.$update(function(response) {
							$scope.user.imageProfile = file.dataUrl;
							$rootScope.alerts.push({type: 'success', msg:'Se ha cargado correctamente su nueva imágen de perfil'});
						}, function error(err) {
							$rootScope.alerts.push({type: 'danger', msg:'Hubo un error, intente nuevamente.'});
						});
	                    
	                });
	        }
	    };

	    $scope.generateThumb = function(file) {
            if (file != null) {
                $timeout(function() {
                    var fileReader = new FileReader();
                    fileReader.readAsDataURL(file);
                    fileReader.onload = function(e) {
                        $timeout(function() {
                            file.dataUrl = e.target.result;
                        });
                    }
                });
            }
        };

	}
]);

'use strict';

angular.module('users').controller('UsersController', ['$scope', '$http', '$location', 'Authentication', 'Users', '$rootScope',
	function($scope, $http, $location, Authentication,Users, $rootScope) {
		$scope.authentication = Authentication;


		
	}
]);
'use strict';

// Authentication service for user variables
angular.module('users').factory('Authentication', [

	function() {
		var _this = this;

		_this._data = {
			user: window.user
		};

		return _this._data;
	}
]);
'use strict';

// Users service used for communicating with the roles REST endpoint
angular.module('users').factory('Roles', ['$resource',
	function($resource) {
		return $resource('roles/:roleId', {
			roleId : '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);
'use strict';

// Users service used for communicating with the users REST endpoint
angular.module('users').factory('Users', ['$resource',
	function($resource) {
		return $resource('users/:userId', {
			userId : '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);
'use strict';

// Configuring the Articles module
angular.module('articles').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Articles', 'articles', 'dropdown', '/articles(/create)?');
		Menus.addSubMenuItem('topbar', 'articles', 'List Articles', 'articles');
		Menus.addSubMenuItem('topbar', 'articles', 'New Article', 'articles/create');
	}
]);
'use strict';

// Setting up route
angular.module('usersNewsLetter').config(['$stateProvider',
	function($stateProvider) {
		// Articles state routing
		$stateProvider.
		state('createUserNewsLetter', {
			url: '/usersNewsLetter/create',
			templateUrl: 'modules/usersNewsLetter/views/create-userNewsLetter.client.view.html'
		})
		.state('authenticateUserNewsletter', {
			url : '/authenticateUserNewsLetter/:userNewsLetterId',
			templateUrl : 'modules/usersNewsLetter/views/authenticate-user-newsletter.client.view.html'
		});
	}
]);
'use strict';

angular.module('usersNewsLetter').controller('UsersNewsLetterController', ['$scope', '$stateParams', '$location', 'Authentication', 'UsersNewsLetter', '$rootScope', '$http',
	function($scope, $stateParams, $location, Authentication, UsersNewsLetter, $rootScope, $http) {
		$scope.authentication = Authentication;

		$scope.create = function() {
			var userNewsLetter = new UsersNewsLetter({
				email: this.email
			});

			userNewsLetter.$save(function(response) {
				$http.post('/authenticateusersend', {
					email : userNewsLetter.email,
					id : userNewsLetter._id
				});
				$rootScope.alerts.push({type: 'success', msg:'Suscripción realizada, revise su correo para completar la suscripción'});
				$scope.email = '';	
			}, function(errorResponse) {
				$scope.email = '';
				console.log(errorResponse);
				$rootScope.alerts.push({type: 'danger', msg:'No se ha podido suscribir: ' + errorResponse.data.message});
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.preCreate = function() {
			if(this.unsuscribe){
				$http.post("/deleteSuscriptorByEmail", {
					"email" : this.email
				})
				.success(function(data){
					$rootScope.alerts.push({type: 'success', msg:'Suscripción eliminada'});	
					$scope.email = '';
				});
			}
			else{
				$scope.create();
			}
		};


		$scope.remove = function(article) {
			if (article) {
				article.$remove();

				for (var i in $scope.articles) {
					if ($scope.articles[i] === article) {
						$scope.articles.splice(i, 1);
					}
				}
			} else {
				$scope.article.$remove(function() {
					$location.path('articles');
				});
			}
		};

		$scope.update = function() {
			var article = $scope.article;

			article.$update(function() {
				$location.path('articles/' + article._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.find = function() {
			$scope.articles = Articles.query();
		};

		$scope.findOne = function() {
			$scope.article = Articles.get({
				articleId: $stateParams.articleId
			});
		};

		$scope.showUnsuscribeValue = false;

		$scope.showUnsuscribe = function() {
			$scope.showUnsuscribeValue = true;			
		};


		$scope.enabledUser = function(){
			console.log($stateParams.userNewsLetterId);
			$http.post('/authenticateUser/'+ $stateParams.userNewsLetterId)
			.success(function(){
				$rootScope.alerts.push({type: 'success', msg:'El usuario ha sido activado correctamente'});
				$location.path('/');
			})
			.error(function(){
				$rootScope.alerts.push({type: 'error', msg:'El usuario no se ha activado, intente nuevamente'});
			});
		};


	}
]);
'use strict';

//Articles service used for communicating with the articles REST endpoints
angular.module('usersNewsLetter').factory('UsersNewsLetter', ['$resource',
	function($resource) {
		return $resource('usersNewsLetter/:userNewsLetterId', {
			userNewsLetterId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);