'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * aventon Schema
 */
var AventonSchema = new Schema({
	created: {
		type: Date,
		default: Date.now
	},
	start_place: {
		type: String,
		default: '',
	},
	end_place: {
		type: String,
		default: '',
	},
	pos_start: {
		type: [Number], // [<longitude>, <latitude>]
      	index: '2dsphere' //2d
	},
	pos_end: {
		type: [Number], 
      	index: '2dsphere'
	},
	start_hour: {
		type: String,
		default: '',
	},
	end_hour: {
		type: String,
		default: '',
	},
	start_day: {
		type: Date,
		default: '',
	},
	end_day: {
		type: Date,
		default: '',
	},
	places_list: [{
		type: String,
		default: '',
	}],
	tags_search: [{
		type: String,
		default: '',
	}],
	tags_pos: [{
		pos: [{type : Number,index: '2dsphere'}]
	}],
	seats_available: {
		type: Number,
		default: 0,
	},
	more_info: {
		type: String,
		default: "",
	},
	recurrent_travel: {
		type: Boolean,
		default: false,
	},
	frequency: {
		type: String,
		default:""
	},
	patent_show: {
		type: Boolean,
		default: false,
	},
	time_flexibility : {
		type: Boolean,
		default : false
	},
	until_hour: {
		type: String,
		default: '',
	},
	from_hour: {
		type: String,
		default: '',
	},
	from_day: {
		type: Date,
		default: '',
	},
	until_day: {
		type: Date,
		default: '',
	},
	until_hour_end: {
		type: String,
		default: '',
	},
	from_hour_end: {
		type: String,
		default: '',
	},
	from_day_end: {
		type: Date,
		default: '',
	},
	until_day_end: {
		type: Date,
		default: '',
	},
	seat_price: {
		type: Number,
		default:0
	},
	private_price: {
		type: Boolean,
		default: false
	},
	consultations:[{
		question: {type: String},
		answer: {type: String},
		responded: {type: Boolean, default:false},
		date_question:{type: Date, default: Date.now},
		date_answer:{type: Date},
		user_name:{type: String},
		user_id:{type: String},
		state:{type: String, default:'visible'} //check, visible, blocked	
	}],	
	user:{
		type: Schema.ObjectId,
		ref:'User'
	}
});


mongoose.model('Aventon', AventonSchema);