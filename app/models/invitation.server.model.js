'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * invitation Schema
 */
var InvitationSchema = new Schema({
	created: {
		type: Date,
		default: Date.now
	},
	state: {
		type: String,
		default: 'unChecked' // unChecked, accepted, rejected
	},
	responded: {
		type: Boolean,
		default: false
	},
	aventon:{
		type: Schema.ObjectId,
		ref:'Aventon'
	},
	user_sender:{
		type: Schema.ObjectId,
		ref:'User'
	},
	user_receiver:{
		type: Schema.ObjectId,
		ref:'User'
	}
});

mongoose.model('Invitation', InvitationSchema);