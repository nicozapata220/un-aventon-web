'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * UserNewsLetter Schema
 */
var UserNewsLetterSchema = new Schema({
	created: {
		type: Date,
		default: Date.now
	},
	email: {
		type: String,
		index : {unique : true},
		default: '',
		trim: true
	},
	state: {
		type : String,
		default : "EsperandoActivacion" // Activado
	}
});

mongoose.model('UserNewsLetter', UserNewsLetterSchema);