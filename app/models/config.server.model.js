'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * config Schema
 */
var ConfigSchema = new Schema({
	created: {
		type: Date,
		default: Date.now
	},
	price_aventon: {
		type: Number,
		default: 10
	}
});

mongoose.model('Config', ConfigSchema);