'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * message Schema
 */
var MessageSchema = new Schema({
	created: {
		type: Date,
		default: Date.now
	},
	state: {
		type: String,
		default: 'unChecked' // or checked
	},
	content: {
		type: String,
		default: ''
	},
	subject: {
		type: String,
		default: ''
	},
	user_sender:{
		type: Schema.ObjectId,
		ref:'User'
	},
	user_receiver:{
		type: Schema.ObjectId,
		ref:'User'
	}
});

mongoose.model('Message', MessageSchema);