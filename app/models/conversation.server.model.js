'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * conversation Schema
 */
var ConversationSchema = new Schema({
	created: {
		type: Date,
		default: Date.now
	},
	members:[{
		type: Schema.ObjectId,
		ref:'User'
	}]
});

mongoose.model('Conversation', ConversationSchema);