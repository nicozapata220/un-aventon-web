'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * faq Schema
 */
var NewsletterSchema = new Schema({
	created: {
		type: Date,
		default: Date.now
	},
	subject: {
		type: String,
		default: ""
	},
	content: {
		type: String,
		default: ""
	}
});

mongoose.model('Newsletter', NewsletterSchema);