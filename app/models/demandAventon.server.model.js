'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * demand Schema
 */
var DemandAventonSchema = new Schema({
	created: {
		type: Date,
		default: Date.now
	},
	state: {
		type: String,
		default: ''
	},
	start_place:{
		type: String,
		default:''
	},
	end_place:{
		type: String,
		default:''
	},
	day:{
		type: Date,
		default:''
	},
	comment:{
		type: String,
		default:''
	},
	recurrent_travel: {
		type: Boolean,
		default: false,
	},
	frequency: {
		type: String,
		default:""
	},
	time_flexibility : {
		type: Boolean,
		default : false
	},
	until_hour: {
		type: String,
		default: '',
	},
	from_hour: {
		type: String,
		default: '',
	},
	from_day: {
		type: Date,
		default: '',
	},
	until_day: {
		type: Date,
		default: '',
	},
	luggage_type: {
		type: String,
		default: ''
	},
	tags_pos: [{
		pos: [{type : Number,index: '2dsphere'}]
	}],
	user:{
		type: Schema.ObjectId,
		ref: 'User'
	}
});

mongoose.model('DemandAventon', DemandAventonSchema);