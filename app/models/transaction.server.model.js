'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Transaction Schema
 */
var TransactionSchema = new Schema({
    created: {
        type: Date,
        default: Date.now
    },
    pack: {
            type: Schema.ObjectId,
            ref: 'Pack'
    },
    user: {
            type: Schema.ObjectId,
            ref: 'User'
    },
    mercado_pago_id: String,
    estado: String
});

TransactionSchema.statics = {
    findByMPId: function(id, cb) {
        this.findOne({
            mercado_pago_id: id
        }).populate('user').populate('pack').exec(cb);
    }
};

mongoose.model('Transaction', TransactionSchema);