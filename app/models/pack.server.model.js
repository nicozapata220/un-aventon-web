'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Pack Schema
 */
var PackSchema = new Schema({
    created: {
        type: Date,
        default: Date.now
    },
    precio: {
        type: Number
    },
    nombre: {
        type: String,
        default: '',
        trim: true
    },      
    cantidad_pines: {
        type: Number
    }
});

PackSchema.path('precio').validate(function(precio) {
    return precio > 0;
}, 'Precio debe ser mayor que cero');

PackSchema.path('cantidad_pines').validate(function(cantidad_pines) {
    return cantidad_pines > 0;
}, 'La cantidad de pines debe ser mayor que cero');

PackSchema.statics = {
    load: function(id, cb) {
        this.findOne({
            _id: id
        }).exec(cb);
    }
};

mongoose.model('Pack', PackSchema);