'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	crypto = require('crypto');

/**
 * A Validation function for local strategy properties
 */
var validateLocalStrategyProperty = function(property) {
	return ((this.provider !== 'local' && !this.updated) || property.length);
};

/**
 * A Validation function for local strategy password
 */
var validateLocalStrategyPassword = function(password) {
	return (this.provider !== 'local' || (password && password.length > 6));
};

/**
 * User Schema
 */
var UserSchema = new Schema({
	firstName: {
		type: String,
		trim: true,
		default: '',
		validate: [validateLocalStrategyProperty, 'Por favor complete su nombre']
	},
	credit : {
		type: Number,
		default: 0
	},
	lastName: {
		type: String,
		trim: true,
		default: '',
		validate: [validateLocalStrategyProperty, 'Por favor complete su apellido']
	},
	patents : [{
		patent:{type : String, trim: true,default: ''},
		validate:{type:Boolean, default:false}
	}],
	cellphone:{
		type : String,
		trim : true,
		default: ''
	},
	cfg_notify:{
		recommended : { type : Boolean, default : true},
		trip : { type : Boolean, default : true},
		review : { type : Boolean, default : true},
		message : { type : Boolean, default : true},
		cancelAventonSite : { type : Boolean, default : true},
		cancelAventon : { type : Boolean, default : true}
	},
	fails_pos: [{
		pos:[{type : Number,index: '2dsphere'}],
		date: {type: Date, default: Date.now}
	}],
	smoker: {
		type: Boolean
	},
	music:{
		type: String,
		default: ''
	},
	topicsOfInterest: {
		type: String,
		default: ''
	},
	state: {
		type : String,
		default : 'EsperandoActivación' // Activado, Bloqueado
	},
	displayName: {
		type: String,
		trim: true
	},
	role : {
		type : String,
		default: 'user' //user, admin
	},
	email: {
		type: String,
		trim: true,
		unique: 'testing error message',
		default: '',
		validate: [validateLocalStrategyProperty, 'Por favor complete su email'],
		match: [/.+\@.+\..+/, 'Por favor ingrese un email correcto']
	},
	username: {
		type: String,
		unique: 'testing error message',
		trim: true
	},
	imageProfile :{
		type: String,
		default: ''
	},
	review_point:{
		type: Number,
		default: 0
	},
	review_count:{
		type: Number,
		default: 0
	},
	password: {
		type: String,
		default: '',
		validate: [validateLocalStrategyPassword, 'La contraseña debe tener al menos 8 caracteres']
	},
	salt: {
		type: String
	},
	provider: {
		type: String,
		required: 'Provider is required'
	},
	providerData: {},
	additionalProvidersData: {},
	/*roles: {
		type: [{
			type: String,
			enum: ['user', 'admin']
		}],
		default: ['user']
	},*/
    custom_roles: [{
        type: Schema.ObjectId,
        ref: 'Role'
    }],
	updated: {
		type: Date
	},
	created: {
		type: Date,
		default: Date.now
	},
	/* For reset password */
	resetPasswordToken: {
		type: String
	},
  	resetPasswordExpires: {
  		type: Date
  	}
});

/**
 * Hook a pre save method to hash the password
 */
UserSchema.pre('save', function(next) {
	if (!this.salt && this.password && this.password.length > 6) {
		this.salt = new Buffer(crypto.randomBytes(16).toString('base64'), 'base64');
		this.password = this.hashPassword(this.password);
	}

	next();
});

/**
 * Create instance method for hashing a password
 */
UserSchema.methods.hashPassword = function(password) {
	if (this.salt && password) {
		return crypto.pbkdf2Sync(password, this.salt, 10000, 64).toString('base64');
	} else {
		return password;
	}
};

/**
 * Create instance method for hashing a password
 */
UserSchema.methods.updatePassword = function() {
	if (this.password && this.password.length > 6) {
		this.salt = new Buffer(crypto.randomBytes(16).toString('base64'), 'base64');
		this.password = this.hashPassword(this.password);
	}
};

/**
 * Create instance method for authenticating user
 */
UserSchema.methods.authenticate = function(password) {
	return this.password === this.hashPassword(password);
};

/**
 * Find possible not used username
 */
UserSchema.statics.findUniqueUsername = function(username, suffix, callback) {
	var _this = this;
	var possibleUsername = username + (suffix || '');

	_this.findOne({
		username: possibleUsername
	}, function(err, user) {
		if (!err) {
			if (!user) {
				callback(possibleUsername);
			} else {
				return _this.findUniqueUsername(username, (suffix || 0) + 1, callback);
			}
		} else {
			callback(null);
		}
	});
};

mongoose.model('User', UserSchema);