'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * Role Schema
 */
var RoleSchema = new Schema({
    created: {
        type: Date,
        default: Date.now
    },
    name: {
        type: String,
        default: '',
        trim: true
    },
    permissions: [{
        type: String,
        enum: ['Manejo_Newsletter', 'Manejo_Usuarios', 'Manejo_Viajes', 'Manejo_Aventones', 'Estadisticas']
    }]
});


RoleSchema.statics = {

    load: function(id, cb) {
        var self = this;
        self
        .findOne({
                _id: id
            })
        .sort('-created')
        .exec(function(err, game) {
            cb(err, game);
        });
    },
};

mongoose.model('Role', RoleSchema);