'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * review Schema
 */

var ReviewSchema = new Schema({
	created: {
		type: Date,
		default: Date.now
	},
	state: {
		type: String,
		default: 'unChecked' // or 'checked'
	},
	comment: {
		type: String,
		default: ''
	},
	valoration: {
		/*
		5 - Todo Excelente
		4 - Muy Buen viaje
		3 - Sin problemas
		2 - Disconforme
		1 - No recomendable
		*/
		type: Number,
		default: 0
	},
	user_origin:{
		type: Schema.ObjectId,
		ref:'User'
	},
	user_receiver:{
		type: Schema.ObjectId,
		ref:'User'
	},
	aventon:{
		type: Schema.ObjectId,
		ref:'Aventon'
	}
});

mongoose.model('Review', ReviewSchema);