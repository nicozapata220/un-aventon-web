'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * chatMsg Schema
 */
var ChatMsgSchema = new Schema({
	created: {
		type: Date,
		default: Date.now
	},
	user_a:{
		type: Schema.ObjectId,
		ref:'User'
	},
	user_b:{
		type: Schema.ObjectId,
		ref:'User'
	},
	displayName_a :{
		type: String,
		default:''
	},
	displayName_b :{
		type: String,
		default:''
	},
	content :{
		type: String,
		default:''
	},
	conversation : {
		type : Schema.ObjectId,
		ref : 'Conversation'
	}
});

mongoose.model('ChatMsg', ChatMsgSchema);