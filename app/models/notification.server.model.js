'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * notification Schema
 */
var NotificationSchema = new Schema({
	created: {
		type: Date,
		default: Date.now
	},
	state: {
		type: String,
		default: 'unChecked'
	},
	comment: {
		type: String,
		default: ''
	},
	type: { //message, review, trip, demandA, cancelAventon, cancelAventonSite, invitation
		type: String,
		default: ''
	},
	user:{
		type: Schema.ObjectId,
		ref:'User'
	},
	anexo_user:{
		type: Schema.ObjectId,
		ref:'User'
	},
	review:{
		type: Schema.ObjectId,
		ref:'Review'
	},
	aventon:{
		type: Schema.ObjectId,
		ref:'Aventon'
	},
	message:{
		type: Schema.ObjectId,
		ref: 'Message'
	}
});

mongoose.model('Notification', NotificationSchema);