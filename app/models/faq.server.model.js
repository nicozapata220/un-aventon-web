'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * faq Schema
 */
var FaqSchema = new Schema({
	created: {
		type: Date,
		default: Date.now
	},
	question: {
		type: String,
		default: ""
	},
	answer: {
		type: String,
		default: ""
	}
});

mongoose.model('Faq', FaqSchema);