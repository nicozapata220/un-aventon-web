'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * demand Schema
 */
var DemandSchema = new Schema({
	created: {
		type: Date,
		default: Date.now
	},
	state: {
		type: String,
		default: 'Espera', //Aceptada o Rechazada, Cancelada
	},
	user:{
		type: Schema.ObjectId,
		ref:'User'
	},
	aventon:{
		type: Schema.ObjectId,
		ref:'Aventon'
	}
});

mongoose.model('Demand', DemandSchema);