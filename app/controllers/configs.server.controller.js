'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Config = mongoose.model('Config'),
	_ = require('lodash');


/**
 * Create a notification
 */
exports.create = function(req, res) {
	var config = new Config(req.body);
	
	config.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(config);
		}
	});
};

/**
 * Show the current notification
 */
exports.read = function(req, res) {
	res.json(req.config);
};

/**
 * Update a config
 */
exports.update = function(req, res) {
	var config = req.config;

	config = _.extend(config, req.body);

	config.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(config);
		}
	});
};

/**
 * Delete an config
 */
exports.delete = function(req, res) {
	var config = req.config;

	config.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(config);
		}
	});
};

/**
 * List of config
 */
exports.list = function(req, res) {
	Config.find().sort('-created').exec(function(err, configs) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(configs);
		}
	});
};

/**
 * config middleware
 */
exports.configByID = function(req, res, next, id) {
	Config.findById(id).exec(function(err, config) {
		if (err) return next(err);
		if (!config) return next(new Error('Failed to load config ' + id));
		req.config = config;
		next();
	});
};


