'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Notification = mongoose.model('Notification'),
	User = mongoose.model('User'),
	_ = require('lodash');
var Nodemailer = require('nodemailer');


/**
 * Create a notification
 */
exports.create = function(req, res) {
	User.findById(req.body.user).exec(function(err, user) {
		if (err) return next(err);
		if (!user) return next(new Error('Failed to load user ' + req.body.user));
		var notification = new Notification(req.body);
		var cfg = user.cfg_notify;
		console.log(cfg);
		//message, review, trip, demandA, cancelAventon, cancelAventonSite
		var follow = false;
		if(notification.type == "message" && cfg.message)
			follow = true;
		if(notification.type == "invitation")
			follow = true;
		if(notification.type == "review" && cfg.review)
			follow = true;
		if(notification.type == "trip" && cfg.trip)
			follow = true;
		if(notification.type == "demandA" && cfg.trip)
			follow = true;
		if(notification.type == "cancelAventon" && cfg.cancelAventon)
			follow = true;
		if(notification.type == "cancelAventonSite" && cfg.cancelAventonSite)
			follow = true;
		if(follow){
			notification.save(function(err) {
				if (err) {
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				} else {
					res.json(notification);
				}
			});
		}else{
			return res.status(200).send({
					message: "No enviado por configuración."
					});	
		}
	});
	
};

/**
 * Show the current notification
 */
exports.read = function(req, res) {
	res.json(req.notification);
};

/**
 * Update a notification
 */
exports.update = function(req, res) {
	var notification = req.notification;

	notification = _.extend(notification, req.body);

	notification.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(notification);
		}
	});
};

/**
 * Delete an notification
 */
exports.delete = function(req, res) {
	var notification = req.notification;

	notification.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(notification);
		}
	});
};

/**
 * List of notification
 */
exports.list = function(req, res) {
	Notification.find().sort('-created').exec(function(err, notifications) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(notifications);
		}
	});
};

/**
 * notification middleware
 */
exports.notificationByID = function(req, res, next, id) {
	Notification.findById(id).exec(function(err, notification) {
		if (err) return next(err);
		if (!notification) return next(new Error('Failed to load notification ' + id));
		req.notification = notification;
		next();
	});
};



exports.sendEmail = function (req,res,cb) {
	//email,subject,content
    var transporter = Nodemailer.createTransport({
        service: 'Gmail',
        auth: {
            user: 'aventonprueba@gmail.com',
            pass: '@26481357'
        }
    });
    var mailOptions = {
        to: req.body.email,
        subject: req.body.subject, 
        html: '<b>'+ req.body.content +'</b>' 
    };

    // send mail with defined transport objectW
    transporter.sendMail(mailOptions, function(error, info){
        if(error){
        	console.log(error);
        }
        else{
        	console.log("Enviado: "+info.response);
        	res.json(info.response);
        }
    });
};


exports.notificationsByUser = function(req,res){
	if(req.user) {
		Notification.find({
			user : req.user._id
		})
		.populate('anexo_user review aventon')
		.limit(10)
		.sort('-created').exec(function(err, notifications) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.json(notifications);
			}
		});
	}
	else {
		res.json([]);	
	}
};

exports.allNotificationsByUser = function(req,res){
	if(req.user) {
		Notification.find({
			user : req.user._id
		})
		.populate('anexo_user review aventon')
		.sort('-created').exec(function(err, notifications) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.json(notifications);
			}
		});
	}
	else {
		res.json([]);	
	}
};