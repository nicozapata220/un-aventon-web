'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	DemandAventon = mongoose.model('DemandAventon'),
	_ = require('lodash');

/**
 * Create a demandAventon
 */
exports.create = function(req, res) {
	var demandAventon = new DemandAventon(req.body);
	
	demandAventon.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(demandAventon);
		}
	});
};

/**
 * Show the current demandAventon
 */
exports.read = function(req, res) {
	res.json(req.demandAventon);
};

/**
 * Update a demandAventon
 */
exports.update = function(req, res) {
	var demandAventon = req.demandAventon;

	demandAventon = _.extend(demandAventon, req.body);

	demandAventon.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(demandAventon);
		}
	});
};

/**
 * Delete an demandAventon
 */
exports.delete = function(req, res) {
	var demandAventon = req.demandAventon;

	demandAventon.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(demandAventon);
		}
	});
};

/**
 * List of demandAventon
 */
exports.list = function(req, res) {
	var d = new Date();
	d.setDate(d.getDate() -1);
	
	DemandAventon.find({"day": { $gte: d}}).count().exec(function(err,count){
		console.log(count);	
		var marginSkip = count - 10;
		var skipValue;
		if(marginSkip <= 0)
			skipValue = 0;
		else
			skipValue = Math.floor(Math.random() * marginSkip) + 0;
		DemandAventon.find({
		        "day": { $gte: d}
	    	}).sort('-created').limit(10).skip(skipValue).populate('user').exec(function(err, demandsAventon) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.json(demandsAventon);
			}
		});

	});
};

exports.myDemandsAventon = function(req, res){
	var d = new Date();
	d.setDate(d.getDate() -1);
	DemandAventon.find({
	        "user": req.user._id
    	}).sort('-created').populate('user').exec(function(err, demandsAventon) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(demandsAventon);
		}
	});
};

/**
 * demandAventon middleware
 */
exports.demandAventonByID = function(req, res, next, id) {
	DemandAventon.findById(id).exec(function(err, demandAventon) {
		if (err) return next(err);
		if (!demandAventon) return next(new Error('Failed to load demand ' + id));
		req.demandAventon = demandAventon;
		next();
	});
};

function rad(x) {return x*Math.PI/180;}

function distancia(lat1,lon1,lat2,lon2){
	var R     = 6378.137;//Radio de la tierra en km
	var dLat  = rad( lat2 - lat1 );
	var dLong = rad( lon2 - lon1 );

	var a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(rad(lat1)) * Math.cos(rad(lat2)) * Math.sin(dLong/2) * Math.sin(dLong/2);
	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
	var d = R * c;

	return d.toFixed(3);//Retorna tres decimales
};


exports.searchDemandAventon = function(req,res){
	var start_lat = req.body.start_place.lat;
	var start_lng = req.body.start_place.lng;
	var end_lat = req.body.end_place.lat;
	var end_lng = req.body.end_place.lng;
	
	var dateMax = new Date(req.body.date)
	dateMax.setDate(dateMax.getDate()+7);
	var dateMin = new Date(req.body.date)

	DemandAventon.find({ //primero se busca por fecha (margen de 7 dias)
		"day": { $gte: dateMin, $lt: dateMax}
	})
	.populate('user')
	.exec(function(err, aventones) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			var resultado = [];
			var d1;
			var d2;
			var included = false;
			for (var i = 0; i < aventones.length; i++) {
				included = false;
				for (var j = 0; j < aventones[i].tags_pos.length; j++) {
					//distancia con start
					d1 = distancia(aventones[i].tags_pos[j].pos[1],aventones[i].tags_pos[j].pos[0], start_lat, start_lng);
					//distancia con end
					d2 = distancia(aventones[i].tags_pos[j].pos[1],aventones[i].tags_pos[j].pos[0], end_lat, end_lng);
					
					if(d1 < 5 && !included){
						resultado.push(aventones[i]);
						included = true;
					}
					if(d2 < 5 && !included){
						resultado.push(aventones[i]);
						included = true;
					}

				};
			};
			res.json(resultado);
		}
	});
};