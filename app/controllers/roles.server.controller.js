'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Role = mongoose.model('Role'),
    config = require('../../config/config'),
     _ = require('lodash'),
    fs = require('fs');

/**
 * Find Role by id
 */
exports.roleByID = function(req, res, next, id) {
    Role.load(id, function(err, role) {
        if (err) return next(err);
        if (!role) return next(new Error('Failed to load new ' + id));
        req.role = role;
        next();
    });
};

/**
 * Create a Role
 */
exports.create = function(req, res) {
    var role = new Role(req.body);

    role.validate(function(error) {
        if (error) {
          res.json({ error : error });
        } else {
            role.save(function(err) {
                if (err) {
                    res.render('error', {
                        status: 500
                    });
                } else {
                    res.jsonp(role);
                }
            });
        }
    });

};

/**
 * Update a Role
 */
exports.update = function(req, res) {
    var role = req.role;

    role = _.extend(role, req.body);

    role.save(function(err) {
        res.jsonp(role);
    });
};

/**
 * Delete an Role
 */
exports.delete = function(req, res) {
    var role = req.role;

    role.remove(function(err) {
        if (err) {
            res.render('error', {
                status: 500
            });
        } else {
            res.jsonp(role);
        }
    });
};


/**
 * Show an role
 */
exports.read = function(req, res) {
    res.jsonp(req.role);    
}
    
/**
 * List of Bands
 */
exports.list = function(req, res) {
    Role.find().sort('-created').exec(function(err, roles) {
        if (err) {
            res.render('error', {
                status: 500
            });
        } else {
            res.jsonp(roles);
        }
    });
};
