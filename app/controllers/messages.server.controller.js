'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Message = mongoose.model('Message'),
	_ = require('lodash');

/**
 * Create a message
 */
exports.create = function(req, res) {
	var message = new Message(req.body);
	
	message.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(message);
		}
	});
};

/**
 * Show the current message
 */
exports.read = function(req, res) {
	res.json(req.message);
};

/**
 * Update a message
 */
exports.update = function(req, res) {
	var message = req.message;

	message = _.extend(message, req.body);

	message.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(message);
		}
	});
};

/**
 * Delete an message
 */
exports.delete = function(req, res) {
	var message = req.message;

	message.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(message);
		}
	});
};

/**
 * List of message
 */
exports.list = function(req, res) {
	Message.find().sort('-created').exec(function(err, messsages) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(messages);
		}
	});
};

/**
 * message middleware
 */
exports.messageByID = function(req, res, next, id) {
	Message.findById(id).exec(function(err, message) {
		if (err) return next(err);
		if (!message) return next(new Error('Failed to load message ' + id));
		req.message = message;
		next();
	});
};


exports.messagesToMe = function(req,res){
	Message.find({'user_receiver': req.user._id}).populate('user_sender','firstName lastName email username _id').exec(function(err,messages){
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(messages);
		}	
	});
};

exports.messagesSent = function(req,res){
	Message.find({'user_sender': req.user._id}).populate('user_receiver','firstName lastName email username _id').exec(function(err,messages){
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(messages);
		}	
	});
};

exports.allMessages = function(req,res){
	Message.find({$or: [ {'user_receiver': req.user._id}, 
											 {'user_sender': req.user._id} ] })
				 .populate('user_receiver','firstName lastName email username _id')
				 .populate('user_sender','firstName lastName email username _id')
				 .exec(function(err,messages){
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(messages);
		}	
	});
}

exports.allMessagesByUser = function(req,res){
	var contact = req.param('contact');
	console.log(contact);
	console.log(req.user._id);
	Message.find({$or: [ {'user_receiver': req.user._id, 'user_sender': contact}, 
											 {'user_sender': req.user._id,'user_receiver': contact} ] })
				 .populate('user_receiver','firstName lastName email username _id')
				 .populate('user_sender','firstName lastName email username _id')
				 .exec(function(err,messages){
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(messages);
		}	
	});
}

// exports.allMessages = function(req,res){
// 	Message.find({$or: [ {'user_receiver': req.user._id}, 
// 											 {'user_sender': req.user._id} ] })
// 				 .populate('user_receiver','firstName lastName email username _id')
// 				 .populate('user_sender','firstName lastName email username _id')
// 				 .distinct('user_receiver','user_receiver',function(err,messages){
// 						if (err) {
// 							return res.status(400).send({
// 								message: errorHandler.getErrorMessage(err)
// 							});
// 						} else {
// 							console.log(messages);

// 							res.json(messages);
// 						}	
// 	});
// }