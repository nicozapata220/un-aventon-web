'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Faq = mongoose.model('Faq'),
	_ = require('lodash');


/**
 * Create a faq
 */
exports.create = function(req, res) {
	var faq = new Faq(req.body);
	
	faq.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(faq);
		}
	});
};

/**
 * Show the current faq
 */
exports.read = function(req, res) {
	res.json(req.faq);
};

/**
 * Update a faq
 */
exports.update = function(req, res) {
	var faq = req.faq;

	faq = _.extend(faq, req.body);

	faq.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(faq);
		}
	});
};

/**
 * Delete an faq
 */
exports.delete = function(req, res) {
	var faq = req.faq;

	faq.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(faq);
		}
	});
};

/**
 * List of faq
 */
exports.list = function(req, res) {
	Faq.find().sort('-created').exec(function(err, faqs) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(faqs);
		}
	});
};

/**
 * config middleware
 */
exports.faqByID = function(req, res, next, id) {
	Faq.findById(id).exec(function(err, faq) {
		if (err) return next(err);
		if (!faq) return next(new Error('Failed to load faq ' + id));
		req.faq = faq;
		next();
	});
};


