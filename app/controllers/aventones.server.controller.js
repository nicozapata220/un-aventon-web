'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Aventon = mongoose.model('Aventon'),
	User = mongoose.model('User'),
	_ = require('lodash');
var Nodemailer = require('nodemailer');


/**
 * Create a aventon
 */
exports.create = function(req, res) {
	var aventon = new Aventon(req.body);
	
	aventon.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			//buscar usuarios con destino cercanos
			var dist0;
			var dist1;
			var distDates;
			var notified = false;
			var url;
			User.find().exec(function(err,users){
				for (var i = 0; i < users.length; i++) {
					notified = false;
					if(users[i].cfg_notify.recommended && users[i].fails_pos && (req.user._id.toHexString() != users[i]._id)){
						for (var j = 0; j < users[i].fails_pos.length; j++) {
							distDates = daysBDates(users[i].fails_pos[j].date, Date.now());
							dist0 = distancia(aventon.pos_start[1],aventon.pos_start[0],users[i].fails_pos[j].pos[1],users[i].fails_pos[j].pos[0]);
							dist1 = distancia(aventon.pos_end[1],aventon.pos_end[0],users[i].fails_pos[j].pos[1],users[i].fails_pos[j].pos[0]);
							console.log(distDates);
							if((dist0 < 5 || dist1 < 5) && !notified && (distDates < 30) ){
								notified = true;
								url = "http://http://polar-waters-6488.herokuapp.com//#!/aventon/"+aventon._id;
								sendEmail(users[i].email,'Hay un viaje que puede interesarte!',
									'Hola! De acuerdo a las últimas búsquedas que realizaste creemos que el siguiente viaje puede interesarte:'+url);
							}
						};
					}
				};
			})
			res.json(aventon);
		}
	});
};

function daysBDates(d1,d2){
	var dat1 = new Date(d1);
	var dat2 = new Date(d2);
	console.log(dat1);
	console.log(dat2);
	var fin = Math.abs(dat2.getTime() - dat1.getTime());
	var dias = Math.floor(fin / (1000 * 60 * 60 * 24))  
 
	return dias;
}

function sendEmail(email,subject,content){
	var transporter = Nodemailer.createTransport({
        service: 'Gmail',
        auth: {
            user: 'aventonprueba@gmail.com',
            pass: '@26481357'
        }
    });
    var mailOptions = {
        to: email,
        subject: subject, 
        html: '<b>'+ content +'</b>' 
    };

    // send mail with defined transport objectW
    transporter.sendMail(mailOptions, function(error, info){
        if(error){
        	console.log(error);
        }
        else{
        	console.log("Enviado: "+info.response);
        }
    });
};

/**
 * Show the current aventon
 */
exports.read = function(req, res) {
	res.json(req.aventon);
};

exports.get = function(req,res){
	var id = req.param('aventonId');
	Aventon.findOne(id).exec(function(err, aventon) {
		if (err) return next(err);
		if (!aventon) return next(new Error('Failed to load aventon ' + id));
		res.json(aventon);
	});
};

/**
 * Update a aventon
 */
exports.update = function(req, res) {
	var aventon = req.aventon;

	aventon = _.extend(aventon, req.body);

	aventon.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(aventon);
		}
	});
};

/**
 * Delete an aventon
 */
exports.delete = function(req, res) {
	var aventon = req.aventon;

	aventon.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(aventon);
		}
	});
};

/**
 * List of aventon
 */
exports.list = function(req, res) {
	var d = new Date();
	d.setDate(d.getDate() -1);
	Aventon.find({
	        "start_day": { $gte: d}
    	}).sort('-created').populate('user', 'displayName email').exec(function(err, aventones) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(aventones);
		}
	});
};

/**
 * List of aventon paginated
 */
exports.listPaginated = function(req, res) {
	var d = new Date();
	d.setDate(d.getDate() -1);
	
	var t = new Date();
	t.setDate(d.getDate() +1);
	t.setHours(23);
	t.setMinutes(59);

	console.log("CONSULTA");
	console.log(d);
	console.log(t);


	var perPage = 10;
	var skip = (req.param('page') -1) * perPage;
	var searchType = req.param('tipodebusqueda')
	if(searchType == "normal")
		var query = {
	        "start_day": { $gte: d}
		};
	else
		var query = {
	        "start_day": { $gte: d, $lt: t }
		};


	Aventon.find(query)
		.sort('-created')
		.skip(skip)
		.limit(perPage)
		.populate('user', 'displayName email review_count review_point imageProfile')
		.exec(function(err, aventones) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				//Aventon.count(query, function(err, count) {
				Aventon.count({}, function(err, count) {
					res.send({
						total: count,
						aventones: aventones
					});
				});
			}
		});
};

/**
 * aventon middleware
 */
exports.aventonByID = function(req, res, next, id) {
	Aventon.findById(id).populate('user').exec(function(err, aventon) {
		if (err) return next(err);
		if (!aventon) return next(new Error('Failed to load aventon ' + id));
		req.aventon = aventon;
		next();
	});
};





exports.aventonesByUser = function(req,res){
	Aventon.find({
		'user' : req.body.userId
	}).exec(function(err, aventones) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(aventones);
		}
	});
};

exports.aventonesQuantity = function(req,res){
	Aventon.count({}, function(err, count) {
		res.send({
			total: count
		});
	});
};


exports.aventonesLiveQuantity = function(req,res){
	var d = new Date();
	d.setDate(d.getDate() -1);
	Aventon.count({"start_day": { $gte: d}}, function(err, count) {
		res.send({
			total: count
		});
	});
};

exports.aventonesTodayQuantity = function(req,res){
	var d = new Date();
	d.setDate(d.getDate() -1);
	Aventon.count({"start_day": d}, function(err, count) {
		res.send({
			total: count
		});
	});
};


/**
 * List of aventon paginated
 */
exports.listPaginatedAdmin = function(req, res) {
	var d = new Date();
	d.setDate(d.getDate() -1);

	var perPage = 2;
	var skip = (req.param('page') -1) * perPage;

	Aventon.find()
		.sort('-created')
		.skip(skip)
		.limit(perPage)
		.populate('user', 'displayName email review_count review_point')
		.exec(function(err, aventones) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				Aventon.count( function(err, count) {
					res.send({
						total: count,
						aventones: aventones
					});
				});
			}
		});
};

exports.showCheckedNeed = function(req,res){
	Aventon.find().populate('user', 'displayName email review_count review_point').exec(function(err,aventones){
		if(err){
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		}else{
			var resultado = [];
			var included = false;
			for (var i = 0; i < aventones.length; i++) {
				included = false;
				for (var j = 0; j < aventones[i].consultations.length; j++) {
					if(aventones[i].consultations[j].state == "check" && !included){
						resultado.push(aventones[i]);
						included = true;
					};
				};
			};	
		};
	});
};

exports.searchAventon = function(req, res){
	
	var start_lat = req.body.start_place.lat;
	var start_lng = req.body.start_place.lng;
	var end_lat = req.body.end_place.lat;
	var end_lng = req.body.end_place.lng;
	
	var dateMax = new Date(req.body.date)
	dateMax.setDate(dateMax.getDate()+7);
	var dateMin = new Date(req.body.date)

	Aventon.find({ //primero se busca por fecha (margen de 7 dias)
		"start_day": { $gte: dateMin, $lt: dateMax}
	})
	.populate('user')
	.exec(function(err, aventones) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			var resultado = [];
			var d1;
			var d2;
			var included = false;
			for (var i = 0; i < aventones.length; i++) {
				included = false;
				for (var j = 0; j < aventones[i].tags_pos.length; j++) {
					//distancia con start
					d1 = distancia(aventones[i].tags_pos[j].pos[1],aventones[i].tags_pos[j].pos[0], start_lat, start_lng);
					//distancia con end
					d2 = distancia(aventones[i].tags_pos[j].pos[1],aventones[i].tags_pos[j].pos[0], end_lat, end_lng);
					console.log(d1);
					console.log(d2);
					if(d1 < 5 && !included){
						resultado.push(aventones[i]);
						included = true;
					}
					if(d2 < 5 && !included){
						resultado.push(aventones[i]);
						included = true;
					}

				};
			};
			res.json(resultado);
		}
	});

};

function rad(x) {return x*Math.PI/180;}

function distancia(lat1,lon1,lat2,lon2){
	var R     = 6378.137;//Radio de la tierra en km
	var dLat  = rad( lat2 - lat1 );
	var dLong = rad( lon2 - lon1 );

	var a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(rad(lat1)) * Math.cos(rad(lat2)) * Math.sin(dLong/2) * Math.sin(dLong/2);
	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
	var d = R * c;
	
	return d.toFixed(3);//Retorna tres decimales
};

exports.aventonesGeo = function(req, res){
	var coords = [];
	coords[0] = req.body.lng;
	coords[1] = req.body.lat;
	var maxDistance = req.body.distance;//metros
	maxDistance /= 100000;
	Aventon.find({
		pos_start:{
			'$near':coords,
			'$maxDistance':maxDistance
		}
	})
	.exec(function(err, aventones) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(aventones);
		}	
	});
};

exports.aventonesValidsByUser = function(req,res){
	var idUser = req.param('idUser');
	var today = new Date();
	today.setDate(today.getDate()-1); //yesterday
	Aventon.find({
		user : idUser,
		start_day :  { $gte: today }
	})
	.exec(function(err,aventones){
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(aventones);
		}
	});
};