'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Demand = mongoose.model('Demand'),
	_ = require('lodash');

/**
 * Create a demand
 */
exports.create = function(req, res) {
	var demandNew = new Demand(req.body);
	Demand.findOne(req.body).exec(function(err, demand) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			if(!demand){
				demandNew.save(function(err) {
					if (err) {
						return res.status(400).send({
							message: errorHandler.getErrorMessage(err)
						});
					} else {
						res.json(demandNew);
					}
				});
			}else{ //la demanda ya existe, enviamos mensaje
				return res.status(200).send({
				message: "La demanda ya fue enviada anteriormente."
				});
			}
		}
	});
	
};

/**
 * Show the current demand
 */
exports.read = function(req, res) {
	res.json(req.demand);
};

/**
 * Update a demand
 */
exports.update = function(req, res) {
	var demand = req.demand;

	demand = _.extend(demand, req.body);

	demand.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(demand);
		}
	});
};

/**
 * Delete an demand
 */
exports.delete = function(req, res) {
	var demand = req.demand;

	demand.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(demand);
		}
	});
};

/**
 * List of demand
 */
exports.list = function(req, res) {
	Demand.find().sort('-created').populate('user', 'displayName').populate('aventon').exec(function(err, demands) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(demands);
		}
	});
};

/**
 * demand middleware
 */
exports.demandByID = function(req, res, next, id) {
	Demand.findById(id).exec(function(err, demand) {
		if (err) return next(err);
		if (!demand) return next(new Error('Failed to load demand ' + id));
		req.demand = demand;
		next();
	});
};


exports.demandsForMe = function(req, res){
	Demand.find()
	.populate('aventon')
	.exec(function(err,demands){
		if(err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			var result = new Array();
			for (var i = demands.length - 1; i >= 0; i--) {
				if(String(req.user._id) ==  String(demands[i].aventon.user))
					result.push(demands[i]);
			};
			res.json(result);
		}
	});
};

exports.demandsWaitingByAventon = function(req,res){
	Demand.find({
		'aventon' : req.body.aventonId,
		'state' : 'Espera'
	})
	.populate('aventon')
	.populate('user')
	.exec(function(err,demands){
		if(err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(demands);
		}
	});
};


exports.demandsAcceptedByAventon = function(req,res){
	Demand.find({
		'aventon' : req.body.aventonId,
		'state' : 'Aceptada'
	})
	.populate('aventon')
	.populate('user')
	.exec(function(err,demands){
		if(err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(demands);
		}
	});
};

exports.demandsRejectedByAventon = function(req,res){
	Demand.find({
		'aventon' : req.body.aventonId,
		'state' : 'Rechazada'
	})
	.populate('aventon')
	.populate('user')
	.exec(function(err,demands){
		if(err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(demands);
		}
	});
};

exports.demandsQuantity = function(req,res){
	Demand.count({}, function(err, count) {
		res.send({
			total: count
		});
	});
};

exports.demandsAcceptedQuantity = function(req,res){
	Demand.count({state : "Aceptada"}, function(err, count) {
		res.send({
			total: count
		});
	});
};

exports.demandsAcceptedQuantityAventon = function(req,res){
	Demand.count({aventon: req.body.idAventon,state : "Aceptada"}, function(err, count) {
		res.send({
			total: count
		});
	});
};