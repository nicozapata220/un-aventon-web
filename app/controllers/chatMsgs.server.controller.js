'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	ChatMsg = mongoose.model('ChatMsg'),
	_ = require('lodash');

/**
 * Create a chatmsg
 */
exports.create = function(req, res) {
	var chatMsg = new ChatMsg(req.body);
	
	chatMsg.save(function(err) {
		if (err) {
			return res.status(400).send({
				chatMsg: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(chatMsg);
		}
	});
};

/**
 * Show the current chatMsg
 */
exports.read = function(req, res) {
	res.json(req.chatmsg);
};

/**
 * Update a chatmsg
 */
exports.update = function(req, res) {
	var chatMsg = req.chatMsg;

	chatMsg = _.extend(chatMsg, req.body);

	chatMsg.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(chatMsg);
		}
	});
};

/**
 * Delete an chatmsg
 */
exports.delete = function(req, res) {
	var chatMsg = req.chatMsg;

	chatMsg.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(chatMsg);
		}
	});
};

/**
 * List of chatmsg
 */
exports.list = function(req, res) {
	ChatMsg.find().sort('-created').exec(function(err, chatMsgs) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(chatMsgs);
		}
	});
};

/**
 * chatmsg middleware
 */
exports.chatMsgByID = function(req, res, next, id) {
	ChatMsg.findById(id).exec(function(err, chatMsg) {
		if (err) return next(err);
		if (!chatMsg) return next(new Error('Failed to load chatmsg ' + id));
		req.chatMsg = chatMsg;
		next();
	});
};

exports.chatMsgByConversationId = function(req, res){
	ChatMsg.find({
		"conversation" : req.body.conversationId
	})
	.exec(function(err, chatMsgs) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(chatMsgs);
		}
	});
};

