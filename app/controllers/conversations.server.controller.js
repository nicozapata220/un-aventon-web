'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Conversation = mongoose.model('Conversation'),
	_ = require('lodash');

/**
 * Create a conversation
 */
exports.create = function(req, res) {
	var conversation = new Conversation(req.body);
	
	Conversation.findOne({members:{$in:req.body.members}})
	.exec(function(err, conversationSearched) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			if(conversationSearched){
				console.log(conversationSearched);
				res.json(conversationSearched);
			} else {
				conversation.save(function(err) {
					console.log('chau');
					if (err) {
						return res.status(400).send({
							conversation: errorHandler.getErrorMessage(err)
						});
					} else {
						res.json(conversation);
					};
				});
			}
		}
	});
};

/**
 * Show the current conversation
 */
exports.read = function(req, res) {
	res.json(req.conversation);
};

/**
 * Update a conversation
 */
exports.update = function(req, res) {
	var conversation = req.conversation;

	conversation = _.extend(conversation, req.body);

	conversation.save(function(err) {
		if (err) {
			return res.status(400).send({
				conversation: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(conversation);
		}
	});
};

/**
 * Delete an conversation
 */
exports.delete = function(req, res) {
	var conversation = req.conversation;

	conversation.remove(function(err) {
		if (err) {
			return res.status(400).send({
				conversation: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(conversation);
		}
	});
};

/**
 * List of conversation
 */
exports.list = function(req, res) {
	Conversation.find().sort('-created').exec(function(err, conversations) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(conversations);
		}
	});
};

/**
 * conversation middleware
 */
exports.conversationByID = function(req, res, next, id) {
	Conversation.findById(id).exec(function(err, conversation) {
		if (err) return next(err);
		if (!conversation) return next(new Error('Failed to load conversation ' + id));
		req.conversation = conversation;
		next();
	});
};

exports.conversationByUserId = function(req, res) {
	Conversation.find({	members:{$in:[req.body.userId]}})
	.populate('members')
	.exec(function(err, conversations) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(conversations);
		}
	});
};