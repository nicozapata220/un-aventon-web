'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	UserNewsLetter = mongoose.model('UserNewsLetter'),
	config = require('../../config/config'),
	_ = require('lodash');
var nodemailer = require('nodemailer');


/**
 * Create a userNewsLetter
 */
exports.create = function(req, res) {
	var userNewsLetter = new UserNewsLetter(req.body);
	
	userNewsLetter.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(userNewsLetter);
		}
	});
};

/**
 * Create a userNewsLetter
 */
exports.subscribe = function(req, res) {
	var userNewsLetterNew = new UserNewsLetter(req.body);
	
	UserNewsLetter.findOne({ "email" : req.body.email})
	.exec(function(err,userNewsLetter){
		if(err ){
			return res.status(400).send({
					message: "error en verificacion"
				});
		}
		if(userNewsLetter){
			return res.status(400).send({
					message: "usuario ya suscripto"
				});
		}
		if(!userNewsLetter){
			userNewsLetterNew.save(function(err) {
				if (err) {
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				} else {
					res.json(userNewsLetterNew);
				}
			});
		}
	});
};

exports.authenticateUser = function(req, res){
	var email = req.body.email;
	var id =  req.body.id;
	
	res.render('templates/authenticate-user-newsletter', {
		url: 'http://' + req.headers.host + '/#!/authenticateUserNewsLetter/' + id,
		appName: config.app.title
	}, function(err, emailHTML) {
		if (err != null) {
			return res.status(400).send({
				message: "Error en renderizado"
			});
		} else {
			var smtpTransport = nodemailer.createTransport(config.mailer.options);
			var mailOptions = {
				to: email,
				from: config.mailer.from,
				subject: 'Active su usuario de newsletter',
				html: emailHTML
			};
			
			smtpTransport.sendMail(mailOptions, function(err) {
				if(err){
					return res.status(400).send({
						message: "Error al enviar email"
					});
				} else {
					return res.status(200).send({
						message : "Email de confirmación enviado"
					});
				} ;
			});
		}
	});	
};

/**
 * Show the current userNewsLetter
 */
exports.read = function(req, res) {
	res.json(req.userNewsLetter);
};

/**
 * Update a userNewsLetter
 */
exports.update = function(req, res) {
	var userNewsLetter = req.userNewsLetter;

	userNewsLetter = _.extend(userNewsLetter, req.body);

	userNewsLetter.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(userNewsLetter);
		}
	});
};

/**
 * Delete an userNewsLetter
 */
exports.delete = function(req, res) {
	var userNewsLetter = req.userNewsLetter;

	userNewsLetter.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(userNewsLetter);
		}
	});
};

/**
 * List of UserNewsLetter
 */
exports.list = function(req, res) {
	UserNewsLetter.find({state : "Activado"}).sort('-created').populate('user', 'displayName').exec(function(err, usersNewsLetter) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(usersNewsLetter);
		}
	});
};

/**
 * userNewsLetter middleware
 */
exports.userNewsLetterByID = function(req, res, next, id) {
	UserNewsLetter.findById(id).exec(function(err, userNewsLetter) {
		if (err) return next(err);
		if (!userNewsLetter) return next(new Error('Failed to load userNewsLetter ' + id));
		req.userNewsLetter = userNewsLetter;
		next();
	});
};

exports.deleteSuscriptorByEmail = function(req, res){
	UserNewsLetter.findOne({ "email" : req.body.email})
	.exec(function(err,userNewsLetter){
		if(err){
			return res.status(400).send({
					message: errorHandler.getErrorMessage("email inexistente")
				});
		}

		if(!userNewsLetter){
			return res.status(400).send({
					message: errorHandler.getErrorMessage("email inexistente")
				});
		}
		userNewsLetter.remove(function(err) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.json(userNewsLetter);
			}
		});
	});
};

exports.unsubscribe = function(req, res){
	UserNewsLetter.findOne({ "email" : req.body.email})
	.exec(function(err,userNewsLetter){
		if(err){
			return res.status(400).send({
					message: errorHandler.getErrorMessage("email inexistente")
				});
		}
		if(!userNewsLetter){
			return res.status(200).send({
					message: "email inexistente"
				});
		}
		userNewsLetter.remove(function(err) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.json(userNewsLetter);
			}
		});
	});
};

exports.usersNewsLetterQuantity = function(req,res){
	UserNewsLetter.count({state : "Activado"}, function(err, count) {
		res.send({
			total: count
		});
	});
};

exports.authenticateUserNewsletter = function(req, res){
	var u = req.userNewsLetter;
	u.state = "Activado";
	u.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(u);
		}
	});	
};

exports.getUserNewsLetterByEmail = function(req,res){
	console.log(req.body.email);
	UserNewsLetter.findOne({email : req.body.email})
	.exec(function(err, userNewsLetter){
		if(err){
			return res.status(400).send({
					message: errorHandler.getErrorMessage("email inexistente")
				});
		}
		if(!userNewsLetter){
			return res.status(400).send({
					message: errorHandler.getErrorMessage("usuario inexistente")
				});
		}
		res.json(userNewsLetter);
	});	
};

