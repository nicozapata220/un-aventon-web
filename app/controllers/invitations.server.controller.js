'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Invitation = mongoose.model('Invitation'),
	_ = require('lodash');

/**
 * Create a invitation
 */
exports.create = function(req, res) {
	var invitation = new Invitation(req.body);
	Invitation.find()
	.exec(function(err,invitations){
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			var exist = false;
			for (var i = 0; i < invitations.length; i++) {
				if(req.body.user_sender == invitations[i].user_sender
					&& req.body.user_receiver == invitations[i].user_receiver
					&& req.body.aventon == invitations[i].aventon){
					exist = true;
				}
			};
			if(!exist){
				invitation.save(function(err) {
					if (err) {
						return res.status(400).send({
							message: errorHandler.getErrorMessage(err)
						});
					} else {
						res.json(invitation);
					}
				});
			}else{
				return res.status(400).send({
					message: "Invitación existente"
				});
			}
			
		}
	});	
};

/**
 * Show the current invitation
 */
exports.read = function(req, res) {
	res.json(req.invitation);
};

/**
 * Update a invitation
 */
exports.update = function(req, res) {
	var invitation = req.invitation;

	invitation = _.extend(invitation, req.body);

	invitation.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(invitation);
		}
	});
};

/**
 * Delete an invitation
 */
exports.delete = function(req, res) {
	var invitation = req.invitation;

	invitation.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(invitation);
		}
	});
};

/**
 * List of invitation
 */
exports.list = function(req, res) {
	Invitation.find().sort('-created').exec(function(err, invitations) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(invitations);
		}
	});
};

/**
 * invitation middleware
 */
exports.invitationByID = function(req, res, next, id) {
	Invitation.findById(id).exec(function(err, invitation) {
		if (err) return next(err);
		if (!invitation) return next(new Error('Failed to load message ' + id));
		req.invitation = invitation;
		next();
	});
};


exports.invitationsToMe = function(req,res){
	Invitation.find({'user_receiver': req.user._id})
	.populate('user_sender','firstName lastName email username _id')
	.populate('aventon')
	.exec(function(err,invitations){
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(invitations);
		}	
	});
};

exports.invitationsSent = function(req,res){
	Invitation.find({'user_sender': req.user._id})
	.populate('user_receiver','firstName lastName email username _id')
	.populate('aventon')
	.exec(function(err,invitations){
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(invitations);
		}	
	});
};

exports.invitationsByIdAventon = function(req,res){
	Invitation.find({aventon : req.body.idAventon})
	.populate('user_receiver')
	.populate('user_sender')
	.exec(function(err,invitations){
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(invitations);
		}
	});
};