'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
	errorHandler = require('../errors.server.controller.js'),
	mongoose = require('mongoose'),
	passport = require('passport'),
	User = mongoose.model('User');


/**
 * Update a user
 */
exports.updateUser = function(req, res) {
	var user = req.profile;
	user = _.extend(user, req.body);
	user.displayName = user.firstName + ' ' + user.lastName;
	req.body.displayName = user.displayName;
	
	User.findOneAndUpdate({'_id' : user._id}, req.body, 
		function(err,userU){
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.json(userU);
			}	
		});
};

/**
 * Update user details
 */
exports.update = function(req, res) {
	// Init Variables
	var user = req.user;
	var message = null;

	// For security measurement we remove the roles from the req.body object
	delete req.body.roles;

	if (user) {
		// Merge existing user
		user = _.extend(user, req.body);
		user.updated = Date.now();
		user.displayName = user.firstName + ' ' + user.lastName;

		user.save(function(err) {
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				req.login(user, function(err) {
					if (err) {
						res.status(400).send(err);
					} else {
						res.json(user);
					}
				});
			}
		});
	} else {
		res.status(400).send({
			message: 'User is not signed in'
		});
	}
};


/**
 * Update user details
 */
exports.updateUserByAdmin = function(req, res) {
	// Init Variables
	var user = req.profile;
	var message = null;

	// For security measurement we remove the roles from the req.body object

	if (user) {
		// Merge existing user
		user = _.extend(user, req.body);
		user.updated = Date.now();
		user.displayName = user.firstName + ' ' + user.lastName;

		user.save(function(err) {

			if (err) {
				console.log(err);
				res.status(400).send(err);
			} else {
				res.json(user);
			}
		});
	} else {
		res.status(400).send({
			message: 'User is not signed in'
		});
	}
};



/**
 * Send User
 */
exports.me = function(req, res) {
	res.json(req.user || null);
};


exports.searchPublicUser = function(req,res){
	User.findById(req.body.id).exec(function(err, user) {
		if (!user) return next(new Error('Failed to load user ' + req.body.id));
		res.json(user);
	});	
}

/**
 * Show the current message
 */
exports.read = function(req, res) {
	res.json(req.profile);
};

exports.usersListAdmin = function(req, res){
	User.find().sort('-created').exec(function(err, users) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(users);
		}
	});
};

exports.createUserAdmin = function(req, res){
	// For security measurement we remove the roles from the req.body object
	delete req.body.roles;

	// Init Variables
	var user = new User(req.body);
	var message = null;

	// Add missing user fields
	user.provider = 'local';
	user.displayName = user.firstName + ' ' + user.lastName;

	// Then save the user 
	user.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			// Remove sensitive data before login
			user.password = undefined;
			user.salt = undefined;

			res.json(user);
		}
	});
};


/**
 * Update a user
 */
exports.addCredit = function(req, res) {
	var user = req.profile;

	var newCredit = user.credit + req.body.credit;

	User.findOneAndUpdate({'_id' : user._id}, { credit : newCredit  }, 
		function(err,userU){
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				res.json(userU);
			}	
		});
};

/**
 * Update a user
 */
exports.addFailPlace = function(req, res) {
	var user = req.profile;

	if (user) {
		// Merge existing user
		if(!user.fails_pos)
			user.fails_pos = [];
		var repeat = false;
		for (var i = 0; i < user.fails_pos.length; i++) {
			if(user.fails_pos[i].pos.pos == req.body.place.pos)
				repeat = true;
		};
		if(!repeat){
			var fail_pos = new Object();
			fail_pos = req.body.place;
			user.fails_pos.push(fail_pos);
			user.save(function(err) {
				if (err) {
					res.status(400).send(err);
				} else {
					res.json(user);
				}
			});
		};

	} else {
		res.status(400).send({
			message: 'User is not signed in'
		});
	}
};


exports.usersQuantity = function(req,res){
	User.count({}, function(err, count) {
		res.send({
			total: count
		});
	});
};

/**
 * Delete an user
 */
exports.delete = function(req, res) {
	var user = req.profile;

	user.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(user);
		}
	});
};

exports.uploadImage = function(req, res){//imageProfile
	res.json(req.files);
};

exports.validateuser = function(req,res){
	console.log(req.profile);
	var token = req.params.token;
	var state = req.profile.state;

	if(token == state){
		User.findOneAndUpdate({'_id' : req.profile._id}, {"state" : "Activado"}, 
		function(err,userU){
			if (err) {
				return res.status(400).send({
					message: errorHandler.getErrorMessage(err)
				});
			} else {
				return res.status(200).send({
					message: "Su cuenta de usuario ha sido activada."
				});
			}	
		});
	}else{
		return res.status(400).send({
					message: "Esta cuenta no pudo ser activada."
				});
	}
	
}