'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
	mongoose = require('mongoose'),
	User = mongoose.model('User');

/**
 * User middleware
 */
exports.userByID = function(req, res, next, id) {
	User.findOne({
		_id: id
	}).exec(function(err, user) {
		if (err) return next(err);
		if (!user) return next(new Error('Failed to load User ' + id));
		req.profile = user;
		next();
	});
};

/**
 * Require login routing middleware
 */
exports.requiresLogin = function(req, res, next) {
	if (!req.isAuthenticated()) {
		return res.status(401).send({
			message: 'User is not logged in'
		});
	}

	next();
};

/**
 * Require admin routing middleware
 */
exports.requiresAdmin = function(req, res, next) {
	if (!req.user || !req.user.role == 'admin') {
		return res.status(401).send({
			message: 'User is not logged in'
		});
	}

	next();
};

/**
 * User authorizations routing middleware
 */
exports.hasAuthorization = function(roles) {
	var _this = this;

	return function(req, res, next) {
		_this.requiresLogin(req, res, function() {
			if (_.intersection(req.user.roles, roles).length) {
				return next();
			} else {
				return res.status(403).send({
					message: 'User is not authorized'
				});
			}
		});
	};
};



function hasType(user, type) {
    var permissions = {};
    for (var i = user.custom_roles.length - 1; i >= 0; i--) {
        for (var j = user.custom_roles[i].permissions.length - 1; j >= 0; j--) {
            permissions[user.custom_roles[i].permissions[j]] = true;
        };
    };

    return permissions[type] || false;
}


exports.hasPermission = function(permission) {
	var _this = this;
	return function(req, res, next) {	
	    if (!hasType(req.user, permission) &&  req.user.role !="admin") {
	        return res.send(403, 'User is not authorized');
	    }
	    next();
	};
};