'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Newsletter = mongoose.model('Newsletter'),
	_ = require('lodash');


/**
 * Create a Newsletter
 */
exports.create = function(req, res) {
	var newsletter = new Newsletter(req.body);
	
	newsletter.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(newsletter);
		}
	});
};

/**
 * Show the current Newsletter
 */
exports.read = function(req, res) {
	res.json(req.newsletter);
};

/**
 * Update a Newsletter
 */
exports.update = function(req, res) {
	var newsletter = req.newsletter;

	newsletter = _.extend(newsletter, req.body);

	newsletter.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(newsletter);
		}
	});
};

/**
 * Delete an newsletter
 */
exports.delete = function(req, res) {
	var newsletter = req.newsletter;

	newsletter.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(newsletter);
		}
	});
};

/**
 * List of newsletter
 */
exports.list = function(req, res) {
	Newsletter.find().sort('-created').exec(function(err, newsletters) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(newsletters);
		}
	});
};

/**
 * config middleware
 */
exports.newsletterByID = function(req, res, next, id) {
	Newsletter.findById(id).exec(function(err, newsletter) {
		if (err) return next(err);
		if (!newsletter) return next(new Error('Failed to load newsletter ' + id));
		req.newsletter = newsletter;
		next();
	});
};


