'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Review = mongoose.model('Review'),
	_ = require('lodash');

/**
 * Create a review
 */
exports.create = function(req, res) {
	var review = new Review(req.body);
	
	review.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(review);
		}
	});
};

/**
 * Show the current review
 */
exports.read = function(req, res) {
	res.json(req.review);
};

/**
 * Update a review
 */
exports.update = function(req, res) {
	var review = req.review;

	review = _.extend(review, req.body);

	review.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(review);
		}
	});
};

/**
 * Delete an review
 */
exports.delete = function(req, res) {
	var review = req.review;

	review.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(review);
		}
	});
};

/**
 * List of review
 */
exports.list = function(req, res) {
	Review.find().sort('-created').exec(function(err, reviews) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(reviews);
		}
	});
};

/**
 * review middleware
 */
exports.reviewByID = function(req, res, next, id) {
	Review.findById(id).exec(function(err, review) {
		if (err) return next(err);
		if (!review) return next(new Error('Failed to load review ' + id));
		req.review = review;
		next();
	});
};


exports.pendingReviews = function(req,res){
	var d = new Date();
	d.setDate(d.getDate()-1);
	Review.find({
		state:'unChecked', 
		user_receiver: req.body.user_receiver, 
		user_origin : req.body.user_origin
	})
	.populate('user_receiver')
	.populate({
		path : 'aventon',
		match : { 'start_day' : { $lte : d} }
	})
	.exec(function(err, reviews) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(reviews);
		}
	});
};


exports.reviewsList = function(req,res){
	Review.find({
		state : 'checked',
		user_receiver : req.body.userId
	})
	.populate('user_origin')
	.populate('aventon')
	.exec(function(err, reviews) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(reviews);
		}
	});
};

