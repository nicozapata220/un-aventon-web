'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
    Pack = mongoose.model('Pack'),
    Transaction = mongoose.model('Transaction'),
    MP = require ("mercadopago"),
	_ = require('lodash');

/**
 * Find pack by id
 */
exports.pack = function(req, res, next, id) {
    Pack.load(id, function(err, pack) {
        if (err) return next(err);
        if (!pack) return next(new Error('Failed to load pack ' + id));
        req.pack = pack;
        next();
    });
};

/**
 * Create a pack
 */
exports.create = function(req, res) {
    var pack = new Pack(req.body);

    pack.validate(function(error) {
        if (error) {
          res.json({ error : error });
        } else {
            pack.save(function(err) {
                if (err) {
                    return res.send('users/signup', {
                        errors: err.errors,
                        pack: pack
                    });
                } else {
                    res.jsonp(pack);
                }
            });
        }
    });
};

/**
 * Update a pack
 */
exports.update = function(req, res) {
    var pack = req.pack;

    pack = _.extend(pack, req.body);

    pack.save(function(err) {
        res.jsonp(pack);
    });
};

/**
 * Delete an pack
 */
exports.destroy = function(req, res) {
    var pack = req.pack;

    pack.remove(function(err) {
        if (err) {
            res.render('error', {
                status: 500
            });
        } else {
            res.jsonp(pack);
        }
    });
};

/**
 * Show an pack
 */
exports.show = function(req, res) {
    res.jsonp(req.pack);
};

/**
 * List of packs
 */
exports.list = function(req, res) {
    Pack.find().sort('-created').exec(function(err, packs) {
        if (err) {
            res.render('error', {
                status: 500
            });
        } else {
            res.jsonp(packs);
        }
    });
};

exports.buy = function(req, res) {

    var mp = new MP('405956536334453', 'aER8trtHxp6lqUOBfn8B2pi6muK4KDvT');
    mp.sandboxMode(true);

    var id = req.user.id + Math.random().toString(36).substring(7);


    var preference = {
            "items": [
                {
                    "title": req.pack.nombre,
                    "quantity": 1,
                    "currency_id": "ARS",
                    "unit_price": req.pack.precio
                }
            ],
            "external_reference": id,
            "back_urls": {
                "success": "http://localhost:3000/#!/returnMP",
                "failure": "http://localhost:3000/#!/profile",
                "pending": "http://localhost:3000/#!/chat"
            },
        };

    if (process.env.NODE_ENV === 'production') {
        preference.back_urls.success = "http://polar-waters-6488.herokuapp.com/#!/returnMP";
        preference.back_urls.failure = "http://polar-waters-6488.herokuapp.com/#!/profile";
        preference.back_urls.pending = "http://polar-waters-6488.herokuapp.com/#!/chat";
    }

    mp.createPreference(preference, function (err, data){
        if (err) {
            console.log(err);
        } else {
            //console.log(JSON.stringify(data, null, 4));
            if(req.user.packs_comprados == null)
                req.user.packs_comprados = [];

            var transaction = new Transaction({
                pack: req.pack._id,
                mercado_pago_id: id,
                estado: "pending",
                user: req.user._id,
            });

            transaction.save(function(err) {
                res.redirect(data.response.sandbox_init_point);    
            })
        }
    });
}


exports.receivePay = function(request, response) {
user
    var mp = new MP(config.client_id_mp, config.client_secret_mp);
    mp.sandboxMode(true);
    console.log("receivePay");
    

    mp.getPaymentInfo(request.param("id"), function(err, data) {
         if (err) {
                console.log(err);
                response.writeHead(200, {
                    'Content-Type' : 'application/json; charset=utf-8'
                });
                response.write(err);
                response.end();
            } else {

                //console.log(JSON.stringify (data, null, 4));
                Transaction.findByMPId(data.response.collection.external_reference, function(err, transaction) {
           
                    transaction.estado = data.response.collection.status;
                    if(transaction.estado == 'approved') {
                        transaction.user.pines = transaction.user.pines + transaction.pack.cantidad_pines;
                    }
                    
                    transaction.save();
                    transaction.user.save();
                    response.writeHead(200, {
                        'Content-Type' : 'application/json; charset=utf-8'
                    });
                    response.write(JSON.stringify (data));
                    response.end();
                });
            }
    }); 
}

exports.changeStatusTransaction = function(req, res){
    Transaction.findOne({
        "mercado_pago_id" : req.body.mercado_pago_id
    })
    .exec(function(err, t) {
        if (err) {
            res.render('error', {
                status: 500
            });
        } else {
            t.estado = req.body.estado;

            t.save(function(err) {
              res.jsonp(t);
            });

        }
    });
}


exports.searchPack = function(req, res){
    Pack.findOne({
        "_id" : req.body.id_pack
    })
    .exec(function(err,pack){
        if (err) {
            res.render('error', {
                status: 500
            });
        } else {
            res.jsonp(pack);
        }
    });
}