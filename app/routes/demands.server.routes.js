'use strict';

/**
 * Module dependencies.
 */
var demands = require('../../app/controllers/demands.server.controller');

module.exports = function(app) {
	// Demand Routes
	app.route('/demand').get(demands.list);
	app.route('/demands').post(demands.create);
	app.route('/demandsWaitingByAventon').post(demands.demandsWaitingByAventon);
	app.route('/demandsAcceptedByAventon').post(demands.demandsAcceptedByAventon);
	app.route('/demandsRejectedByAventon').post(demands.demandsRejectedByAventon);
	app.route('/demandsQuantity').get(demands.demandsQuantity);
	app.route('/demandsAcceptedQuantity').get(demands.demandsAcceptedQuantity);
	app.route('/demandsAcceptedQuantityAventon').post(demands.demandsAcceptedQuantityAventon);
	

	app.route('/demands/:demandId')
		.get(demands.read)
		.put(demands.update)
		.delete(demands.delete);

	app.route('/demandsForMe').get(demands.demandsForMe);

	// Finish by binding the article middleware
	app.param('demandId', demands.demandByID);
};