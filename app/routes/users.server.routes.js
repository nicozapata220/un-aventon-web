'use strict';

/**
 * Module dependencies.
 */
var passport = require('passport');

module.exports = function(app) {
	// User Routes
	var users = require('../../app/controllers/users.server.controller');

	app.route('/searchPublicUser').post(users.searchPublicUser);
	// Setting up the users profile api
	app.route('/users/me').get(users.me);
	app.route('/users').put(users.update);
	app.route('/admin/users/:userId').put(users.hasPermission('Manejo_Usuarios'), users.updateUserByAdmin);
	app.route('/users/:userId').put(users.updateUser);
	app.route('/users/accounts').delete(users.removeOAuthProvider);
	app.route('/users/:userId').get(users.read);
	app.route('/usersListAdmin').get(users.usersListAdmin);
	app.route('/createUseradmin').post(users.createUserAdmin);
	app.route('/addCredit/:userId').post(users.addCredit);
	app.route('/addFailPlace/:userId').post(users.addFailPlace);
	app.route('/usersQuantity').get(users.usersQuantity);
	app.route('/users/:userId').delete(users.hasPermission('Manejo_Usuarios'), users.delete);
	app.route('/uploadImage/:userId').post(users.uploadImage);

	// Setting up the users password api
	app.route('/users/password').post(users.changePassword);
	app.route('/auth/forgot').post(users.forgot);
	app.route('/auth/reset/:token').get(users.validateResetToken);
	app.route('/auth/reset/:token').post(users.reset);
	app.route('/auth/validateuser/:userId/:token').get(users.validateuser);

	// Setting up the users authentication api
	app.route('/auth/signup').post(users.signup);
	app.route('/auth/signin').post(users.signin);
	app.route('/auth/signout').get(users.signout);

	// Setting the facebook oauth routes
	app.route('/auth/facebook').get(passport.authenticate('facebook', {
		scope: ['email']
	}));
	app.route('/auth/facebook/callback').get(users.oauthCallback('facebook'));

	// Setting the twitter oauth routes
	app.route('/auth/twitter').get(passport.authenticate('twitter'));
	app.route('/auth/twitter/callback').get(users.oauthCallback('twitter'));

	// Setting the google oauth routes
	app.route('/auth/google').get(passport.authenticate('google', {
		scope: [
			'https://www.googleapis.com/auth/userinfo.profile',
			'https://www.googleapis.com/auth/userinfo.email'
		]
	}));
	app.route('/auth/google/callback').get(users.oauthCallback('google'));

	// Setting the linkedin oauth routes
	app.route('/auth/linkedin').get(passport.authenticate('linkedin'));
	app.route('/auth/linkedin/callback').get(users.oauthCallback('linkedin'));
	
	// Setting the github oauth routes
	app.route('/auth/github').get(passport.authenticate('github'));
	app.route('/auth/github/callback').get(users.oauthCallback('github'));

	// Finish by binding the user middleware
	app.param('userId', users.userByID);
};
