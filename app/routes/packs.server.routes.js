'use strict';

/**
 * Module dependencies.
 */
var users = require('../../app/controllers/users.server.controller'),
    packs = require('../../app/controllers/packs.server.controller');

module.exports = function(app) {
	// pack 
    app.get('/packs', packs.list);
    app.post('/packs', users.hasPermission('Manejo_Packs'), packs.create);
    app.get('/packs/:packId/buy', packs.buy);
    app.post('/packs/receivePay', packs.receivePay);
    app.get('/packs/:packId', packs.show);
    app.put('/packs/:packId', users.hasPermission('Manejo_Packs'), packs.update);
    app.delete('/packs/:packId',  users.hasPermission('Manejo_Packs'), packs.destroy);
    app.post('/changeStatusTransaction', packs.changeStatusTransaction);
    app.post('/searchPack', packs.searchPack);

    //Finish with setting up the packId param
    app.param('packId', packs.pack);

};