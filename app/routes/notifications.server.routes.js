'use strict';

/**
 * Module dependencies.
 */
var notifications = require('../../app/controllers/notifications.server.controller');

module.exports = function(app) {
	// review Routes
	app.route('/notifications').get(notifications.list);
	app.route('/notifications').post(notifications.create);
	app.route('/notificationsByUser').post(notifications.notificationsByUser);
	app.route('/allNotificationsByUser').post(notifications.allNotificationsByUser);

	app.route('/notifications/:notificationId')
		.get(notifications.read)
		.put(notifications.update)
		.delete(notifications.delete);

	app.route('/sendEmail').post(notifications.sendEmail);

	// Finish by binding the article middleware
	app.param('notificationId', notifications.notificationByID);
};