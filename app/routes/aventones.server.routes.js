'use strict';

/**
 * Module dependencies.
 */
var aventones = require('../../app/controllers/aventones.server.controller');

module.exports = function(app) {
	// Aventon Routes
	app.route('/aventon').get(aventones.list);
	app.route('/aventon/paginate').get(aventones.listPaginated);
	app.route('/aventon/paginateAdmin').get(aventones.listPaginatedAdmin);
	app.route('/aventon').post(aventones.create);
	app.route('/aventonesByUser').post(aventones.aventonesByUser);
	app.route('/aventonesGeo').post(aventones.aventonesGeo);
	app.route('/aventonesValidsByUser').post(aventones.aventonesValidsByUser);
	
	app.route('/aventonesQuantity').get(aventones.aventonesQuantity);
	app.route('/aventonesLiveQuantity').get(aventones.aventonesLiveQuantity);
	app.route('/aventonesTodayQuantity').get(aventones.aventonesTodayQuantity);
	app.route('/getaventon/:aventonId').get(aventones.get);
	app.route('/showCheckedNeed').get(aventones.showCheckedNeed);
	app.route('/aventon/:aventonId')
		.get(aventones.read)
		.put(aventones.update)
		.delete(aventones.delete);

	app.route('/searchAventon').post(aventones.searchAventon);

	// Finish by binding the article middleware
	app.param('aventonId', aventones.aventonByID);
};