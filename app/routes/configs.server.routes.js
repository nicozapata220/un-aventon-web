'use strict';

/**
 * Module dependencies.
 */
var configs = require('../../app/controllers/configs.server.controller');

module.exports = function(app) {
	// review Routes
	app.route('/configs').get(configs.list);
	app.route('/configs').post(configs.create);

	app.route('/configs/:configId')
		.get(configs.read)
		.put(configs.update)
		.delete(configs.delete);

	// Finish by binding the article middleware
	app.param('configId', configs.configByID);
};