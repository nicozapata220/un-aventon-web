'use strict';

/**
 * Module dependencies.
 */
var conversations = require('../../app/controllers/conversations.server.controller');

module.exports = function(app) {
	// messages Routes
	app.route('/conversation').get(conversations.list);
	app.route('/conversations').post(conversations.create);
	app.route('/conversationsByUser').post(conversations.conversationByUserId);

	app.route('/conversations/:conversationId')
		.get(conversations.read)
		.put(conversations.update)
		.delete(conversations.delete);


	// Finish by binding the message middleware
	app.param('converasationId', conversations.conversationByID);
};