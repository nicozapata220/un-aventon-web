'use strict';

/**
 * Module dependencies.
 */
var invitations = require('../../app/controllers/invitations.server.controller');

module.exports = function(app) {
	// invitations Routes
	app.route('/invitations').get(invitations.list);
	app.route('/invitations').post(invitations.create);

	app.route('/invitations/:invitationId')
		.get(invitations.read)
		.put(invitations.update)
		.delete(invitations.delete);

	app.route('/invitationsToMe').get(invitations.invitationsToMe);
	app.route('/invitationsSent').get(invitations.invitationsSent);
	app.route('/invitationsByIdAventon').post(invitations.invitationsByIdAventon);

	// Finish by binding the message middleware
	app.param('invitationId', invitations.invitationByID);
};