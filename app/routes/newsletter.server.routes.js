'use strict';

/**
 * Module dependencies.
 */
var newsletters = require('../../app/controllers/newsletters.server.controller');

module.exports = function(app) {
	// newsletters Routes
	app.route('/newsletters').get(newsletters.list);
	app.route('/newsletters').post(newsletters.create);

	app.route('/newsletters/:newsletterId')
		.get(newsletters.read)
		.put(newsletters.update)
		.delete(newsletters.delete);

	// Finish by binding the article middleware
	app.param('newsletterId', newsletters.newsletterByID);
};