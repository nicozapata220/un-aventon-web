'use strict';

/**
 * Module dependencies.
 */
var users = require('../../app/controllers/users.server.controller'),
	roles = require('../../app/controllers/roles.server.controller');

module.exports = function(app) {
	// Article Routes
	app.route('/roles')
		.get(users.requiresAdmin, roles.list)
		.post(users.requiresAdmin, roles.create);

	app.route('/roles/:roleId')
		.get(users.requiresAdmin, roles.read)
		.put(users.requiresAdmin, roles.update)
		.delete(users.requiresAdmin, roles.delete);

	// Finish by binding the article middleware
	app.param('roleId', roles.roleByID);
};