'use strict';

/**
 * Module dependencies.
 */
var chatMsgs = require('../../app/controllers/chatMsgs.server.controller');

module.exports = function(app) {
	// messages Routes
	app.route('/chatMsg').get(chatMsgs.list);
	app.route('/chatMsgs').post(chatMsgs.create);
	app.route('/chatMsgByConversationId').post(chatMsgs.chatMsgByConversationId);

	app.route('/chatMsgs/:chatMsgId')
		.get(chatMsgs.read)
		.put(chatMsgs.update)
		.delete(chatMsgs.delete);


	// Finish by binding the message middleware
	app.param('chatMsgId', chatMsgs.chatMsgByID);
};