'use strict';

/**
 * Module dependencies.
 */
var usersNewsLetter = require('../../app/controllers/usersNewsLetter.server.controller');

module.exports = function(app) {
	// UserNewsLetter Routes
	app.route('/usersNewsLetter').get(usersNewsLetter.list);
	app.route('/usersNewsLetter').post(usersNewsLetter.subscribe);
	app.route('/deleteSuscriptorByEmail').post(usersNewsLetter.deleteSuscriptorByEmail);
	app.route('/usersNewsLetterQuantity').get(usersNewsLetter.usersNewsLetterQuantity);
	app.route('/authenticateusersend').post(usersNewsLetter.authenticateUser);
	app.route('/authenticateUser/:userNewsLetterId').post(usersNewsLetter.authenticateUserNewsletter);
	app.route('/subscribe').post(usersNewsLetter.subscribe);
	app.route('/unsubscribe').post(usersNewsLetter.unsubscribe);
	app.route('/getUserNewsLetterByEmail').post(usersNewsLetter.getUserNewsLetterByEmail);

	app.route('/usersNewsLetter/:userNewsLetterId')
		.get(usersNewsLetter.read)
		.put(usersNewsLetter.update)
		.delete(usersNewsLetter.delete);

	// Finish by binding the article middleware
	app.param('userNewsLetterId', usersNewsLetter.userNewsLetterByID);
};