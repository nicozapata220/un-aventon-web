'use strict';

/**
 * Module dependencies.
 */
var demandsAventon = require('../../app/controllers/demandsAventon.server.controller');

module.exports = function(app) {
	// DemandAventon Routes
	app.route('/demandAventon').get(demandsAventon.list);
	app.route('/myDemandsAventon').get(demandsAventon.myDemandsAventon);
	app.route('/demandsAventon').post(demandsAventon.create);
	app.route('/searchDemandAventon').post(demandsAventon.searchDemandAventon);
	
	app.route('/demandsAventon/:demandAventonId')
		.get(demandsAventon.read)
		.put(demandsAventon.update)
		.delete(demandsAventon.delete);

	// Finish by binding the article middleware
	app.param('demandAventonId', demandsAventon.demandAventonByID);
};