'use strict';

/**
 * Module dependencies.
 */
var faqs = require('../../app/controllers/faqs.server.controller');

module.exports = function(app) {
	// faqs Routes
	app.route('/faqs').get(faqs.list);
	app.route('/faqs').post(faqs.create);

	app.route('/faqs/:faqId')
		.get(faqs.read)
		.put(faqs.update)
		.delete(faqs.delete);

	// Finish by binding the article middleware
	app.param('faqId', faqs.faqByID);
};