'use strict';

/**
 * Module dependencies.
 */
var messages = require('../../app/controllers/messages.server.controller');

module.exports = function(app) {
	// messages Routes
	app.route('/message').get(messages.list);
	app.route('/messages').post(messages.create);

	app.route('/messages/:messageId')
		.get(messages.read)
		.put(messages.update)
		.delete(messages.delete);

	app.route('/messagesToMe').get(messages.messagesToMe);
	app.route('/messagesSent').get(messages.messagesSent);
	app.route('/allMessages').get(messages.allMessages);
	app.route('/allMessagesByUser/:contact').get(messages.allMessagesByUser);

	// Finish by binding the message middleware
	app.param('messageId', messages.messageByID);
};