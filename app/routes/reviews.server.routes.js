'use strict';

/**
 * Module dependencies.
 */
var reviews = require('../../app/controllers/reviews.server.controller');

module.exports = function(app) {
	// review Routes
	app.route('/review').get(reviews.list);
	app.route('/review').post(reviews.create);
	app.route('/pendingReviews').post(reviews.pendingReviews);
	app.route('/reviewsList').post(reviews.reviewsList);

	app.route('/reviews/:reviewId')
		.get(reviews.read)
		.put(reviews.update)
		.delete(reviews.delete);

	// Finish by binding the article middleware
	app.param('reviewId', reviews.reviewByID);
};